#ifndef DIALOGSTARTCALIBRAZIONE_H
#define DIALOGSTARTCALIBRAZIONE_H

#include <QDialog>
#include "rotella.h"
#include "diatermia.h"
#include "tastierinonumerico.h"

namespace Ui {
class dialogStartCalibrazione;
}

class dialogStartCalibrazione : public QDialog
{
    Q_OBJECT

public:
    explicit dialogStartCalibrazione(QWidget *parent = 0);
    ~dialogStartCalibrazione();

    void setRotella( Rotella* _rotella );
    void setParametri( parametri* par);


private:
    Ui::dialogStartCalibrazione *ui;
    Rotella* miaRotella;

    void showEvent(QShowEvent*);
    void closeEvent(QCloseEvent*);

    parametri* config;


public slots:
    void rotellaMove(bool up);
    void rotellaClick();

    void startCalibra();
    void setPassword();
signals:
    void onClose();
    void onStartCalibra();



};

#endif // DIALOGSTARTCALIBRAZIONE_H
