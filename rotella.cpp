#include "rotella.h"

Rotella::Rotella(QObject *parent) : QObject(parent)
{
    enableBeep = true;
    step       = 1;
}

void Rotella::setValue(int _rotella, int _tasto)
{
    val   = _rotella;
    tasto = _tasto;

    diff = abs( val - valOld);

    if( tasto == 0 && tastoOld == 1)
    {
        emit click();
        emit beep(1);
    }
    tastoOld = tasto;
    if( ( val - valOld ) > step )
    {
        emit valueChange(true);
        if( enableBeep == true ) emit beep(1);
    }
    else if( ( val - valOld ) < -step )
    {
        emit valueChange(false);
        if( enableBeep == true ) emit beep(1);
    }
//    if( diff > step )
//    {
//        emit valueChange(false);
//        if( enableBeep == true ) emit beep(1);
//    }

    valOld = val;
}

void Rotella::setStep(int _step)
{
    step = _step;
}

void Rotella::setDelegate(QWidget *pDelegate)
{
    delegate = pDelegate;

//    disconnect( this, SIGNAL(valueChange(bool)), 0, 0 );
//    disconnect( this, SIGNAL(click()),           0, 0 );
//    connect(    this, SIGNAL(valueChange(bool)), delegate, SLOT(rotellaMove(bool)));
//    connect(    this, SIGNAL(click()),           delegate , SLOT(rotellaClick()));
}


