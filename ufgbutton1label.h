#ifndef UFGBUTTON1LABEL_H
#define UFGBUTTON1LABEL_H

#include <QWidget>

namespace Ui {
class ufgButton1label;
}

class ufgButton1label : public QWidget
{
    Q_OBJECT

public:
    explicit ufgButton1label(QWidget *parent = 0);
    ~ufgButton1label();

    void setText(  QString text);    
    void setBackgroundColor( QColor col1, QColor col2);
    void setBorderColor( QColor col);
    void setFontSize( int size );

    QString getText();

    bool isFocused();

    void setBorderSize( int size) { mBorderSize = size; }

private:
    Ui::ufgButton1label *ui;

    QColor bgColor1;
    QColor bgColor2;
    QColor bdColor;

    QFont mioFont;

    int mBorderSize;

    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent* );
    void mouseReleaseEvent(QMouseEvent* );
    void focusInEvent(QFocusEvent* );
    void focusOutEvent(QFocusEvent* );

    bool focus;
    bool premuto;

signals:
    void buttonClicked();

};

#endif // UFGBUTTON1LABEL_H
