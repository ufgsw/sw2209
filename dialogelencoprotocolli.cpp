#include "dialogelencoprotocolli.h"
#include "ui_dialogelencoprotocolli.h"

dialogElencoProtocolli::dialogElencoProtocolli(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogElencoProtocolli)
{
    ui->setupUi(this);

    ui->buttonExit->setImage( ":/img/ico/return.png", ":/img/ico/return.png", ":/img/ico/return.png" , 100 );
    ui->buttonNext->setImage( ":/img/ico/next.png",   ":/img/ico/next.png",   ":/img/ico/next.png" ,   100 );
    ui->buttonNext->setEnabled( false );



    connect( ui->buttonExit, SIGNAL(buttonClicked()), this, SLOT(esci()));
    connect( ui->buttonNext, SIGNAL(buttonClicked()), this, SLOT(showNext()) );

    connect(ui->button1,  SIGNAL(buttonClicked()), this, SLOT( showProgrammi1())) ;
    connect(ui->button2,  SIGNAL(buttonClicked()), this, SLOT( showProgrammi2())) ;
    connect(ui->button3,  SIGNAL(buttonClicked()), this, SLOT( showProgrammi3())) ;
    connect(ui->button4,  SIGNAL(buttonClicked()), this, SLOT( showProgrammi4())) ;
    connect(ui->button5,  SIGNAL(buttonClicked()), this, SLOT( showProgrammi5())) ;
    connect(ui->button6,  SIGNAL(buttonClicked()), this, SLOT( showProgrammi6())) ;
    connect(ui->button7,  SIGNAL(buttonClicked()), this, SLOT( showProgrammi7())) ;
    connect(ui->button8,  SIGNAL(buttonClicked()), this, SLOT( showProgrammi8())) ;
    connect(ui->button9,  SIGNAL(buttonClicked()), this, SLOT( showProgrammi9())) ;
    connect(ui->button10, SIGNAL(buttonClicked()), this, SLOT( showProgrammi10())) ;
    connect(ui->button11, SIGNAL(buttonClicked()), this, SLOT( showProgrammi11())) ;
    connect(ui->button12, SIGNAL(buttonClicked()), this, SLOT( showProgrammi12())) ;
    connect(ui->button13, SIGNAL(buttonClicked()), this, SLOT( showProgrammi13())) ;
    connect(ui->button14, SIGNAL(buttonClicked()), this, SLOT( showProgrammi14())) ;

    ui->button1->setBorderSize( 3 );
    ui->button2->setBorderSize( 3 );
    ui->button3->setBorderSize( 3 );
    ui->button4->setBorderSize( 3 );
    ui->button5->setBorderSize( 3 );
    ui->button6->setBorderSize( 3 );
    ui->button7->setBorderSize( 3 );
    ui->button8->setBorderSize( 3 );
    ui->button9->setBorderSize( 3 );
    ui->button10->setBorderSize( 3 );
    ui->button11->setBorderSize( 3 );
    ui->button12->setBorderSize( 3 );
    ui->button13->setBorderSize( 3 );
    ui->button14->setBorderSize( 3 );

    ui->button1->setFontSize( 10 );
    ui->button2->setFontSize( 10 );
    ui->button3->setFontSize( 10 );
    ui->button4->setFontSize( 10 );
    ui->button5->setFontSize( 10 );
    ui->button6->setFontSize( 10 );
    ui->button7->setFontSize( 10 );

    ui->button8->setFontSize( 10 );
    ui->button9->setFontSize( 10 );
    ui->button10->setFontSize( 10 );
    ui->button11->setFontSize( 10 );
    ui->button12->setFontSize( 10 );
    ui->button13->setFontSize( 10 );
    ui->button14->setFontSize( 10 );



    formSelezionaProgramma = new dialogSelezionaProgramma( this );

    connect( formSelezionaProgramma, SIGNAL(programmaSelezionato(int)), this, SLOT(selezionatoProgramma(int)));
    connect( formSelezionaProgramma, SIGNAL(onclose()), this, SLOT(riprendoRotella()));
}

dialogElencoProtocolli::~dialogElencoProtocolli()
{
    delete formSelezionaProgramma;
    delete ui;
}

void dialogElencoProtocolli::setLicenzaStart(bool lic)
{
    mLicenza = lic;
    formSelezionaProgramma->setLicenzaStart( lic );
}

void dialogElencoProtocolli::showNext()
{
    setProtocllo( ORTOPEDICI_GINOCCHIO );
}

void dialogElencoProtocolli::setProtocllo(eProtocolli _protocollo)
{
    tipoProtocollo = _protocollo;
    ui->buttonNext->setEnabled( false );

    switch (tipoProtocollo) {
    case GENERICI:
        this->ui->labelTitolo->setText( tr("PROTOCOLLI GENERICI"));

        this->ui->button1->setText( tr("FASE ACUTA"));
        this->ui->button2->setText( tr("FASE CRONICA"));
        this->ui->button3->setText( tr("RIDUZIONE DEL DOLORE"));
        this->ui->button4->setText( tr("CONTRATTURA"));
        this->ui->button5->setText( tr("LINFODRENAGGIO"));
        this->ui->button6->setText( tr("RADICOLOPATIA FASE ACUTA"));
        this->ui->button7->setText( tr("BLOCCO ARTICOLARE / RIGIDITA"));

        this->ui->button1->setVisible(  true  );
        this->ui->button2->setVisible(  true  );
        this->ui->button3->setVisible(  true  );
        this->ui->button4->setVisible(  true  );
        this->ui->button5->setVisible(  true  );
        this->ui->button6->setVisible(  true  );
        this->ui->button7->setVisible(  true  );
        this->ui->button8->setVisible(  false );
        this->ui->button9->setVisible(  false );
        this->ui->button10->setVisible( false );
        this->ui->button11->setVisible( false );
        this->ui->button12->setVisible( false );
        this->ui->button13->setVisible( false );
        this->ui->button14->setVisible( false );
        break;
    case ORTOPEDICI:

        ui->buttonNext->setEnabled( true );

        this->ui->labelTitolo->setText( tr("PROTOCOLLI PER PATOLOGIA"));

        this->ui->button1->setText(  tr("LOMBALGIA"));
        this->ui->button2->setText(  tr("LOMBOSCIALTAGIA SUB ACUTA"));
        this->ui->button3->setText(  tr("PERIARTRITE SPALLA FASE ACUTA (DINAMICI)"));
        this->ui->button4->setText(  tr("PERIARTRITE SPALLA FASE ACUTA (STATICI)"));
        this->ui->button5->setText(  tr("PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (DINAMICI)"));
        this->ui->button6->setText(  tr("PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (STATICI)"));
        this->ui->button7->setText(  tr("SPALLA FASE ACUTA"));

        this->ui->button8->setText(  tr("SPALLA FASE SUB CUTA"));
        this->ui->button9->setText(  tr("EPICONDILITE DA SOVRACCARICO"));
        this->ui->button10->setText( tr("POLSO FASE ACUTA"));
        this->ui->button11->setText( tr("POLSO FASE SUB  ACUTA O CRONICA"));
        this->ui->button12->setText( tr("COXARTROSI SUB ACUTA E CRONICA"));
        this->ui->button13->setText( tr("DISTORSIONE CAVIGLIA FASE ACUTA"));
        this->ui->button14->setText( tr("DISTORSIONE CAVIGLIA FASE SUB ACUTA"));

        this->ui->button1->setVisible(  true );
        this->ui->button2->setVisible(  true );
        this->ui->button3->setVisible(  true );
        this->ui->button4->setVisible(  true );
        this->ui->button5->setVisible(  true );
        this->ui->button6->setVisible(  true );
        this->ui->button7->setVisible(  true );
        this->ui->button8->setVisible(  true );
        this->ui->button9->setVisible(  true );
        this->ui->button10->setVisible( true );
        this->ui->button11->setVisible( true );
        this->ui->button12->setVisible( true );
        this->ui->button13->setVisible( true );
        this->ui->button14->setVisible( true );

        break;

    case ORTOPEDICI_GINOCCHIO:
        this->ui->labelTitolo->setText( tr("GINOCCHIO POST CHIRURGICO"));

        this->ui->button1->setText(  tr("PRIMA FASE - DRENAGGIO"));
        this->ui->button2->setText(  tr("PRIMA FASE - ANTINFIAMMATORIO"));
        this->ui->button3->setText(  tr("SECONDA FASE - MOBILIZZAZZIONE PASSIVA"));
        this->ui->button4->setText(  tr("TERZA FASE - MOBILIZZAZZIONE ATTIVA"));
        this->ui->button5->setText(  tr("QUARTA FASE - MOBILIZZAZZIONE ATTIVA CON RESISTENZA"));
        this->ui->button6->setText(  tr("QUINTA FASE - RECUPERO DEFICIT DI FLESSIONE"));
        this->ui->button7->setText(  tr("SESTA FASE - RECUPERO DEFICIT IN FLESSO/ESTENSIONE"));

        this->ui->button8->setText(  tr("SESTA FASE - RECUPERO MOBILITA ARTICOLARE"));

        this->ui->button1->setVisible(  true );
        this->ui->button2->setVisible(  true );
        this->ui->button3->setVisible(  true );
        this->ui->button4->setVisible(  true );
        this->ui->button5->setVisible(  true );
        this->ui->button6->setVisible(  true );
        this->ui->button7->setVisible(  true );
        this->ui->button8->setVisible(  true );
        this->ui->button9->setVisible(  false );
        this->ui->button10->setVisible( false );
        this->ui->button11->setVisible( false );
        this->ui->button12->setVisible( false );
        this->ui->button13->setVisible( false );
        this->ui->button14->setVisible( false );

        break;

    case NEUROLOGICI:
        this->ui->labelTitolo->setText( tr("PROTOCOLLI NEUROLOGICI"));

        ui->button1->setFontSize( 10 );
        ui->button2->setFontSize( 10 );
        ui->button3->setFontSize( 10 );
        ui->button4->setFontSize( 10 );
        ui->button5->setFontSize( 10 );

        this->ui->button1->setText( tr("MIELO LESIONE PARZIALE"));
        this->ui->button2->setText( tr("TRATTAMENTO POST ICTUS"));
        this->ui->button3->setText( tr("LESIONI NERVOSE PERIFERICHE ( ERNIE DISCALI )"));
        this->ui->button4->setText( tr("LESIONI NERVOSE PERIFERICHE (PLESSO BRACHIALE )"));
        this->ui->button5->setText( tr("TRATTAMENTO DELLE LESIONI SPINALI PERIFERICHE"));

        this->ui->button6->setVisible(  false );
        this->ui->button7->setVisible(  false );
        this->ui->button8->setVisible(  false );
        this->ui->button9->setVisible(  false );
        this->ui->button10->setVisible( false );
        this->ui->button11->setVisible( false );
        this->ui->button12->setVisible( false );
        this->ui->button13->setVisible( false );
        this->ui->button14->setVisible( false );

        break;
    default:
        break;
    }
}

void dialogElencoProtocolli::showProgrammi1()
{
    QString programma = ui->button1->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        // FASE ACUTA

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("zona dolente"));
        programma1.elettrodo             = QString( tr( "DINAMICO 60"));
        programma1.tempo                 = 1200;
        programma1.potenza               = 5;
        programma1.potenzaMinima         = 5;
        programma1.potenzaMassima        = 15;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("prono"));
        programma2.posizioneNeutro       = QString( tr("addome"));
        programma2.posizioneElettrodo    = QString( tr("zona dolente"));
        programma2.elettrodo             = QString( tr( "DINAMICO 60"));
        programma2.tempo                 = 1200;
        programma2.potenza               = 10;
        programma2.potenzaMinima         = 5;
        programma2.potenzaMassima        = 15;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 100;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prono"));
        programma3.posizioneNeutro       = QString( tr("addome"));
        programma3.posizioneElettrodo    = QString( tr("zona dolente"));
        programma3.elettrodo             = QString( tr("STATICO grande"));
        programma3.tempo                 = 1800;
        programma3.potenza               = 5;
        programma3.potenzaMinima         = 5;
        programma3.potenzaMassima        = 15;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_statico_grande = 1;

        programma4.posizionePaziente     = QString( tr("prono"));
        programma4.posizioneNeutro       = QString( tr("addome"));
        programma4.posizioneElettrodo    = QString( tr("zona dolente"));
        programma4.elettrodo             = QString( tr( "STATICO"));
        programma4.tempo                 = 1800;
        programma4.potenza               = 10;
        programma4.potenzaMinima         = 5;
        programma4.potenzaMassima        = 15;
        programma4.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma4.frequenza             = 460;
        programma4.frequenzaPemf         = 100;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_statico_grande = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;
        break;
    case ORTOPEDICI:
        // LOMBALGIA ( 25/01/2018 )

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("diaframma"));
        programma1.posizioneElettrodo    = QString( tr("lombosacrale"));
        programma1.elettrodo             = QString( tr("STATICO grande"));
        programma1.tempo                 = 600;
        programma1.potenza               = 15;
        programma1.potenzaMinima         = 15;
        programma1.potenzaMassima        = 30;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_grande = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("zona lombare"));
        programma2.posizioneElettrodo    = QString( tr("muscolo psoas"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 600;
        programma2.potenza               = 20;
        programma2.potenzaMinima         = 20;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prono"));
        programma3.posizioneNeutro       = QString( tr("diaframma"));
        programma3.posizioneElettrodo    = QString( tr("L1-L4 e piliforme"));
        programma3.elettrodo             = QString( tr("DINAMICO 60"));
        programma3.tempo                 = 600;
        programma3.potenza               = 10;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 20;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_dinamico = 1;

        programma4.posizionePaziente     = QString( tr("supino"));
        programma4.posizioneNeutro       = QString( tr("zona lombare"));
        programma4.posizioneElettrodo    = QString( tr("zona lombare e glutei"));
        programma4.elettrodo             = QString( tr( "DINAMICO 60"));
        programma4.tempo                 = 600;
        programma4.potenza               = 20;
        programma4.potenzaMinima         = 20;
        programma4.potenzaMassima        = 40;
        programma4.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma4.frequenza             = 460;
        programma4.frequenzaPemf         = 300;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;
        break;

    case ORTOPEDICI_GINOCCHIO:
        // PRIMA FASE DRENAGGIO
        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("zona lombare"));
        programma1.posizioneElettrodo    = QString( tr("cavo popliteo"));
        programma1.elettrodo             = QString( tr("STATICO piccolo"));
        programma1.tempo                 = 900;
        programma1.potenza               = 10;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;
        break;

    case NEUROLOGICI:
        // MIELO LESIONE PARZIALE
        programma = ui->button1->getText();

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("diaframma"));
        programma1.posizioneElettrodo    = QString( tr("sulla lesione"));
        programma1.elettrodo             = QString( tr("STATICO grande"));
        programma1.tempo                 = 1800;
        programma1.potenza               = 20;
        programma1.potenzaMinima         = 20;
        programma1.potenzaMassima        = 20;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_grande = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("polso"));
        programma2.posizioneElettrodo    = QString( tr("sulla lesione"));
        programma2.elettrodo             = QString( tr( "STATICO grande"));
        programma2.tempo                 = 900;
        programma2.potenza               = 20;
        programma2.potenzaMinima         = 20;
        programma2.potenzaMassima        = 20;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 690;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_statico_grande = 1;

        programma3.posizionePaziente     = QString( tr("supino"));
        programma3.posizioneNeutro       = QString( tr("pianta del piede"));
        programma3.posizioneElettrodo    = QString( tr("sulla lesione"));
        programma3.elettrodo             = QString( tr("STATICO grande"));
        programma3.tempo                 = 900;
        programma3.potenza               = 20;
        programma3.potenzaMinima         = 20;
        programma3.potenzaMassima        = 20;
        programma3.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma3.frequenza             = 690;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.capacitivo_statico_grande = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = false;
        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);

    formSelezionaProgramma->showNormal();

}
void dialogElencoProtocolli::showProgrammi2()
{
    QString programma  = ui->button2->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        // FASE CRONICA

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("zona dolente"));
        programma1.elettrodo             = QString( tr( "DINAMICO 60"));
        programma1.tempo                 = 1200;
        programma1.potenza               = 15;
        programma1.potenzaMinima         = 15;
        programma1.potenzaMassima        = 30;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("prono"));
        programma2.posizioneNeutro       = QString( tr("addome"));
        programma2.posizioneElettrodo    = QString( tr("zona dolente"));
        programma2.elettrodo             = QString( tr( "DINAMICO 60"));
        programma2.tempo                 = 1200;
        programma2.potenza               = 20;
        programma2.potenzaMinima         = 20;
        programma2.potenzaMassima        = 50;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prono"));
        programma3.posizioneNeutro       = QString( tr("addome"));
        programma3.posizioneElettrodo    = QString( tr("zona dolente"));
        programma3.elettrodo             = QString( tr("STATICO grande"));
        programma3.tempo                 = 1800;
        programma3.potenza               = 10;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 20;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 300;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_statico_grande = 1;

        programma4.posizionePaziente     = QString( tr("prono"));
        programma4.posizioneNeutro       = QString( tr("addome"));
        programma4.posizioneElettrodo    = QString( tr("zona dolente"));
        programma4.elettrodo             = QString( tr("STATICO grande"));
        programma4.tempo                 = 1800;
        programma4.potenza               = 20;
        programma4.potenzaMinima         = 20;
        programma4.potenzaMassima        = 30;
        programma4.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma4.frequenza             = 460;
        programma4.frequenzaPemf         = 300;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_statico_grande = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;
        break;
    case ORTOPEDICI:
        // LOMBOSCIALTAGIA SUB ACUTA

        programma1.posizionePaziente     = QString( tr("decubito laterale prono"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("inserzione nervo sciatico fino irradiazione dolore"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 10;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 50;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 200;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("decubito laterale prono"));
        programma2.posizioneNeutro       = QString( tr("addome"));
        programma2.posizioneElettrodo    = QString( tr("muscoli paravertebrali, glutei, bicipiti femorali, polpacci e tibiali"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 900;
        programma2.potenza               = 15;
        programma2.potenzaMinima         = 5;
        programma2.potenzaMassima        = 50;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 200;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = false;
        programma4.enable = false;
        break;

    case ORTOPEDICI_GINOCCHIO:
        // PRIMA ANTINFIAMMATORIO
        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("gadtrocnemius"));
        programma1.posizioneElettrodo    = QString( tr("retto femorale"));
        programma1.elettrodo             = QString( tr("STATICO piccolo"));
        programma1.tempo                 = 900;
        programma1.potenza               = 10;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;
        break;

    case NEUROLOGICI:
        // TRATTAMENTO POST ICTUS
        programma = ui->button2->getText();

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("zona cervico/dorsale"));
        programma1.posizioneElettrodo    = QString( tr("muscolo retto femorale"));
        programma1.elettrodo             = QString( tr("STATICO grande"));
        programma1.tempo                 = 1200;
        programma1.potenza               = 20;
        programma1.potenzaMinima         = 20;
        programma1.potenzaMassima        = 30;
        programma1.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_grande = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("zona cervico/dorsale"));
        programma2.posizioneElettrodo    = QString( tr("articolazione gleno omerale"));
        programma2.elettrodo             = QString( tr("STATICO piccolo"));
        programma2.tempo                 = 1200;
        programma2.potenza               = 15;
        programma2.potenzaMinima         = 15;
        programma2.potenzaMassima        = 15;
        programma2.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 100;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.resistivo_statico_piccolo = 1;

        programma3.posizionePaziente     = QString( tr("supino"));
        programma3.posizioneNeutro       = QString( tr("zona cervico/dorsale"));
        programma3.posizioneElettrodo    = QString( tr("polso"));
        programma3.elettrodo             = QString( tr("STATICO piccolo"));
        programma3.tempo                 = 900;
        programma3.potenza               = 10;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 15;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_statico_piccolo = 1;

        programma4.posizionePaziente     = QString( tr("supino"));
        programma4.posizioneNeutro       = QString( tr("zona cervico/dorsale"));
        programma4.posizioneElettrodo    = QString( tr("arto superiore plegico"));
        programma4.elettrodo             = QString( tr("DINAMICO 60"));
        programma4.tempo                 = 1200;
        programma4.potenza               = 20;
        programma4.potenzaMinima         = 20;
        programma4.potenzaMassima        = 20;
        programma4.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma4.frequenza             = 690;
        programma4.frequenzaPemf         = 300;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;
        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();

}
void dialogElencoProtocolli::showProgrammi3()
{
    QString programma;
    programma = ui->button3->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        // RIDUZIONE DEL DOLORE

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("zona dolente"));
        programma1.elettrodo             = QString( tr( "DINAMICO 60"));
        programma1.tempo                 = 1500;
        programma1.potenza               = 5;
        programma1.potenzaMinima         = 5;
        programma1.potenzaMassima        = 5;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("prono"));
        programma2.posizioneNeutro       = QString( tr("addome"));
        programma2.posizioneElettrodo    = QString( tr("zona dolente"));
        programma2.elettrodo             = QString( tr( "DINAMICO 60"));
        programma2.tempo                 = 1500;
        programma2.potenza               = 5;
        programma2.potenzaMinima         = 5;
        programma2.potenzaMassima        = 10;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 100;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prono"));
        programma3.posizioneNeutro       = QString( tr("addome"));
        programma3.posizioneElettrodo    = QString( tr("zona dolente"));
        programma3.elettrodo             = QString( tr( "STATICO grande"));
        programma3.tempo                 = 1800;
        programma3.potenza               = 5;
        programma3.potenzaMinima         = 5;
        programma3.potenzaMassima        = 5;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_statico_grande = 1;

        programma4.posizionePaziente     = QString( tr("prono"));
        programma4.posizioneNeutro       = QString( tr("addome"));
        programma4.posizioneElettrodo    = QString( tr("zona dolente"));
        programma4.elettrodo             = QString( tr("STATICO grande"));
        programma4.tempo                 = 1800;
        programma4.potenza               = 5;
        programma4.potenzaMinima         = 5;
        programma4.potenzaMassima        = 10;
        programma4.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma4.frequenza             = 460;
        programma4.frequenzaPemf         = 100;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_statico_grande = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;

        break;
    case ORTOPEDICI:
        // PERIARTRITE SPALLA FASE ACUTA ( DINAMICI )

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("scapola"));
        programma1.posizioneElettrodo    = QString( tr("deltoide e bicipiti brachiali"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 10;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 50;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("zona lombare"));
        programma2.posizioneElettrodo    = QString( tr("pettorale e bicipiti brachiali"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 600;
        programma2.potenza               = 40;
        programma2.potenzaMinima         = 40;
        programma2.potenzaMassima        = 50;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 100;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prono"));
        programma3.posizioneNeutro       = QString( tr("braccio"));
        programma3.posizioneElettrodo    = QString( tr("sotto scapolare e scapolare"));
        programma3.elettrodo             = QString( tr("DINAMICO 60"));
        programma3.tempo                 = 600;
        programma3.potenza               = 10;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 50;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_dinamico = 1;

        programma4.posizionePaziente     = QString( tr("su di un lato"));
        programma4.posizioneNeutro       = QString( tr("gomito"));
        programma4.posizioneElettrodo    = QString( tr("deltoide e zona scapolare"));
        programma4.elettrodo             = QString( tr("DINAMICO 60"));
        programma4.tempo                 = 600;
        programma4.potenza               = 40;
        programma4.potenzaMinima         = 10;
        programma4.potenzaMassima        = 50;
        programma4.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma4.frequenza             = 460;
        programma4.frequenzaPemf         = 100;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;
        break;

    case ORTOPEDICI_GINOCCHIO:
        // SECONDA FASE MOBILIZZAZZIONE PASSIVA
        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("zona lombare"));
        programma1.posizioneElettrodo    = QString( tr("retto femorale"));
        programma1.elettrodo             = QString( tr("STATICO piccolo"));
        programma1.tempo                 = 900;
        programma1.potenza               = 15;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 200;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;
        break;

    case NEUROLOGICI:
        // LESIONI NERVOSE PERIFERICHE ( ERNIA DISCALI )

        programma1.posizionePaziente     = QString( tr("confortevole"));
        programma1.posizioneNeutro       = QString( tr("diaframma"));
        programma1.posizioneElettrodo    = QString( tr("compressione intervertebrale"));
        programma1.elettrodo             = QString( tr("STATICO piccolo"));
        programma1.tempo                 = 1200;
        programma1.potenza               = 5;
        programma1.potenzaMinima         = 5;
        programma1.potenzaMassima        = 10;
        programma1.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;
        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}
void dialogElencoProtocolli::showProgrammi4()
{
    QString programma = ui->button4->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        // CONTRATTURA

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("zona dolente"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 60;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma1.frequenza             = 690;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("prono"));
        programma2.posizioneNeutro       = QString( tr("addome"));
        programma2.posizioneElettrodo    = QString( tr("zona dolente"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 300;
        programma2.potenza               = 30;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.resistivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prono"));
        programma3.posizioneNeutro       = QString( tr("addome"));
        programma3.posizioneElettrodo    = QString( tr("zona dolente"));
        programma3.elettrodo             = QString( tr("STATICO piccolo"));
        programma3.tempo                 = 900;
        programma3.potenza               = 20;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 100;
        programma3.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 300;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.capacitivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = false;

        break;
    case ORTOPEDICI:
        // PERIARTRITE SPALLA FASE ACUTA ( ELETTRODI STATICI )

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("braccio"));
        programma1.posizioneElettrodo    = QString( tr("zona scapolare"));
        programma1.elettrodo             = QString( tr("STATICO piccolo"));
        programma1.tempo                 = 600;
        programma1.potenza               = 10;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_piccolo = 1;

        programma2.posizionePaziente     = QString( tr("su di un lato"));
        programma2.posizioneNeutro       = QString( tr("gomito"));
        programma2.posizioneElettrodo    = QString( tr("deltoide e zona scapolare"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 600;
        programma2.potenza               = 40;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 65;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 100;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("seduto"));
        programma3.posizioneNeutro       = QString( tr("avambraccio"));
        programma3.posizioneElettrodo    = QString( tr("bicipite brachiale"));
        programma3.elettrodo             = QString( tr("STATICO piccolo"));
        programma3.tempo                 = 600;
        programma3.potenza               = 10;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 90;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_statico_piccolo = 1;

        programma4.posizionePaziente     = QString( tr("su di un lato"));
        programma4.posizioneNeutro       = QString( tr("gomito"));
        programma4.posizioneElettrodo    = QString( tr("deltoide e zona scapolare"));
        programma4.elettrodo             = QString( tr("DINAMICO 60"));
        programma4.tempo                 = 600;
        programma4.potenza               = 40;
        programma4.potenzaMinima         = 10;
        programma4.potenzaMassima        = 90;
        programma4.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma4.frequenza             = 460;
        programma4.frequenzaPemf         = 100;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;
        break;

    case ORTOPEDICI_GINOCCHIO:
        // TERZA FASE MOBILIZZAZZIONE ATTIVA

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("gadtrocnemius"));
        programma1.posizioneElettrodo    = QString( tr("retto femorale"));
        programma1.elettrodo             = QString( tr("STATICO piccolo"));
        programma1.tempo                 = 900;
        programma1.potenza               = 15;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_piccolo = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("zona lombare"));
        programma2.posizioneElettrodo    = QString( tr("retto femorale"));
        programma2.elettrodo             = QString( tr("STATICO piccolo"));
        programma2.tempo                 = 900;
        programma2.potenza               = 15;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = false;
        programma4.enable = false;
        break;

    case NEUROLOGICI:
        // TRATTAMENTO LESIONI NERVOSE PERIFERICHE ( PLESSO BRACHIALE )

        programma1.posizionePaziente     = QString( tr("confortevole"));
        programma1.posizioneNeutro       = QString( tr("plesso brachiale posteriore"));
        programma1.posizioneElettrodo    = QString( tr("plesso brachiale anteriore"));
        programma1.elettrodo             = QString( tr("STATICO piccolo"));
        programma1.tempo                 = 1200;
        programma1.potenza               = 5;
        programma1.potenzaMinima         = 5;
        programma1.potenzaMassima        = 10;
        programma1.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;
        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}
void dialogElencoProtocolli::showProgrammi5()
{
    QString programma;
    programma = ui->button5->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        // LINFODRENAGGIO

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("zona dolente"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 1200;
        programma1.potenza               = 20;
        programma1.potenzaMinima         = 20;
        programma1.potenzaMassima        = 40;
        programma1.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("prono"));
        programma2.posizioneNeutro       = QString( tr("addome"));
        programma2.posizioneElettrodo    = QString( tr("zona dolente"));
        programma2.elettrodo             = QString( tr("STATICO grande"));
        programma2.tempo                 = 1800;
        programma2.potenza               = 10;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 15;
        programma2.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_statico_grande = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = false;
        programma4.enable = false;

        break;
    case ORTOPEDICI:
        // PERIARTRITE DI SPALLA FASE SUB ACUTA O CRONICA ( ELETTRODI DINAMICI )

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("scapola"));
        programma1.posizioneElettrodo    = QString( tr("deltoide e bicipiti brachiali"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 15;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 40;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("zona lombare"));
        programma2.posizioneElettrodo    = QString( tr("pettorale e bicipiti brachiali"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 600;
        programma2.potenza               = 50;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 690;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prona"));
        programma3.posizioneNeutro       = QString( tr("braccio"));
        programma3.posizioneElettrodo    = QString( tr("sotto scapolare e scapolare"));
        programma3.elettrodo             = QString( tr("DINAMICO 60"));
        programma3.tempo                 = 600;
        programma3.potenza               = 15;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 40;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 300;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_dinamico = 1;

        programma4.posizionePaziente     = QString( tr("su di un lato"));
        programma4.posizioneNeutro       = QString( tr("gomito"));
        programma4.posizioneElettrodo    = QString( tr("deltoide e zona scapolare"));
        programma4.elettrodo             = QString( tr("DINAMICO 60"));
        programma4.tempo                 = 600;
        programma4.potenza               = 50;
        programma4.potenzaMinima         = 10;
        programma4.potenzaMassima        = 100;
        programma4.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma4.frequenza             = 690;
        programma4.frequenzaPemf         = 300;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;
        break;

    case ORTOPEDICI_GINOCCHIO:
        // QUARTA FASE MOBILIZZAZZIONE ATTIVA CON RESISTENZA
        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("zona lombare"));
        programma1.posizioneElettrodo    = QString( tr("retto femorale"));
        programma1.elettrodo             = QString( tr("STATICO piccolo"));
        programma1.tempo                 = 900;
        programma1.potenza               = 20;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;
        break;


    case NEUROLOGICI:
        // TRATTAMENTO LESIONI SPINALI PERIFERICHE

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("lombare"));
        programma1.posizioneElettrodo    = QString( tr("muscoli denervati"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 1200;
        programma1.potenza               = 20;
        programma1.potenzaMinima         = 5;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma1.frequenza             = 690;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}
void dialogElencoProtocolli::showProgrammi6()
{
    QString programma;
    programma = ui->button6->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        // RADICOLOPATIA FASE ACUTA

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("zona dolente"));
        programma1.elettrodo             = QString( tr("STATICO grande"));
        programma1.tempo                 = 1800;
        programma1.potenza               = 5;
        programma1.potenzaMinima         = 5;
        programma1.potenzaMassima        = 5;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_grande = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;

        break;
    case ORTOPEDICI:
        // PERIARTRITE DI SPALLA FASE SUB ACUTA O CRONICA ( ELETTRODI STATICI )

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("braccio"));
        programma1.posizioneElettrodo    = QString( tr("zona scapolare"));
        programma1.elettrodo             = QString( tr("RESISTIVO piccolo"));
        programma1.tempo                 = 600;
        programma1.potenza               = 15;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 50;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_piccolo = 1;

        programma2.posizionePaziente     = QString( tr("su di un lato"));
        programma2.posizioneNeutro       = QString( tr("gomito"));
        programma2.posizioneElettrodo    = QString( tr("deltoide e zona scapolare"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 600;
        programma2.potenza               = 50;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma2.frequenza             = 690;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("seduto"));
        programma3.posizioneNeutro       = QString( tr("avambraccio"));
        programma3.posizioneElettrodo    = QString( tr("bicipite brachiale"));
        programma3.elettrodo             = QString( tr("STATICO piccolo"));
        programma3.tempo                 = 600;
        programma3.potenza               = 15;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 50;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 300;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_statico_piccolo = 1;

        programma4.posizionePaziente     = QString( tr("su di un lato"));
        programma4.posizioneNeutro       = QString( tr("gomito"));
        programma4.posizioneElettrodo    = QString( tr("deltoide e zona scapolare"));
        programma4.elettrodo             = QString( tr("DINAMICO 60"));
        programma4.tempo                 = 600;
        programma4.potenza               = 50;
        programma4.potenzaMinima         = 10;
        programma4.potenzaMassima        = 100;
        programma4.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma4.frequenza             = 690;
        programma4.frequenzaPemf         = 300;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;
        break;

    case ORTOPEDICI_GINOCCHIO:
        // QUINTA FASE RECUPERO DEFICIT DI FLESSIONE
        programma1.posizionePaziente     = QString( tr("seduto"));
        programma1.posizioneNeutro       = QString( tr("bicipite femorale"));
        programma1.posizioneElettrodo    = QString( tr("inserzione vasto mediale,vasto laterale,retto femorale"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 900;
        programma1.potenza               = 20;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("seduto"));
        programma2.posizioneNeutro       = QString( tr("bicipite femorale"));
        programma2.posizioneElettrodo    = QString( tr("retto femorale"));
        programma2.elettrodo             = QString( tr("STATICO piccolo"));
        programma2.tempo                 = 900;
        programma2.potenza               = 20;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = false;
        programma4.enable = false;
        break;

    case NEUROLOGICI:

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);

    formSelezionaProgramma->showNormal();
}

void dialogElencoProtocolli::showProgrammi7()
{
    QString programma;
    programma = ui->button7->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        // BLOCCO ARTICOLARE / RIGIDITA

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("zona dolente"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 300;
        programma1.potenza               = 30;
        programma1.potenzaMinima         = 50;
        programma1.potenzaMassima        = 5;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("prono"));
        programma2.posizioneNeutro       = QString( tr("addome"));
        programma2.posizioneElettrodo    = QString( tr("zona dolente"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 900;
        programma2.potenza               = 10;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 10;
        programma2.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.resistivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prono"));
        programma3.posizioneNeutro       = QString( tr("addome"));
        programma3.posizioneElettrodo    = QString( tr("zona dolente"));
        programma3.elettrodo             = QString( tr("DINAMICO 60"));
        programma3.tempo                 = 300;
        programma3.potenza               = 20;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 100;
        programma3.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma3.frequenza             = 690;
        programma3.frequenzaPemf         = 300;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_dinamico = 1;

        programma4.posizionePaziente     = QString( tr("prono"));
        programma4.posizioneNeutro       = QString( tr("addome"));
        programma4.posizioneElettrodo    = QString( tr("zona dolente"));
        programma4.elettrodo             = QString( tr("STATICO piccolo"));
        programma4.tempo                 = 1800;
        programma4.potenza               = 10;
        programma4.potenzaMinima         = 10;
        programma4.potenzaMassima        = 100;
        programma4.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma4.frequenza             = 460;
        programma4.frequenzaPemf         = 300;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.resistivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;

        break;
    case ORTOPEDICI:
        // SPALLA FASE ACUTA

        programma1.posizionePaziente     = QString( tr("seduto"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("tricipite brachiale,sovraspinato,infraspinato,trapezio,pettorali,bicipite brachiale"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 40;
        programma1.potenzaMinima         = 5;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("seduto"));
        programma2.posizioneNeutro       = QString( tr("addome"));
        programma2.posizioneElettrodo    = QString( tr("tricipite brachiale,sovraspinato,infraspinato,trapezio,pettorali,bicipite brachiale"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 600;
        programma2.potenza               = 10;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 100;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.resistivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = false;
        programma4.enable = false;

        break;

    case ORTOPEDICI_GINOCCHIO:
        // SESTA FASE RECUPERO DEFICIT IN FLESSO/ESTENSIONE

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("retto femorale"));
        programma1.posizioneElettrodo    = QString( tr("bicipite femorale,polpaccio,tibiale"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 900;
        programma1.potenza               = 40;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;
        break;

    case NEUROLOGICI:

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}

void dialogElencoProtocolli::showProgrammi8()
{
    QString programma;
    programma = ui->button8->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        break;
    case ORTOPEDICI:
        // SPALLA FASE SUB ACUTA

        programma1.posizionePaziente     = QString( tr("seduto"));
        programma1.posizioneNeutro       = QString( tr("addome"));
        programma1.posizioneElettrodo    = QString( tr("tricipite brachiale,sovraspinato,infraspinato,trapezio,pettorali,bicipite brachiale"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 50;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 200;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("seduto"));
        programma2.posizioneNeutro       = QString( tr("addome"));
        programma2.posizioneElettrodo    = QString( tr("tricipite brachiale,sovraspinato,infraspinato,trapezio,pettorali,bicipite brachiale"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 600;
        programma2.potenza               = 15;
        programma2.potenzaMinima         = 5;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 200;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.resistivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = false;
        programma4.enable = false;

        break;

    case ORTOPEDICI_GINOCCHIO:
        // SESTA FASE RECUPERO MOBILITA ARTICOLARE

        programma1.posizionePaziente     = QString( tr("seduto"));
        programma1.posizioneNeutro       = QString( tr("bicipite femorale"));
        programma1.posizioneElettrodo    = QString( tr("retto femorale"));
        programma1.elettrodo             = QString( tr("STATICO piccolo"));
        programma1.tempo                 = 900;
        programma1.potenza               = 20;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = false;
        programma3.enable = false;
        programma4.enable = false;
        break;

    case NEUROLOGICI:

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}


void dialogElencoProtocolli::showProgrammi9()
{
    QString programma;
    programma = ui->button9->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        break;
    case ORTOPEDICI:
        // EPICONDILITE DA SOVRACCARICO

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("scapola"));
        programma1.posizioneElettrodo    = QString( tr("braccio e avambraccio"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 20;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("scapola"));
        programma2.posizioneElettrodo    = QString( tr("inserzione tendini"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 300;
        programma2.potenza               = 5;
        programma2.potenzaMinima         = 5;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 100;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.resistivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("supino"));
        programma3.posizioneNeutro       = QString( tr("scapola"));
        programma3.posizioneElettrodo    = QString( tr("avambraccio"));
        programma3.elettrodo             = QString( tr("STATICO piccolo"));
        programma3.tempo                 = 600;
        programma3.potenza               = 5;
        programma3.potenzaMinima         = 5;
        programma3.potenzaMassima        = 100;
        programma3.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = false;

        break;

    case NEUROLOGICI:

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}

void dialogElencoProtocolli::showProgrammi10()
{
    QString programma;
    programma = ui->button10->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        break;
    case ORTOPEDICI:
        // POLSO FASE ACUTA

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("braccio"));
        programma1.posizioneElettrodo    = QString( tr("polso"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 10;
        programma1.potenzaMinima         = 5;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("braccio"));
        programma2.posizioneElettrodo    = QString( tr("avambraccio"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 600;
        programma2.potenza               = 20;
        programma2.potenzaMinima         = 5;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 100;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("supino"));
        programma3.posizioneNeutro       = QString( tr("braccio"));
        programma3.posizioneElettrodo    = QString( tr("polso"));
        programma3.elettrodo             = QString( tr("STATICO piccolo"));
        programma3.tempo                 = 600;
        programma3.potenza               = 5;
        programma3.potenzaMinima         = 5;
        programma3.potenzaMassima        = 100;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_statico_piccolo = 1;

        programma4.posizionePaziente     = QString( tr("supino"));
        programma4.posizioneNeutro       = QString( tr("braccio"));
        programma4.posizioneElettrodo    = QString( tr("avambraccio"));
        programma4.elettrodo             = QString( tr("DINAMICO 60"));
        programma4.tempo                 = 600;
        programma4.potenza               = 20;
        programma4.potenzaMinima         = 5;
        programma4.potenzaMassima        = 100;
        programma4.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma4.frequenza             = 460;
        programma4.frequenzaPemf         = 100;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;

        break;

    case NEUROLOGICI:

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}


void dialogElencoProtocolli::showProgrammi11()
{
    QString programma;
    programma = ui->button11->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        break;
    case ORTOPEDICI:
        // POLSO FASE SUB ACUTA O CRONICA

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("braccio"));
        programma1.posizioneElettrodo    = QString( tr("polso"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 10;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("braccio"));
        programma2.posizioneElettrodo    = QString( tr("avambraccio"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 600;
        programma2.potenza               = 30;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("supino"));
        programma3.posizioneNeutro       = QString( tr("braccio"));
        programma3.posizioneElettrodo    = QString( tr("polso"));
        programma3.elettrodo             = QString( tr("STATICO piccolo"));
        programma3.tempo                 = 600;
        programma3.potenza               = 10;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 100;
        programma3.tipopTrattamnento     = QString( tr("RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 300;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_statico_piccolo = 1;

        programma4.posizionePaziente     = QString( tr("supino"));
        programma4.posizioneNeutro       = QString( tr("braccio"));
        programma4.posizioneElettrodo    = QString( tr("avambraccio"));
        programma4.elettrodo             = QString( tr("DINAMICO 60"));
        programma4.tempo                 = 600;
        programma4.potenza               = 30;
        programma4.potenzaMinima         = 10;
        programma4.potenzaMassima        = 100;
        programma4.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma4.frequenza             = 460;
        programma4.frequenzaPemf         = 300;
        programma4.sonda.val             = 0;
        programma4.sonda.tipo.capacitivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = true;

        break;

    case NEUROLOGICI:

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}

void dialogElencoProtocolli::showProgrammi12()
{
    QString programma;
    programma = ui->button12->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        break;
    case ORTOPEDICI:
        // COXOARTROSI FASE SUB-ACUTA O CRONICA

        programma1.posizionePaziente     = QString( tr("supino"));
        programma1.posizioneNeutro       = QString( tr("lombo sacrale"));
        programma1.posizioneElettrodo    = QString( tr("zona trocanterica e ischiatica"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 10;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 300;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.resistivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("supina"));
        programma2.posizioneNeutro       = QString( tr("braccio"));
        programma2.posizioneElettrodo    = QString( tr("coscia"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 300;
        programma2.potenza               = 20;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 300;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prona"));
        programma3.posizioneNeutro       = QString( tr("addome"));
        programma3.posizioneElettrodo    = QString( tr("gluteo e bicipite femorale"));
        programma3.elettrodo             = QString( tr("DINAMICO 60"));
        programma3.tempo                 = 300;
        programma3.potenza               = 20;
        programma3.potenzaMinima         = 10;
        programma3.potenzaMassima        = 100;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO" ) );
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 300;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = false;

        break;

    case NEUROLOGICI:

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}

void dialogElencoProtocolli::showProgrammi13()
{
    QString programma;
    programma = ui->button13->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        break;
    case ORTOPEDICI:
        // DISTORSIONE CAVIGLIA IN ACUTO

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("pianta del piede"));
        programma1.posizioneElettrodo    = QString( tr("gastrocnemius mediale e laterale e soleo"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 10;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr("CAPACITIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 100;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("supino"));
        programma2.posizioneNeutro       = QString( tr("gastrocnemius"));
        programma2.posizioneElettrodo    = QString( tr("dorso del piede"));
        programma2.elettrodo             = QString( tr("DINAMICO 60"));
        programma2.tempo                 = 300;
        programma2.potenza               = 5;
        programma2.potenzaMinima         = 5;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 100;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.capacitivo_dinamico = 1;

        programma3.posizionePaziente     = QString( tr("prono"));
        programma3.posizioneNeutro       = QString( tr("gastrocnemius pos"));
        programma3.posizioneElettrodo    = QString( tr("pianta del piede, soleo e gastrocnemius laterale"));
        programma3.elettrodo             = QString( tr("DINAMICO 60"));
        programma3.tempo                 = 300;
        programma3.potenza               = 5;
        programma3.potenzaMinima         = 5;
        programma3.potenzaMassima        = 100;
        programma3.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma3.frequenza             = 460;
        programma3.frequenzaPemf         = 100;
        programma3.sonda.val             = 0;
        programma3.sonda.tipo.resistivo_dinamico = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = true;
        programma4.enable = false;

        break;

    case NEUROLOGICI:

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}

void dialogElencoProtocolli::showProgrammi14()
{
    QString programma;
    programma = ui->button14->getText();

    switch (tipoProtocollo) {
    case GENERICI:
        break;
    case ORTOPEDICI:
        // DISTORSIONE CAVIGLIA FASE SUB ACUTA

        programma1.posizionePaziente     = QString( tr("prono"));
        programma1.posizioneNeutro       = QString( tr("pianta del piede"));
        programma1.posizioneElettrodo    = QString( tr("gastrocnemius mediale e laterale e soleo"));
        programma1.elettrodo             = QString( tr("DINAMICO 60"));
        programma1.tempo                 = 600;
        programma1.potenza               = 20;
        programma1.potenzaMinima         = 10;
        programma1.potenzaMassima        = 100;
        programma1.tipopTrattamnento     = QString( tr( "CAPACITIVO"));
        programma1.frequenza             = 460;
        programma1.frequenzaPemf         = 200;
        programma1.sonda.val             = 0;
        programma1.sonda.tipo.capacitivo_dinamico = 1;

        programma2.posizionePaziente     = QString( tr("prono"));
        programma2.posizioneNeutro       = QString( tr("gastrocnemius"));
        programma2.posizioneElettrodo    = QString( tr("pianta del piede"));
        programma2.elettrodo             = QString( tr("STATICO piccolo"));
        programma2.tempo                 = 300;
        programma2.potenza               = 10;
        programma2.potenzaMinima         = 10;
        programma2.potenzaMassima        = 100;
        programma2.tipopTrattamnento     = QString( tr( "RESISTIVO"));
        programma2.frequenza             = 460;
        programma2.frequenzaPemf         = 200;
        programma2.sonda.val             = 0;
        programma2.sonda.tipo.resistivo_statico_piccolo = 1;

        programma1.enable = true;
        programma2.enable = true;
        programma3.enable = false;
        programma4.enable = false;

        break;

    case NEUROLOGICI:

        break;
    default:
        break;
    }

    formSelezionaProgramma->setRotella( miaRotella );
    formSelezionaProgramma->setProgrammi( ui->labelTitolo->text() ,
                                          programma,
                                          &programma1,
                                          &programma2,
                                          &programma3,
                                          &programma4);


    formSelezionaProgramma->showNormal();
}


void dialogElencoProtocolli::rotellaClick()
{
//    esci();

    if(      this->focusWidget() == ui->buttonExit  ) esci();
    else if( this->focusWidget() == ui->button1     ) showProgrammi1();
    else if( this->focusWidget() == ui->button2     ) showProgrammi2();
    else if( this->focusWidget() == ui->button3     ) showProgrammi3();
    else if( this->focusWidget() == ui->button4     ) showProgrammi4();
    else if( this->focusWidget() == ui->button5     ) showProgrammi5();
    else if( this->focusWidget() == ui->button6     ) showProgrammi6();
    else if( this->focusWidget() == ui->button7     ) showProgrammi7();
    else if( this->focusWidget() == ui->button8     ) showProgrammi8();
    else if( this->focusWidget() == ui->button9     ) showProgrammi9();
    else if( this->focusWidget() == ui->button10    ) showProgrammi10();
    else if( this->focusWidget() == ui->button11    ) showProgrammi11();
    else if( this->focusWidget() == ui->button12    ) showProgrammi12();
    else if( this->focusWidget() == ui->button13    ) showProgrammi13();
    else if( this->focusWidget() == ui->button14    ) showProgrammi14();
    else if( this->focusWidget() == ui->buttonNext  && ui->buttonNext->isEnabled() == true ) showNext();
}

void dialogElencoProtocolli::rotellaMove(bool up)
{
    if( up ) {
        this->focusNextChild();
    }
    else
    {
        this->focusPreviousChild();
    }
}
void dialogElencoProtocolli::setRotella(Rotella *_rotella)
{
    miaRotella = _rotella;
    this->riprendoRotella();
}

void dialogElencoProtocolli::riprendoRotella()
{
    disconnect( miaRotella, SIGNAL(valueChange(bool)), 0, 0 );
    disconnect( miaRotella, SIGNAL(click()),           0, 0 );
    connect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    connect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));

    miaRotella->setEnableBeep( true );
    miaRotella->setStep(1);
    miaRotella->delegate = this;
}


void dialogElencoProtocolli::selezionatoProgramma(int prog)
{
    if( prog > 0 )
    {
        if(      prog == 1 ) emit( caricaProgramma(programma1) );
        else if( prog == 2 ) emit( caricaProgramma(programma2) );
        else if( prog == 3 ) emit( caricaProgramma(programma3) );
        else if( prog == 4 ) emit( caricaProgramma(programma4) );

        this->close();
    }
    else
        riprendoRotella();
}

void dialogElencoProtocolli::esci()
{
    if( tipoProtocollo == ORTOPEDICI_GINOCCHIO )
    {
        setProtocllo( ORTOPEDICI );
    }
    else
    {
        emit onclose();
        this->close();
    }
}
