#include "ufgbuttontastiera.h"
#include "ui_ufgbuttontastiera.h"
#include "QPainter"

ufgButtonTastiera::ufgButtonTastiera(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ufgButtonTastiera)
{
    ui->setupUi(this);

    borderColor      = QColor( 255 ,255 ,255 );
    backgroundColor1 = QColor( 255,50,0 );
    backgroundColor2 = QColor( 255,120,0);
    focusColor       = QColor( 255,0,0);

    premuto     = false;
    focus       = false;
    checkButton = false;

    ui->label->setGeometry(this->rect());
}

ufgButtonTastiera::~ufgButtonTastiera()
{
    delete ui;
}


void ufgButtonTastiera::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    QLinearGradient myGradient;
    QPen penPremuto( QColor( 10, 50 ,10 ),   2);
    QPen penBordo( borderColor , 2);
    QPen penFocus( focusColor  , 2);
    //QColor colore1 = QColor(255,255,255);

    QRect brect = this->rect();
    brect.setWidth( this->rect().width() - 2 );
    brect.setHeight( this->rect().height() - 2);
    brect.setLeft(2);
    brect.setTop(2);

    myGradient = QLinearGradient( QPointF(0,0),QPointF(1,0) );


    if( premuto == true)
    {
//        p.drawPixmap(this->rect(), QPixmap(":img/bottoneArancioDown.png" ).scaled( this->rect().size()));
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;

        path.addRoundedRect( brect , 2, 2);
        myGradient.setStart(  brect.size().width() / 2, 0 );
        myGradient.setFinalStop( brect.size().width() / 2, this->rect().size().height() );
        myGradient.setColorAt(0, backgroundColor1);
        myGradient.setColorAt(1, backgroundColor2);

        //p.setPen(penBordo);
        p.fillPath(path, myGradient );
        p.drawPath(path);
        p.setPen(penPremuto);
        p.drawRoundedRect( brect , 2, 2);
    }
    else if( focus == true )
    {
//        p.drawPixmap(this->rect(), QPixmap(":img/bottoneArancioUP.png" ).scaled( this->rect().size()));
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;
        path.addRoundedRect( brect , 2, 2);
        myGradient.setStart(  brect.size().width() / 2, 0 );
        myGradient.setFinalStop( brect.size().width() / 2, this->rect().size().height() );
//        myGradient.setColorAt(0,QColor(255,165,0));
//        myGradient.setColorAt(0.2,QColor(255,50,0));
//        myGradient.setColorAt(0.8,QColor(255,50,0));
//        myGradient.setColorAt(1,QColor(255,165,0));
        myGradient.setColorAt(1,backgroundColor1);
        myGradient.setColorAt(0,backgroundColor2);

        //p.setPen(pen);
        p.fillPath(path,myGradient);
        p.drawPath(path);
        p.setPen(penFocus);
        p.drawRoundedRect( brect , 2, 2);
    }
    else
    {
//        p.drawPixmap(this->rect(), QPixmap(":img/bottoneArancioUP.png" ).scaled( this->rect().size()));
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;
        path.addRoundedRect( brect , 2, 2);
        myGradient.setStart(  brect.size().width() / 2, 0 );
        myGradient.setFinalStop( brect.size().width() / 2, this->rect().size().height() );
//        myGradient.setColorAt(0,QColor(255,165,0));
//        myGradient.setColorAt(0.2,QColor(255,50,0));
//        myGradient.setColorAt(0.8,QColor(255,50,0));
//        myGradient.setColorAt(1,QColor(255,165,0));
        myGradient.setColorAt(1,backgroundColor1);
        myGradient.setColorAt(0,backgroundColor2);

        //p.setPen(pen);
        p.fillPath(path,myGradient);
        p.drawPath(path);
        p.setPen(penBordo);
        p.drawRoundedRect( brect , 2, 2);
    }
}

void ufgButtonTastiera::mousePressEvent(QMouseEvent* )
{

    if( this->checkButton == false )
    {
        premuto = true;
    }
    else
    {
        if( premuto == true ) premuto = false;
        else                  premuto = true;
    }
    this->update();
}
void ufgButtonTastiera::mouseReleaseEvent(QMouseEvent* )
{
    //QPainter painter(this);
    //painter.drawPixmap(this->rect(), QPixmap(":img/button_verde.png" ).scaled( this->rect().size()));

    if( this->checkButton == false )
    {
        premuto = false;
        this->update();
    }
    emit buttonClicked( this );
}

void ufgButtonTastiera::focusInEvent(QFocusEvent *)
{
    focus = true;
    this->update();
}

void ufgButtonTastiera::focusOutEvent(QFocusEvent *)
{
    focus = false;
    this->update();
}


void ufgButtonTastiera::setTasto(QString nome, tipoTasto _tipo)
{
    ui->label->setText(nome);
    tipo = _tipo;
}

void ufgButtonTastiera::setPixmap( QPixmap png)
{
    immagine = png;
}

void ufgButtonTastiera::setColors(QColor bg1, QColor bg2, QColor border, QColor focus)
{
    backgroundColor1 = bg1;
    backgroundColor2 = bg2;
    borderColor      = border;
    focusColor       = focus;

    this->update();
}

void ufgButtonTastiera::setChecked(bool check)
{
    checkButton = check;
}

QString ufgButtonTastiera::getText()
{
    return ui->label->text();
}

tipoTasto ufgButtonTastiera::getTipo()
{
    return tipo;
}

bool ufgButtonTastiera::isChecked()
{
    return premuto;
}
