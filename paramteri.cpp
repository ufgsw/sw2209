#include "paramteri.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

class paramteriData : public QSharedData
{
public:
    QJsonDocument jsonDoc;
    QJsonObject   jParametri;
    QJsonArray    jProgrammi;
    QJsonObject   jProgramma;

    bool trattamentoCapacitivo;
    bool trattamentoResistivo;
    bool frequenza450khz;
    bool frequenza680khz;
    bool elettrodiStatici;
    bool elettrodiDinamici;
    bool pemf100Hz;
    bool pemf200Hz;
    bool pemf300Hz;
};

paramteri::paramteri() : data(new paramteriData)
{

}

paramteri::paramteri(const paramteri &rhs) : data(rhs.data)
{

}

paramteri &paramteri::operator=(const paramteri &rhs)
{
    if (this != &rhs)
        data.operator=(rhs.data);
    return *this;
}

paramteri::~paramteri()
{

}

