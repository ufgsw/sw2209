#include "dialogwarning.h"
#include "ui_dialogwarning.h"

#include <QPainter>

dialogWarning::dialogWarning(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogWarning)
{
    ui->setupUi(this);
}

dialogWarning::~dialogWarning()
{
    delete ui;
}

void dialogWarning::setText(QString testo)
{
    ui->label->setText(testo);
}


void dialogWarning::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    QRect area = this->rect();

    area.setX(2);
    area.setY(2);
    area.setWidth( area.width()-2 );
    area.setHeight( area.height()-2 );


    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(4);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);

    painter.setPen(pen);

    painter.drawRoundRect( area ,10,10);
}
