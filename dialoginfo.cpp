#include "dialoginfo.h"
#include "ui_dialoginfo.h"

#include "QPainter"

dialogInfo::dialogInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogInfo)
{
    ui->setupUi(this);
    ui->buttonOk->setImage( ":/img/ico/ok.png", ":/img/ico/ok.png", ":/img/ico/ok.png" , 100 );

    connect( ui->buttonOk, SIGNAL(buttonClicked()), this, SLOT(esci()));

    ui->buttonOk->setFocus();
}

dialogInfo::~dialogInfo()
{
    delete ui;
}

void dialogInfo::setText(QString text)
{
    ui->textInfo->setText(text);// ->setPlainText(text);
}

void dialogInfo::setTitle(QString title)
{
    ui->labelTitle->setText(title);
}

void dialogInfo::paintEvent(QPaintEvent *e)
{
    //QPainterPath path;
    QPainter painter(this);
    QRect area = this->rect();

    area.setX(2);
    area.setY(2);
    area.setWidth( area.width()-2 );
    area.setHeight( area.height()-2 );


    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(4);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);


    painter.setPen(pen);

    //path.addRoundedRect( area, 15, 15);
    //painter.drawPath( path );
    //painter.fillPath( path , Qt::blue );
    painter.drawRoundRect( area ,4,4 );
}

void dialogInfo::setRotella(int rotella, int tasto)
{

}

void dialogInfo::setImageTitle(QString file)
{
    QPixmap pix = QPixmap( file);
    ui->labelTitle->setText("");
    ui->labelTitle->setPixmap(pix);
}

void dialogInfo::rotellaMove(bool up)
{
//    if( ui->selezioneA->focus ) ui->selezioneB->setFocus();
//    else ui->selezioneA->setFocus();

//    if(     this->focusWidget() == ui->selezioneA  ) selezionatoA();
//    else if(this->focusWidget() == ui->selezioneB  ) selezionatoB();
}

void dialogInfo::rotellaClick()
{
    esci();

//    if(     this->focusWidget() == ui->selezioneA  ) selezionatoA();
//    else if(this->focusWidget() == ui->selezioneB  ) selezionatoB();
//    else if(this->focusWidget() == ui->buttonOk    ) esci();
}

void dialogInfo::esci()
{
    emit onClose();
    close();
}
