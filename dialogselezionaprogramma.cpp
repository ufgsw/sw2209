#include "dialogselezionaprogramma.h"
#include "ui_dialogselezionaprogramma.h"

dialogSelezionaProgramma::dialogSelezionaProgramma(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogSelezionaProgramma)
{
    ui->setupUi(this);

    ui->buttonExit->setImage( ":/img/ico/return.png", ":/img/ico/return.png", ":/img/ico/return.png" ,100 );
    ui->buttonProgramma1->setImage( ":/img/ico/play.png", ":/img/ico/play.png", ":/img/ico/play.png" , 80 );
    ui->buttonProgramma2->setImage( ":/img/ico/play.png", ":/img/ico/play.png", ":/img/ico/play.png" , 80 );
    ui->buttonProgramma3->setImage( ":/img/ico/play.png", ":/img/ico/play.png", ":/img/ico/play.png" , 80 );
    ui->buttonProgramma4->setImage( ":/img/ico/play.png", ":/img/ico/play.png", ":/img/ico/play.png" , 80 );

    ui->buttonProgramma1->setVisible( false );
    ui->buttonProgramma2->setVisible( false );
    ui->buttonProgramma3->setVisible( false );
    ui->buttonProgramma4->setVisible( false );

    connect( ui->buttonExit,       SIGNAL(buttonClicked()), this, SLOT(esci()));

    mLicenza = false;
}

dialogSelezionaProgramma::~dialogSelezionaProgramma()
{

    delete ui;
}

void dialogSelezionaProgramma::setLicenzaStart(bool lic)
{
    mLicenza = lic;
    if( lic == true )
    {
//        ui->buttonProgramma1->setVisible( true );
//        ui->buttonProgramma2->setVisible( true );
//        ui->buttonProgramma3->setVisible( true );
//        ui->buttonProgramma4->setVisible( true );
        connect( ui->buttonProgramma1, SIGNAL(buttonClicked()), this, SLOT(selezionato1()) );
        connect( ui->buttonProgramma2, SIGNAL(buttonClicked()), this, SLOT(selezionato2()) );
        connect( ui->buttonProgramma3, SIGNAL(buttonClicked()), this, SLOT(selezionato3()) );
        connect( ui->buttonProgramma4, SIGNAL(buttonClicked()), this, SLOT(selezionato4()) );

        qDebug("Licenza ok");
    }
    else qDebug( "Licenza fail" );
}

void dialogSelezionaProgramma::showEvent(QShowEvent *)
{
    ui->labelPosizionePaziente1->setText( tr("Posizione Paziente :"));
    ui->labelPosizionePaziente2->setText( tr("Posizione Paziente :"));
    ui->labelPosizionePaziente3->setText( tr("Posizione Paziente :"));
    ui->labelPosizionePaziente4->setText( tr("Posizione Paziente :"));

    ui->labelPosizioneNeutra1->setText( tr("Posizione Neutro :"));
    ui->labelPosizioneNeutra2->setText( tr("Posizione Neutro :"));
    ui->labelPosizioneNeutra3->setText( tr("Posizione Neutro :"));
    ui->labelPosizioneNeutra4->setText( tr("Posizione Neutro :"));

    ui->labelPosizioneElettrodo1->setText( tr("Posizione Elettrodo :"));
    ui->labelPosizioneElettrodo2->setText( tr("Posizione Elettrodo :"));
    ui->labelPosizioneElettrodo3->setText( tr("Posizione Elettrodo :"));
    ui->labelPosizioneElettrodo4->setText( tr("Posizione Elettrodo :"));

    ui->labelElettrodo1->setText( tr("Elettrodo") );
    ui->labelElettrodo2->setText( tr("Elettrodo") );
    ui->labelElettrodo3->setText( tr("Elettrodo") );
    ui->labelElettrodo4->setText( tr("Elettrodo") );

    ui->textPosizioneElettrodo1->setWordWrap( true );
    ui->textPosizioneElettrodo2->setWordWrap( true );
    ui->textPosizioneElettrodo3->setWordWrap( true );
    ui->textPosizioneElettrodo4->setWordWrap( true );
}

void dialogSelezionaProgramma::setProgrammi( QString protocollo, QString programma, sProgramma *prog1, sProgramma *prog2, sProgramma *prog3, sProgramma *prog4)
{
    int mm,ss;

    QString tempo;//     = QString( tr("TEMPO") );
    QString potenza   = QString( tr("POTENZA") );
    QString modalita  = QString( tr("MODALITA") );
    QString frequenza = QString( tr("FREQUENZA") );
    QString slf       = QString( tr("SLF") );

    ui->buttonProgramma1->setVisible( false );
    ui->buttonProgramma2->setVisible( false );
    ui->buttonProgramma3->setVisible( false );
    ui->buttonProgramma4->setVisible( false );

    ui->labelProtocollo->setText( protocollo );
    ui->labelProogramma->setText( programma );


    ui->labelPosizionePaziente1->setVisible( prog1->enable );
    ui->textPosizionePaziente1->setVisible(  prog1->enable );
    ui->labelPosizioneNeutra1->setVisible(   prog1->enable );
    ui->textPosizioneNeutro1->setVisible(    prog1->enable );
    ui->labelPosizioneElettrodo1->setVisible(prog1->enable );
    ui->textPosizioneElettrodo1->setVisible( prog1->enable );
    ui->labelElettrodo1->setVisible(         prog1->enable );
    ui->textElettrodo1->setVisible(          prog1->enable );
    ui->textTempo1->setVisible(              prog1->enable );
    ui->textPotenza1->setVisible(            prog1->enable );
    ui->textModalita1->setVisible(           prog1->enable );
    ui->textFrequenza1->setVisible(          prog1->enable );
    ui->textSlf1->setVisible(                prog1->enable );

    ui->labelPosizionePaziente2->setVisible( prog2->enable );
    ui->textPosizionePaziente2->setVisible(  prog2->enable );
    ui->labelPosizioneNeutra2->setVisible(   prog2->enable );
    ui->textPosizioneNeutro2->setVisible(    prog2->enable );
    ui->labelPosizioneElettrodo2->setVisible(prog2->enable );
    ui->textPosizioneElettrodo2->setVisible( prog2->enable );
    ui->labelElettrodo2->setVisible(         prog2->enable );
    ui->textElettrodo2->setVisible(          prog2->enable );
    ui->textTempo2->setVisible(              prog2->enable );
    ui->textPotenza2->setVisible(            prog2->enable );
    ui->textModalita2->setVisible(           prog2->enable );
    ui->textFrequenza2->setVisible(          prog2->enable );
    ui->textSlf2->setVisible(                prog2->enable );

    ui->labelPosizionePaziente3->setVisible( prog3->enable );
    ui->textPosizionePaziente3->setVisible(  prog3->enable );
    ui->labelPosizioneNeutra3->setVisible(   prog3->enable );
    ui->textPosizioneNeutro3->setVisible(    prog3->enable );
    ui->labelPosizioneElettrodo3->setVisible(prog3->enable );
    ui->textPosizioneElettrodo3->setVisible( prog3->enable );
    ui->labelElettrodo3->setVisible(         prog3->enable );
    ui->textElettrodo3->setVisible(          prog3->enable );
    ui->textTempo3->setVisible(              prog3->enable );
    ui->textPotenza3->setVisible(            prog3->enable );
    ui->textModalita3->setVisible(           prog3->enable );
    ui->textFrequenza3->setVisible(          prog3->enable );
    ui->textSlf3->setVisible(                prog3->enable );

    ui->labelPosizionePaziente4->setVisible( prog4->enable );
    ui->textPosizionePaziente4->setVisible(  prog4->enable );
    ui->labelPosizioneNeutra4->setVisible(   prog4->enable );
    ui->textPosizioneNeutro4->setVisible(    prog4->enable );
    ui->labelPosizioneElettrodo4->setVisible(prog4->enable );
    ui->textPosizioneElettrodo4->setVisible( prog4->enable );
    ui->labelElettrodo4->setVisible(         prog4->enable );
    ui->textElettrodo4->setVisible(          prog4->enable );
    ui->textTempo4->setVisible(              prog4->enable );
    ui->textPotenza4->setVisible(            prog4->enable );
    ui->textModalita4->setVisible(           prog4->enable );
    ui->textFrequenza4->setVisible(          prog4->enable );
    ui->textSlf4->setVisible(                prog4->enable );

    if( prog1->enable )
    {
        ui->textPosizionePaziente1->setText( prog1->posizionePaziente );
        ui->textPosizioneNeutro1->setText( prog1->posizioneNeutro );
        ui->textPosizioneElettrodo1->setText( prog1->posizioneElettrodo );
        ui->textElettrodo1->setText( prog1->elettrodo );

        mm       = prog1->tempo / 60;
        ss       = prog1->tempo - ( mm * 60);
        tempo    = QString( tr("TEMPO")    ) + QString(" %1:%2").arg(mm,2,10,QLatin1Char('0')).arg(ss,2,10,QLatin1Char('0'));
        potenza  = QString( tr("POTENZA")  ) + QString( " %1 %").arg( prog1->potenza );
        modalita = QString( tr("MODALITA") ) + QString( " %1"  ).arg(prog1->tipopTrattamnento);
        if(      prog1->frequenza == 460     ) frequenza = QString( tr("FREQUENZA") ) + " 0.46 MHz";
        else if( prog1->frequenza == 690     ) frequenza = QString( tr("FREQUENZA") ) + " 0.69 MHz";
        if(      prog1->frequenzaPemf == 100 ) slf = QString( tr("SLF")) + " 100Hz";
        else if( prog1->frequenzaPemf == 200 ) slf = QString( tr("SLF")) + " 200Hz";
        else if( prog1->frequenzaPemf == 300 ) slf = QString( tr("SLF")) + " 300Hz";
        ui->textSlf1->setText( slf );
        ui->textTempo1->setText(     tempo     );
        ui->textPotenza1->setText(   potenza   );
        ui->textModalita1->setText(  modalita  );
        ui->textFrequenza1->setText( frequenza );
        ui->buttonProgramma1->setVisible( mLicenza );
    }

    if( prog2->enable )
    {
        ui->textPosizionePaziente2->setText( prog2->posizionePaziente );
        ui->textPosizioneNeutro2->setText( prog2->posizioneNeutro );
        ui->textPosizioneElettrodo2->setText( prog2->posizioneElettrodo );
        ui->textElettrodo2->setText( prog2->elettrodo );

        mm       = prog2->tempo / 60;
        ss       = prog2->tempo - ( mm * 60);
        tempo    = QString( tr("TEMPO") ) + QString(" %1:%2").arg(mm,2,10,QLatin1Char('0')).arg(ss,2,10,QLatin1Char('0'));
        potenza  = QString( tr("POTENZA") ) + QString( " %1 %").arg( prog2->potenza );
        modalita = QString( tr("MODALITA") ) + QString( " %1"  ).arg(prog2->tipopTrattamnento);
        if(      prog2->frequenza == 460 ) frequenza = QString( tr("FREQUENZA") ) + " 0.46 MHz";
        else if( prog2->frequenza == 690 ) frequenza = QString( tr("FREQUENZA") ) + " 0.69 MHz";
        if(      prog2->frequenzaPemf == 100 ) slf = QString( tr("SLF") ) + " 100Hz";
        else if( prog2->frequenzaPemf == 200 ) slf = QString( tr("SLF") ) + " 200Hz";
        else if( prog2->frequenzaPemf == 300 ) slf = QString( tr("SLF") ) + " 300Hz";
        ui->textSlf2->setText( slf );
        ui->textTempo2->setText(     tempo     );
        ui->textPotenza2->setText(   potenza   );
        ui->textModalita2->setText(  modalita  );
        ui->textFrequenza2->setText( frequenza );
        ui->buttonProgramma2->setVisible( mLicenza );
    }

    if( prog3->enable )
    {
        ui->textPosizionePaziente3->setText( prog3->posizionePaziente );
        ui->textPosizioneNeutro3->setText( prog3->posizioneNeutro );
        ui->textPosizioneElettrodo3->setText( prog3->posizioneElettrodo );
        ui->textElettrodo3->setText( prog3->elettrodo );

        mm       = prog3->tempo / 60;
        ss       = prog3->tempo - ( mm * 60);
        tempo    = QString( tr("TEMPO") ) + QString(" %1:%2").arg(mm,2,10,QLatin1Char('0')).arg(ss,2,10,QLatin1Char('0'));
        potenza  = QString( tr("POTENZA") ) + QString( " %1 %").arg( prog3->potenza );
        modalita = QString( tr("MODALITA") ) + QString( " %1"  ).arg(prog3->tipopTrattamnento);
        if(      prog3->frequenza == 460     ) frequenza = QString( tr("FREQUENZA") ) + " 0.46 MHz";
        else if( prog3->frequenza == 690     ) frequenza = QString( tr("FREQUENZA") ) + " 0.69 MHz";
        if(      prog3->frequenzaPemf == 100 ) slf = QString( tr("SLF") ) + " 100Hz";
        else if( prog3->frequenzaPemf == 200 ) slf = QString( tr("SLF") ) + " 200Hz";
        else if( prog3->frequenzaPemf == 300 ) slf = QString( tr("SLF") ) + " 300Hz";
        ui->textSlf3->setText( slf );
        ui->textTempo3->setText(     tempo     );
        ui->textPotenza3->setText(   potenza   );
        ui->textModalita3->setText(  modalita  );
        ui->textFrequenza3->setText( frequenza );
        ui->buttonProgramma3->setVisible( mLicenza );
    }

    if( prog4->enable )
    {
        ui->textPosizionePaziente4->setText( prog4->posizionePaziente );
        ui->textPosizioneNeutro4->setText( prog4->posizioneNeutro );
        ui->textPosizioneElettrodo4->setText( prog4->posizioneElettrodo );
        ui->textElettrodo4->setText( prog4->elettrodo );

        mm       = prog4->tempo / 60;
        ss       = prog4->tempo - ( mm * 60);
        tempo    = QString( tr("TEMPO") ) + QString(" %1:%2").arg(mm,2,10,QLatin1Char('0')).arg(ss,2,10,QLatin1Char('0'));
        potenza  = QString( tr("POTENZA") ) + QString( " %1 %").arg( prog4->potenza );
        modalita = QString( tr("MODALITA") ) + QString( " %1"  ).arg(prog4->tipopTrattamnento);
        if(      prog4->frequenza == 460     ) frequenza = QString( tr("FREQUENZA") ) + " 0.46 MHz";
        else if( prog4->frequenza == 690     ) frequenza = QString( tr("FREQUENZA") ) + " 0.69 MHz";
        if(      prog4->frequenzaPemf == 100 ) slf = QString( tr("SLF") ) + " 100Hz";
        else if( prog4->frequenzaPemf == 200 ) slf = QString( tr("SLF") ) + " 200Hz";
        else if( prog4->frequenzaPemf == 300 ) slf = QString( tr("SLF") ) + " 300Hz";
        ui->textSlf4->setText( slf );
        ui->textTempo4->setText(     tempo     );
        ui->textPotenza4->setText(   potenza   );
        ui->textModalita4->setText(  modalita  );
        ui->textFrequenza4->setText( frequenza );
        ui->buttonProgramma4->setVisible( mLicenza );
    }

}


void dialogSelezionaProgramma::rotellaClick()
{
    if(      this->focusWidget() == ui->buttonExit )
    {
        emit  onclose();
        this->close();
    }
    else if( this->focusWidget() == ui->buttonProgramma1    )
    {
        selezionato1();
    }
    else if( this->focusWidget() == ui->buttonProgramma2    )
    {
        selezionato2();
    }
    else if( this->focusWidget() == ui->buttonProgramma3    )
    {
        selezionato3();
    }
    else if( this->focusWidget() == ui->buttonProgramma4    )
    {
        selezionato4();
    }
}

void dialogSelezionaProgramma::rotellaMove(bool up)
{
//    if( ui->selezioneA->focus ) ui->selezioneB->setFocus();
//    else ui->selezioneA->setFocus();

//    if(     this->focusWidget() == ui->selezioneA  ) selezionatoA();
//    else if(this->focusWidget() == ui->selezioneB  ) selezionatoB();

    if( up ) {
        this->focusNextChild();
    }
    else
    {
        this->focusPreviousChild();
    }
}

void dialogSelezionaProgramma::setRotella(Rotella *_rotella)
{
    miaRotella = _rotella;

    disconnect( miaRotella, SIGNAL(valueChange(bool)), 0, 0 );
    disconnect( miaRotella, SIGNAL(click()),           0, 0 );
    connect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    connect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));

    miaRotella->setEnableBeep( true );
    miaRotella->setStep(1);
    miaRotella->delegate = this;
}

void dialogSelezionaProgramma::selezionato1()
{
    emit( programmaSelezionato(1) );
    this->close();
}
void dialogSelezionaProgramma::selezionato2()
{
    emit( programmaSelezionato(2) );
    this->close();
}
void dialogSelezionaProgramma::selezionato3()
{
    emit( programmaSelezionato(3) );
    this->close();
}
void dialogSelezionaProgramma::selezionato4()
{
    emit( programmaSelezionato(4) );
    this->close();
}
void dialogSelezionaProgramma::esci()
{
    emit onclose();
    this->close();
}
