#include "tastierinonumerico.h"
#include "ui_tastierinonumerico.h"

#include <QPainter>

tastierinoNumerico::tastierinoNumerico(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::tastierinoNumerico)
{
    ui->setupUi(this);

    ui->tasto0->setTasto( "0" , TASTO_NUMERICO );
    ui->tasto1->setTasto( "1" , TASTO_NUMERICO );
    ui->tasto2->setTasto( "2" , TASTO_NUMERICO );
    ui->tasto3->setTasto( "3" , TASTO_NUMERICO );
    ui->tasto4->setTasto( "4" , TASTO_NUMERICO );
    ui->tasto5->setTasto( "5" , TASTO_NUMERICO );
    ui->tasto6->setTasto( "6" , TASTO_NUMERICO );
    ui->tasto7->setTasto( "7" , TASTO_NUMERICO );
    ui->tasto8->setTasto( "8" , TASTO_NUMERICO );
    ui->tasto9->setTasto( "9" , TASTO_NUMERICO );

    ui->tastoenter->setTasto( "EN", TASTO_ENTER   );
    ui->tastopm->setTasto( "+/-",   TASTO_PIUMENO );
    ui->tastopunto->setTasto( ".",  TASTO_PUNTO   );
    ui->tastomin->setTasto( "<-",   TASTO_BACK     );
    ui->tastomax->setTasto( "CL",   TASTO_CLEAR     );
    //ui->tastocanc->setTasto( "x", TASTO_CLEAR     );

    ui->tastopunto->setEnabled(false);
    ui->tastopm->setEnabled(false);

    connect(ui->tasto0,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tasto1,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tasto2,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tasto3,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tasto4,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tasto5,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tasto6,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tasto7,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tasto8,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tasto9,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));

    connect(ui->tastoenter,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    //connect(ui->tastopm,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    //connect(ui->tastopunto,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tastomin,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    connect(ui->tastomax,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));
    //connect(ui->tastocanc,SIGNAL(buttonClicked(ddmButton*)),this,SLOT(tastoPremuto(ddmButton*)));

    primoTasto   = true;
    passwordMode = true;
}

tastierinoNumerico::~tastierinoNumerico()
{
    delete ui;
}

void tastierinoNumerico::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QRect area = this->rect();

    area.setX(2);
    area.setY(2);
    area.setWidth( area.width() -2 );
    area.setHeight( area.height()-2 );


    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(3);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);

    painter.setPen(pen);

    painter.drawRoundRect( area , 3 ,3  );
}

void tastierinoNumerico::tastoPremuto(ddmButton* tasto)
{
    QString copiaTesto;

    int     posizioneCursore;

    if( primoTasto == true )
    {
        primoTasto = false;
        this->ui->display->setStyleSheet("color: black");
        ui->display->setText( "" );
        testo = "";
    }

    //copiaTesto       = ui->display->text();
    //posizioneCursore = ui->display->cursorPosition();

    switch (tasto->getTipo()) {
    case TASTO_NUMERICO:
        testo.append( tasto->getText() );
        break;

    case TASTO_ENTER:
        //this->showanimation(rectStart,rectEnd);
        emit endEdit( copiaTesto );
        this->close();
        break;

    case TASTO_BACK:
        testo.remove( posizioneCursore -1, 1 );
        break;

    case TASTO_DEL:
        testo.remove( posizioneCursore , 1 );
        break;

    case TASTO_ESC:
        break;
    case TASTO_CLEAR:
        testo.clear();
        break;

    case TASTO_CURSORE_DX:
        break;

    case TASTO_CURSORE_SX:
        break;

    default:
        break;
    }

    if( passwordMode == true ) copiaTesto.fill( '*', testo.length() );
    else                       copiaTesto = testo;
    ui->display->setText(copiaTesto);
    ui->display->setFocus();
}

void tastierinoNumerico::setTitle(QString title)
{
    primoTasto = true;
    this->ui->display->setStyleSheet("color: lightgray");
    this->ui->display->setText( title );
}

QString tastierinoNumerico::getText()
{
    return testo;
}

