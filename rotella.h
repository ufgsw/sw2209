#ifndef ROTELLA_H
#define ROTELLA_H

#include <QObject>

class Rotella : public QObject
{
    Q_OBJECT
public:
    explicit Rotella(QObject *parent = 0);

    void setValue( int _rotella, int _tasto);
    void setDelegate(QWidget* pDelegate);
    void setStep(int _step);
    QWidget *delegate;

    int getVal()
    {
        return val;
    }
    int getDiff()
    {
        return diff;
    }
    void setEnableBeep( bool _en)
    {
        enableBeep = _en;
    }

private:

    int val;
    int valOld;
    int tasto;
    int tastoOld;
    int step;
    int diff;

    bool enableBeep;

    QWidget *delegateOld;

signals:
    void click();
    void valueChange(bool up);
    void beep(int tempo);
public slots:
};

#endif // ROTELLA_H
