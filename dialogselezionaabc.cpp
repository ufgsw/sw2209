#include "dialogselezionaabc.h"
#include "ui_dialogselezionaabc.h"

#include "QPainter"

dialogSelezionaabc::dialogSelezionaabc(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogSelezionaabc)
{
    ui->setupUi(this);

    ui->buttonOk->setImage(":/img/ico/ok.png",":/img/ico/ok.png",":/img/ico/ok.png", 100 );
    //ui->buttonOk->setBordoDimensione(2);

    ui->selezionaA->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->selezionaB->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->selezionaC->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );

    connect( ui->selezionaA,  SIGNAL(buttonClicked()), this, SLOT(selezionatoA()));
    connect( ui->selezionaB,  SIGNAL(buttonClicked()), this, SLOT(selezionatoB()));
    connect( ui->selezionaC,  SIGNAL(buttonClicked()), this, SLOT(selezionatoC()));

    connect( ui->buttonOk, SIGNAL(buttonClicked()), this, SLOT(esci()));


    //ui->selezionaA->setFocus();

}

dialogSelezionaabc::~dialogSelezionaabc()
{
    delete ui;
}

void dialogSelezionaabc::setVariabileA(bool *var, QString name)
{
    if( var != NULL )
    {
        variabileA = var;
        ui->selezionaA->setTitle(name);
        ui->selezionaA->setChecked(*variabileA);
    }
}

void dialogSelezionaabc::setVariabileB(bool *var, QString name)
{
    variabileB = var;
    ui->selezionaB->setTitle(name);
    ui->selezionaB->setChecked(*variabileB);
}

void dialogSelezionaabc::setVariabileC(bool *var, QString name)
{
    variabileC = var;
    ui->selezionaC->setTitle(name);
    ui->selezionaC->setChecked(*variabileC);
}

void dialogSelezionaabc::setTitle(QString title)
{
    ui->labelTitle->setText(title);
}

void dialogSelezionaabc::selezionatoA()
{
    ui->selezionaA->setChecked(true);
    ui->selezionaB->setChecked(false);
    ui->selezionaC->setChecked(false);
}
void dialogSelezionaabc::selezionatoB()
{
    ui->selezionaA->setChecked(false);
    ui->selezionaB->setChecked(true);
    ui->selezionaC->setChecked(false);
}
void dialogSelezionaabc::selezionatoC()
{
    ui->selezionaA->setChecked(false);
    ui->selezionaB->setChecked(false);
    ui->selezionaC->setChecked(true);
}

void dialogSelezionaabc::esci()
{
    if( variabileA != NULL ) *variabileA = ui->selezionaA->checked;
    if( variabileB != NULL ) *variabileB = ui->selezionaB->checked;
    if( variabileC != NULL ) *variabileC = ui->selezionaC->checked;
    this->close();
    emit onClose();
}

void dialogSelezionaabc::rotellaClick()
{
    esci();
}

void dialogSelezionaabc::rotellaMove(bool up)
{
    if(      ui->selezionaA->focus ) ui->selezionaB->setFocus();
    else if( ui->selezionaB->focus ) ui->selezionaC->setFocus();
    else                             ui->selezionaA->setFocus();

    if(     this->focusWidget() == ui->selezionaA  ) selezionatoA();
    else if(this->focusWidget() == ui->selezionaB  ) selezionatoB();
    else if(this->focusWidget() == ui->selezionaC  ) selezionatoC();

}

void dialogSelezionaabc::paintEvent(QPaintEvent *e)
{
    //QPainterPath path;
    QPainter painter(this);
    QRect area = this->rect();

    area.setX(2);
    area.setY(2);
    area.setWidth( area.width()-2 );
    area.setHeight( area.height()-2 );


    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(4);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);


    painter.setPen(pen);

    //path.addRoundedRect( area, 15, 15);
    //painter.drawPath( path );
    //painter.fillPath( path , Qt::blue );
    painter.drawRoundRect( area ,4,4 );
}

void dialogSelezionaabc::showEvent(QShowEvent *e)
{

}

