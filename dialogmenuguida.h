#ifndef DIALOGMENUGUIDA_H
#define DIALOGMENUGUIDA_H

#include <QDialog>
#include "dialogelencoprotocolli.h"
#include "diatermia.h"
#include "rotella.h"

namespace Ui {
class dialogMenuGuida;
}

class dialogMenuGuida : public QDialog
{
    Q_OBJECT

public:
    explicit dialogMenuGuida(QWidget *parent = 0);
    ~dialogMenuGuida();

    void setRotella( Rotella* _rotella);
    void setLicenza(bool lic)
    {
        mLicenza = lic;
    }

private:
    Ui::dialogMenuGuida *ui;
    Rotella* miaRotella;

    dialogElencoProtocolli* formElencoProtocolli;

    bool mLicenza;
    void showEvent(QShowEvent* );

signals:    
    void onClose();
    void beep(int tempo);
    void showElenco( int elenco );
    void caricaProgramma(sProgramma programma);

public slots:
    void riprendoRotella();
    void rotellaMove(bool up);
    void rotellaClick();

    void showGenerici();
    void showOrtopedici();
    void showNeurologici();

    void selezionatoProgramma(sProgramma programma);

private slots:
    void esci();

};

#endif // DIALOGMENUGUIDA_H
