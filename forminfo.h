#ifndef FORMINFO_H
#define FORMINFO_H

#include <QWidget>

namespace Ui {
class formInfo;
}

class formInfo : public QWidget
{
    Q_OBJECT

public:
    explicit formInfo(QWidget *parent = 0);
    ~formInfo();

    void setTextInfo( QString testo);

private:
    Ui::formInfo *ui;

protected:
    void paintEvent(QPaintEvent *event);
};

#endif // FORMINFO_H
