#include "ufgbutton1label.h"
#include "ui_ufgbutton1label.h"

#include "qpainter.h"

ufgButton1label::ufgButton1label(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ufgButton1label)
{
    ui->setupUi(this);

    bdColor  = QColor( 255 ,255 ,255 );
    bgColor2 = QColor( 255,  150,   0 );
    //bgColor2 = QColor( 50,  205,  50 );
    bgColor1 = QColor( 165,  42,  42 );

    focus   = false;
    premuto = false;

    mBorderSize = 4;
}

ufgButton1label::~ufgButton1label()
{
    delete ui;
}

void ufgButton1label::setFontSize(int size)
{
    mioFont = ui->labelTesto->font();
    mioFont.setPointSize( size );
    ui->labelTesto->setFont( mioFont );
}

void ufgButton1label::setText(QString text)
{
    ui->labelTesto->setText(text);
}
QString ufgButton1label::getText()
{
    return ui->labelTesto->text();
}

void ufgButton1label::setBackgroundColor( QColor col1, QColor col2)
{
    bgColor1 = col1;
    bgColor2 = col2;
    this->update();
}

void ufgButton1label::setBorderColor( QColor col)
{
    bdColor = col;
    this->update();
}

void ufgButton1label::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    QPen pen(      QColor( 255 ,150 ,150 ),  2);
    QPen penBordo( bdColor         , mBorderSize );
    //QPen penFocus( QColor(255,0,0) , mBorderSize );
    QPen penFocus( bgColor2 , mBorderSize );

    QRect brect = this->rect();
    brect.setWidth(  this->rect().width()  - (mBorderSize * 2) );
    brect.setHeight( this->rect().height() - (mBorderSize * 2) );

    brect.setLeft( mBorderSize );
    brect.setTop(  mBorderSize );

    //myGradient = QLinearGradient( QPointF(0,0),QPointF(1,0) );

    if( premuto == true)
    {
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;

        path.addRoundedRect( brect , 15, 15);
        p.setPen(pen);
        p.fillPath(path, bgColor2 );
        p.drawPath(path);
    }
    else if( focus == true )
    {
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;
        path.addRoundedRect( brect , 15, 15);

        p.setPen(pen);
        //p.fillPath(path,myGradient);
        p.fillPath(path,bgColor1);
        p.drawPath(path);
        p.setPen(penFocus);
        p.drawRoundedRect( brect , 15, 15);
    }
    else
    {
//        p.drawPixmap(this->rect(), QPixmap(":img/bottoneArancioUP.png" ).scaled( this->rect().size()));
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;
        path.addRoundedRect( brect , 15, 15);

        p.setPen(pen);
        p.fillPath(path,bgColor1);
        p.drawPath(path);
        p.setPen(penBordo);
        p.drawRoundedRect( brect , 15, 15);
    }

}



void ufgButton1label::mousePressEvent(QMouseEvent* )
{
//    QPainter painter(this);
//    painter.drawPixmap(this->rect(), QPixmap(":img/button_rosso.png" ).scaled( this->rect().size()));

    //this->setStyleSheet( "border-image { ");

    //this->set
    premuto = true;
    //qDebug( "premuto");

    this->update();


}
void ufgButton1label::mouseReleaseEvent(QMouseEvent* )
{
    //QPainter painter(this);
    //painter.drawPixmap(this->rect(), QPixmap(":img/button_verde.png" ).scaled( this->rect().size()));

    premuto = false;
    //qDebug( "rilasciato");
    this->update();

    emit buttonClicked();
}

void ufgButton1label::focusInEvent(QFocusEvent *)
{
    focus = true;
    this->update();
}

void ufgButton1label::focusOutEvent(QFocusEvent *)
{
    focus = false;
    this->update();
}

bool ufgButton1label::isFocused()
{
    return focus;
}

