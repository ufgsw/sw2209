#ifndef UFGTASTIERAFS_H
#define UFGTASTIERAFS_H

#include <QDialog>
#include "ufgbuttontastiera.h"
#include "ufgtastiera.h"

//enum tipoTastiera
//{
//    T_DEFAULT,
//    T_MINUSCOLE,
//    T_MAIUSCOLE,
//    T_ALT1,
//    T_ALT2
//};


namespace Ui {
class ufgTastieraFs;
}

class ufgTastieraFs : public QDialog
{
    Q_OBJECT

public:
    explicit ufgTastieraFs(QWidget *parent = 0);
    ~ufgTastieraFs();

    void defaultText(QString testo);

private:
    Ui::ufgTastieraFs *ui;

    tipoTastiera tipo;
    tipoTastiera oldTipo;

    bool visibile;
    bool shift;
    bool caps;

    void paintEvent(QPaintEvent *);

    void setFN(tipoTastiera _fn);

private slots:
    void tastoPremuto(ufgButtonTastiera* tasto);

signals:
    void endEdit(QString testo);

};

#endif // UFGTASTIERAFS_H
