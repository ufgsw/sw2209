#ifndef UFGPROGRESSBAR_H
#define UFGPROGRESSBAR_H

#include <QWidget>
#include "rotella.h"

namespace Ui {
class ufgProgressBar;
}

class ufgProgressBar : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(int value   READ value   WRITE setValue )
    Q_PROPERTY(int minimum READ minimum WRITE setMinimum )
    Q_PROPERTY(int maximum READ maximum WRITE setMaximum )
public:
    explicit ufgProgressBar(QWidget *parent = 0);
    ~ufgProgressBar();

    Rotella* rotella;

    void setColor( QColor col1, QColor col2, QColor cborder, QColor cfocus, QColor cselected);
    void setValue(int _value)
    {
        if( _value <= myMaximum )
        {
            myValue = _value;
            this->update();
        }
    }
    void setMinimum( int _minimum )
    {
        myMinimum = _minimum;
    }
    void setMaximum( int _maximum )
    {
        myMaximum = _maximum;
        if( myValue > myMaximum ) myValue = myMaximum;
    }
    void setIncrement( int incremento, int incrementoAccelerato )
    {
        myIncrementSet     = incremento;
        myIncrement        = incremento;
        myFastIncrement    = incrementoAccelerato;
        myFastIncrementSet = incrementoAccelerato;
    }

    int value()
    {
        return myValue;
    }
    int minimum()
    {
        return myMinimum;
    }
    int maximum()
    {
        return myMaximum;
    }

    bool focus;
    bool selected;
    bool enabled;


private:
    Ui::ufgProgressBar *ui;

    QTimer *timer1;

    int myValue;
    int myMinimum;
    int myMaximum;
    int myIncrement;
    int myIncrementSet;

    int myFastIncrement;
    int myFastIncrementSet;

    QColor backgroundColor1;
    QColor backgroundColor2;
    QColor borderColor;
    QColor focusColor;
    QColor selectedColor;

    void paintEvent(QPaintEvent *e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void mouseMoveEvent(QMouseEvent* event);

    void focusInEvent(QFocusEvent* e);
    void focusOutEvent(QFocusEvent* e);

public slots:
    void rotellaMove(bool up);
    void rotellaClick();
    void controlloIncremento();

signals:
    void valueChange( int value);

};

#endif // UFGPROGRESSBAR_H
