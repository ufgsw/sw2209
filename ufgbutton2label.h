#ifndef UFGBUTTON2LABEL_H
#define UFGBUTTON2LABEL_H

#include <QWidget>

namespace Ui {
class ufgButton2Label;
}

class ufgButton2Label : public QWidget
{
    Q_OBJECT

public:
    explicit ufgButton2Label(QWidget *parent = 0);
    ~ufgButton2Label();

    void setTitle( QString title);
    void setText(  QString text);
    void setBackgroundColor( QColor col1, QColor col2);
    void setBorderColor( QColor col);

    void setTextColor(QColor colore);
    void setTitleColor(QColor colore);

    bool isFocused();

private:
    Ui::ufgButton2Label *ui;

    QColor bgColor1;
    QColor bgColor2;
    QColor bdColor;

    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent* );
    void mouseReleaseEvent(QMouseEvent* );
    void focusInEvent(QFocusEvent* );
    void focusOutEvent(QFocusEvent* );

    bool focus;
    bool premuto;

signals:
    void buttonClicked();

};

#endif // UFGBUTTON2LABEL_H
