#ifndef UFGTESTOEDIT_H
#define UFGTESTOEDIT_H

#include <QWidget>
#include <QPropertyAnimation>
#include <QLineEdit>
#include <QTimer>

#include "ufgtastiera.h"
#include "ufgtastierafs.h"

class ufgTestoEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit ufgTestoEdit(QWidget *parent = 0);

private:
    void mousePressEvent(QMouseEvent* );
    void mouseReleaseEvent(QMouseEvent* );
    //void focusInEvent(QFocusEvent *e);
    void focusOutEvent(QFocusEvent *);

    void runTastiera();

    ufgTastiera* miaTastiera;


    QTimer* timer;

    QPropertyAnimation* anim1;
signals:
    void click(ufgTestoEdit* edit);

public slots:
    void tastieraChiusa();
    void tastieraFsChiusa(QString testo);
    void tastieraAperta();
    void fineEdit();

};

#endif // UFGTESTOEDIT_H
