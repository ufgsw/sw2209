#ifndef DIALODSELEZIONAAB_H
#define DIALODSELEZIONAAB_H

#include <QDialog>

namespace Ui {
class dialodSelezionaAB;
}

class dialodSelezionaAB : public QDialog
{
    Q_OBJECT

public:
    explicit dialodSelezionaAB(QWidget *parent = 0);
    ~dialodSelezionaAB();

    bool     animated;

    void setTitle(QString title);
    void setVariabileA( bool* var, QString name);
    void setVariabileB( bool* var, QString name);

    //void setRotella( Rotella* pRotella);

private:
    Ui::dialodSelezionaAB *ui;
    void paintEvent(QPaintEvent *e);
    void showEvent(QShowEvent* e);

    bool* variabileA;
    bool* variabileB;


public slots:
    void selezionatoA();
    void selezionatoB();
    void esci();

    void rotellaMove(bool up);
    void rotellaClick();

signals:
    void onClose();
};

#endif // DIALODSELEZIONAAB_H
