#-------------------------------------------------
#
# Project created by QtCreator 2015-10-30T15:27:57
#
#-------------------------------------------------

QT += core gui
QT += widgets
QT += sql

TRANSLATIONS = diatermia_it.ts \
               diatermia_en.ts \
               diatermia_cina.ts \
               diatermia_de.ts

TARGET = diatermia
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    ufgbutton2label.cpp \
    dialogsetvar.cpp \
    dialoginfo.cpp \
    dialodselezionaab.cpp \
    ufgcheckbox.cpp \
    ufgseriale.cpp \
    ufgprogressbar.cpp \
    rotella.cpp \
    ufgbutton.cpp \
    ufgbuttontastiera.cpp \
    dialogmenuguida.cpp \
    dialogselezionaabc.cpp \
    ufgbutton1label.cpp \
    tastierinonumerico.cpp \
    ddmbutton.cpp \
    dialogsetup.cpp \
    dialogelencoprotocolli.cpp \
    dialogselezionaprogramma.cpp \
    dialogstartcalibrazione.cpp \
    dialogwarning.cpp \
    dieloguser.cpp \
    diatermia.cpp \
    dialogselezionatrattamento.cpp \
    dialogorologio.cpp \
    ufgorologio.cpp

HEADERS  += mainwindow.h \
    ufgbutton2label.h \
    dialogsetvar.h \
    dialoginfo.h \
    dialodselezionaab.h \
    ufgcheckbox.h \
    ufgseriale.h \
    ufgprogressbar.h \
    rotella.h \
    ufgbutton.h \
    ufgbuttontastiera.h \
    dialogmenuguida.h \
    dialogselezionaabc.h \
    ufgbutton1label.h \
    tastierinonumerico.h \
    ddmbutton.h \
    dialogsetup.h \
    dialogelencoprotocolli.h \
    dialogselezionaprogramma.h \
    dialogstartcalibrazione.h \
    dialogwarning.h \
    dieloguser.h \
    diatermia.h \
    dialogselezionatrattamento.h \
    dialogorologio.h \
    ufgorologio.h

FORMS    += mainwindow.ui \
    ufgbutton2label.ui \
    dialogsetvar.ui \
    dialoginfo.ui \
    dialodselezionaab.ui \
    ufgcheckbox.ui \
    ufgprogressbar.ui \
    ufgbuttontastiera.ui \
    dialogmenuguida.ui \
    dialogselezionaabc.ui \
    ufgbutton1label.ui \
    tastierinonumerico.ui \
    ddmbutton.ui \
    dialogselezionaabcd.ui \
    dialogsetup.ui \
    dialogelencoprotocolli.ui \
    dialogselezionaprogramma.ui \
    dialogstartcalibrazione.ui \
    dialogwarning.ui \
    dieloguser.ui \
    dialogorologio.ui \
    ufgorologio.ui


unix {
target.path = /home/root
INSTALLS += target
}


RESOURCES += \
    risorse.qrc
