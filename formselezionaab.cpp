#include "formselezionaab.h"
#include "ui_formselezionaab.h"

#include "QPainter"

formSelezionaAB::formSelezionaAB(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::formSelezionaAB)
{
    ui->setupUi(this);
}

formSelezionaAB::~formSelezionaAB()
{
    delete ui;
}

void formSelezionaAB::setTitle(QString title)
{
    ui->labelTitolo->setText(title);
}

void formSelezionaAB::setVar1(QString nome, bool* var)
{
    variabileA = var;
}

void formSelezionaAB::setVar2(QString nome, bool* var)
{
    variabileB = var;
}

void formSelezionaAB::salva()
{
    if( variabileA != NULL && variabileB != NULL )
    {

    }
}

void formSelezionaAB::annulla()
{
    this->close();
}

void formSelezionaAB::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    QRect area = this->rect();

    area.setX(2);
    area.setY(2);
    area.setWidth( area.width()-2 );
    area.setHeight( area.height()-2 );


    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(4);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);

    painter.setPen(pen);

    painter.drawRoundRect( area ,15,15);

}
