#ifndef UFGBUTTONTASTIERA_H
#define UFGBUTTONTASTIERA_H

#include <QWidget>


enum tipoTasto
{
    TASTO_NUMERICO = 1,
    TASTO_ALFANUMERICO,
    TASTO_SPACE,
    TASTO_ENTER,
    TASTO_FN,
    TASTO_CAPS,
    TASTO_ESC,
    TASTO_CURSORE_DX,
    TASTO_CURSORE_SX,
    TASTO_UNDO,
    TASTO_DEL,
    TASTO_CLEAR,
    TASTO_BACK,
    TASTO_SHIFT
};

namespace Ui {
class ufgButtonTastiera;
}

class ufgButtonTastiera : public QWidget
{
    Q_OBJECT

public:
    explicit ufgButtonTastiera(QWidget *parent = 0);
    ~ufgButtonTastiera();

    void setChecked(bool check );
    void setTasto( QString nome, tipoTasto _tipo);
    void setPixmap( QPixmap png);
    void setColors(QColor bg1, QColor bg2, QColor border, QColor focus);
    bool isChecked();

    tipoTasto getTipo();
    QString getText();

private:
    Ui::ufgButtonTastiera *ui;

    bool premuto;
    bool focus;
    bool checkButton;

    QColor backgroundColor1;
    QColor backgroundColor2;
    QColor borderColor;
    QColor focusColor;

    QPixmap immagine;

    tipoTasto tipo;

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent* );
    void mouseReleaseEvent(QMouseEvent* );
    void focusInEvent(QFocusEvent* );
    void focusOutEvent(QFocusEvent* );

signals:
    void buttonClicked(ufgButtonTastiera *tasto);
};

#endif // UFGBUTTONTASTIERA_H
