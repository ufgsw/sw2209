#ifndef UFGOROLOGIO_H
#define UFGOROLOGIO_H

#include <QWidget>
#include <QTimer>

#include <QDateTime>
#include <QTime>

namespace Ui {
class ufgOrologio;
}

class ufgOrologio : public QWidget
{
    Q_OBJECT

public:
    explicit ufgOrologio(QWidget *parent = 0);
    ~ufgOrologio();


    void setOrario( int ore, int minuti, int secondi );
    void setAutoApdate( bool set) { mAutoUpdate = set; }

    int getOre()     { return mOre; }
    int getMinuti()  { return mMinuti; }
    int getSecondi() { return mSecondi; }

    void salva();

private:
    Ui::ufgOrologio *ui;

    void paintEvent(QPaintEvent *e);
    void showEvent(QShowEvent *event);

    QTimer*   timer;
    QTime     time;
    QDateTime dateTime;

    int mOre;
    int mMinuti;
    int mSecondi;

    bool mAutoUpdate;

private slots:

    void ridisegna();

};

#endif // UFGOROLOGIO_H
