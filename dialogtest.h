#ifndef DIALOGTEST_H
#define DIALOGTEST_H

#include <QDialog>
#include "qcgaugewidget.h"
#include "keyboard/widgetKeyBoard.h"
#include "ufgtastiera.h"


namespace Ui {
class DialogTest;
}

class DialogTest : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTest(QWidget *parent = 0);
    ~DialogTest();

    widgetKeyBoard * getKeyboard();
private slots:
    void on_buttonShowTastiera_clicked();

private:
    Ui::DialogTest *ui;

    ufgTastiera* miaTastiera;

    QcGaugeWidget* mAirspeedGauge;
    QcGaugeWidget* mCompassGauge;
    QcNeedleItem*  mAirspeedNeedle;
    QcNeedleItem*  mCompassNeedle;

    widgetKeyBoard* tastiera;
};

#endif // DIALOGTEST_H
