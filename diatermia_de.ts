<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>DialogOrologio</name>
    <message>
        <location filename="dialogorologio.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="12"/>
        <source>Orologio</source>
        <translation>UHR</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="82"/>
        <source>Ore</source>
        <translation>Stunden</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="98"/>
        <source>Minuti</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="114"/>
        <source>Secondi</source>
        <translation>Sekunden</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="36"/>
        <source>Diatermia</source>
        <translation>DIATHERMIE</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="42"/>
        <location filename="mainwindow.cpp" line="1828"/>
        <source>MODALITA&apos;</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="43"/>
        <location filename="mainwindow.cpp" line="815"/>
        <location filename="mainwindow.cpp" line="1829"/>
        <source>FREQUENZA</source>
        <translation>FREQUENZ</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="49"/>
        <location filename="mainwindow.cpp" line="1835"/>
        <source>IMPOSTAZIONI</source>
        <translation>EINSTELLUNGEN</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <location filename="mainwindow.cpp" line="337"/>
        <location filename="mainwindow.cpp" line="587"/>
        <location filename="mainwindow.cpp" line="608"/>
        <location filename="mainwindow.cpp" line="1855"/>
        <location filename="mainwindow.cpp" line="1870"/>
        <source>STATICO GRANDE</source>
        <translation>große statische</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="323"/>
        <location filename="mainwindow.cpp" line="344"/>
        <location filename="mainwindow.cpp" line="594"/>
        <location filename="mainwindow.cpp" line="615"/>
        <location filename="mainwindow.cpp" line="1850"/>
        <location filename="mainwindow.cpp" line="1875"/>
        <source>STATICO PICCOLO</source>
        <translation>kleine statische</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>FREQUENZA PEMF</source>
        <translation>SUPER LOW FREQUENCY</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="45"/>
        <location filename="mainwindow.cpp" line="867"/>
        <location filename="mainwindow.cpp" line="1831"/>
        <source>POTENZA</source>
        <translation>POWER</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="44"/>
        <location filename="mainwindow.cpp" line="1830"/>
        <source>FREQUENZA LOW</source>
        <translation>SUPER LOW FREQUENCY</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="46"/>
        <location filename="mainwindow.cpp" line="732"/>
        <location filename="mainwindow.cpp" line="1832"/>
        <source>TEMPO</source>
        <translation>ZEIT</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="47"/>
        <location filename="mainwindow.cpp" line="1833"/>
        <source>GUIDA</source>
        <translation>HILFE</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <location filename="mainwindow.cpp" line="520"/>
        <location filename="mainwindow.cpp" line="1834"/>
        <source>INFO</source>
        <translation>INFORMATIONEN</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="153"/>
        <location filename="mainwindow.cpp" line="1022"/>
        <location filename="mainwindow.cpp" line="1878"/>
        <source>Versione</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="308"/>
        <location filename="mainwindow.cpp" line="315"/>
        <location filename="mainwindow.cpp" line="322"/>
        <location filename="mainwindow.cpp" line="579"/>
        <location filename="mainwindow.cpp" line="586"/>
        <location filename="mainwindow.cpp" line="593"/>
        <location filename="mainwindow.cpp" line="1839"/>
        <location filename="mainwindow.cpp" line="1844"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <location filename="mainwindow.cpp" line="1854"/>
        <source>CAPACITIVO</source>
        <translation>KAPAZITIVE</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="309"/>
        <location filename="mainwindow.cpp" line="330"/>
        <location filename="mainwindow.cpp" line="580"/>
        <location filename="mainwindow.cpp" line="601"/>
        <location filename="mainwindow.cpp" line="1840"/>
        <location filename="mainwindow.cpp" line="1845"/>
        <location filename="mainwindow.cpp" line="1860"/>
        <location filename="mainwindow.cpp" line="1865"/>
        <source>DINAMICO</source>
        <translation>DYNAMISCH</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="329"/>
        <location filename="mainwindow.cpp" line="336"/>
        <location filename="mainwindow.cpp" line="343"/>
        <location filename="mainwindow.cpp" line="600"/>
        <location filename="mainwindow.cpp" line="607"/>
        <location filename="mainwindow.cpp" line="614"/>
        <location filename="mainwindow.cpp" line="1859"/>
        <location filename="mainwindow.cpp" line="1864"/>
        <location filename="mainwindow.cpp" line="1869"/>
        <location filename="mainwindow.cpp" line="1874"/>
        <source>RESISTIVO</source>
        <translation>RESISTIV</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="521"/>
        <source>desrizione info</source>
        <translation>B-ENERGY ist ein medizinisches Gerät, Missbrauch Schädigung des Patienten führen kann, bestätigen Sie, dass Sie das Handbuch gelesen haben und beobachtet haben, die oben angeführten Vorsichtsmaßnahmen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="539"/>
        <location filename="mainwindow.cpp" line="560"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="764"/>
        <source>MODALITA</source>
        <translation>MODE</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="897"/>
        <source>DINAMICI</source>
        <translation>DYNAMIC</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="898"/>
        <source>STATICI</source>
        <translation>STATIC</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="972"/>
        <source>CALIBRAZIONE TERMINATA</source>
        <translation>KALIBRIERUNG ABGESCHLOSSEN</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="990"/>
        <source>CALIBRAZIONE IN CORSO</source>
        <translation>KALIBRIERUNG LÄUFT</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1600"/>
        <source>descrizione allarme B001</source>
        <translation>Hardware Error B001( Interne Kommunikationsfehler an das System )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1613"/>
        <source>descrizione allarme A001</source>
        <translation>Hardware Error A001 ( Das System wird durch Kurzschluss erkannt )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1618"/>
        <location filename="mainwindow.cpp" line="1628"/>
        <location filename="mainwindow.cpp" line="1633"/>
        <location filename="mainwindow.cpp" line="1648"/>
        <location filename="mainwindow.cpp" line="1653"/>
        <location filename="mainwindow.cpp" line="1685"/>
        <source>descrizione allarme B002</source>
        <translation>Hardware Error B002 ( Generisches Hardware-Fehler )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1623"/>
        <source>descrizione allarme A003</source>
        <translation>Hardware Error A003 ( Das System hat einen anomalen Stromverbrauch )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1638"/>
        <source>descrizione allarme A004</source>
        <translation>Hardware Error A004 ( Das System hat eine abnorme Spannung )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1643"/>
        <source>descrizione allarme A002</source>
        <translation>Hardware Error A002 ( Das System hat eine Überhitzung erkannt )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1658"/>
        <source>descrizione allarme A005</source>
        <translation>Hardware Error A005 ( Das System hat einen anormalen aktuellen Patienten )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1675"/>
        <source>descrizione allarme A006</source>
        <translation>Hardware Error A006 ( Das System hat einen anormalen Betriebsfrequenz )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1680"/>
        <location filename="mainwindow.cpp" line="1745"/>
        <source>descrizione warning elettrodo massa</source>
        <translation>Aufmerksamkeit : Masseelektrode getrennt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1690"/>
        <source>descrizione warning mano scollegata</source>
        <translation>Aufmerksamkeit : Notknopf offline</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1771"/>
        <source>Verifica contatto sonda</source>
        <translation>Überprüfen Sie die Sonde Kontakt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1665"/>
        <location filename="mainwindow.cpp" line="1697"/>
        <location filename="mainwindow.cpp" line="1753"/>
        <source>descrizione warning capacitivo</source>
        <translation>Aufmerksamkeit : Kapazitive Elektrode getrennt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1669"/>
        <location filename="mainwindow.cpp" line="1702"/>
        <location filename="mainwindow.cpp" line="1758"/>
        <source>descrizione warning resistivo</source>
        <translation>Aufmerksamkeit : Resistive Elektrode getrennt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1765"/>
        <source>descrizione allarme emergenza</source>
        <translation>Aufmerksamkeit : Überprüfung der Notfall-Taste des Patienten</translation>
    </message>
</context>
<context>
    <name>dialogElencoProtocolli</name>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="97"/>
        <source>PROTOCOLLI GENERICI</source>
        <translation>GENERAL PROTOKOLLE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="99"/>
        <source>FASE ACUTA</source>
        <translation>AKUT PHASE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="100"/>
        <source>FASE CRONICA</source>
        <translation>Chronische Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="101"/>
        <source>RIDUZIONE DEL DOLORE</source>
        <translation>Schmerzreduktion</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="102"/>
        <source>CONTRATTURA</source>
        <translation>Kontraktur</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="103"/>
        <source>LINFODRENAGGIO</source>
        <translation>Lymphodränage</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="104"/>
        <source>RADICOLOPATIA FASE ACUTA</source>
        <translation>Radiculopathy AKUT PHASE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="105"/>
        <source>BLOCCO ARTICOLARE / RIGIDITA</source>
        <translation>Blockfuge / STEIFIGKEIT</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="126"/>
        <source>PROTOCOLLI PER PATOLOGIA</source>
        <translation>PROTOKOLLE FÜR KRANKHEIT</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="192"/>
        <source>PROTOCOLLI NEUROLOGICI</source>
        <translation>PROTOKOLLE KREBS</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="200"/>
        <source>MIELO LESIONE PARZIALE</source>
        <translation>Mielo PARTIAL VERLETZUNGS</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="201"/>
        <source>TRATTAMENTO POST ICTUS</source>
        <translation>Behandlung nach STROKE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="202"/>
        <source>LESIONI NERVOSE PERIFERICHE ( ERNIE DISCALI )</source>
        <translation>Verletzung peripherer Nerven (Bandscheibenvorfall )</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="203"/>
        <source>LESIONI NERVOSE PERIFERICHE (PLESSO BRACHIALE )</source>
        <translation>Verletzung peripherer Nerven (Plexus brachialis)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="230"/>
        <location filename="dialogelencoprotocolli.cpp" line="244"/>
        <location filename="dialogelencoprotocolli.cpp" line="258"/>
        <location filename="dialogelencoprotocolli.cpp" line="272"/>
        <location filename="dialogelencoprotocolli.cpp" line="322"/>
        <location filename="dialogelencoprotocolli.cpp" line="452"/>
        <location filename="dialogelencoprotocolli.cpp" line="466"/>
        <location filename="dialogelencoprotocolli.cpp" line="480"/>
        <location filename="dialogelencoprotocolli.cpp" line="494"/>
        <location filename="dialogelencoprotocolli.cpp" line="662"/>
        <location filename="dialogelencoprotocolli.cpp" line="676"/>
        <location filename="dialogelencoprotocolli.cpp" line="690"/>
        <location filename="dialogelencoprotocolli.cpp" line="704"/>
        <location filename="dialogelencoprotocolli.cpp" line="755"/>
        <location filename="dialogelencoprotocolli.cpp" line="856"/>
        <location filename="dialogelencoprotocolli.cpp" line="870"/>
        <location filename="dialogelencoprotocolli.cpp" line="884"/>
        <location filename="dialogelencoprotocolli.cpp" line="1052"/>
        <location filename="dialogelencoprotocolli.cpp" line="1066"/>
        <location filename="dialogelencoprotocolli.cpp" line="1221"/>
        <location filename="dialogelencoprotocolli.cpp" line="1369"/>
        <location filename="dialogelencoprotocolli.cpp" line="1383"/>
        <location filename="dialogelencoprotocolli.cpp" line="1397"/>
        <location filename="dialogelencoprotocolli.cpp" line="1411"/>
        <location filename="dialogelencoprotocolli.cpp" line="1472"/>
        <location filename="dialogelencoprotocolli.cpp" line="1956"/>
        <location filename="dialogelencoprotocolli.cpp" line="1984"/>
        <location filename="dialogelencoprotocolli.cpp" line="2035"/>
        <location filename="dialogelencoprotocolli.cpp" line="2049"/>
        <source>prono</source>
        <translation>anfällig</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="231"/>
        <location filename="dialogelencoprotocolli.cpp" line="245"/>
        <location filename="dialogelencoprotocolli.cpp" line="259"/>
        <location filename="dialogelencoprotocolli.cpp" line="273"/>
        <location filename="dialogelencoprotocolli.cpp" line="453"/>
        <location filename="dialogelencoprotocolli.cpp" line="467"/>
        <location filename="dialogelencoprotocolli.cpp" line="481"/>
        <location filename="dialogelencoprotocolli.cpp" line="495"/>
        <location filename="dialogelencoprotocolli.cpp" line="517"/>
        <location filename="dialogelencoprotocolli.cpp" line="531"/>
        <location filename="dialogelencoprotocolli.cpp" line="663"/>
        <location filename="dialogelencoprotocolli.cpp" line="677"/>
        <location filename="dialogelencoprotocolli.cpp" line="691"/>
        <location filename="dialogelencoprotocolli.cpp" line="705"/>
        <location filename="dialogelencoprotocolli.cpp" line="857"/>
        <location filename="dialogelencoprotocolli.cpp" line="871"/>
        <location filename="dialogelencoprotocolli.cpp" line="885"/>
        <location filename="dialogelencoprotocolli.cpp" line="1053"/>
        <location filename="dialogelencoprotocolli.cpp" line="1067"/>
        <location filename="dialogelencoprotocolli.cpp" line="1222"/>
        <location filename="dialogelencoprotocolli.cpp" line="1370"/>
        <location filename="dialogelencoprotocolli.cpp" line="1384"/>
        <location filename="dialogelencoprotocolli.cpp" line="1398"/>
        <location filename="dialogelencoprotocolli.cpp" line="1412"/>
        <location filename="dialogelencoprotocolli.cpp" line="1435"/>
        <location filename="dialogelencoprotocolli.cpp" line="1449"/>
        <location filename="dialogelencoprotocolli.cpp" line="1523"/>
        <location filename="dialogelencoprotocolli.cpp" line="1537"/>
        <location filename="dialogelencoprotocolli.cpp" line="1906"/>
        <source>addome</source>
        <translation>Bauch</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="232"/>
        <location filename="dialogelencoprotocolli.cpp" line="246"/>
        <location filename="dialogelencoprotocolli.cpp" line="260"/>
        <location filename="dialogelencoprotocolli.cpp" line="274"/>
        <location filename="dialogelencoprotocolli.cpp" line="454"/>
        <location filename="dialogelencoprotocolli.cpp" line="468"/>
        <location filename="dialogelencoprotocolli.cpp" line="482"/>
        <location filename="dialogelencoprotocolli.cpp" line="496"/>
        <location filename="dialogelencoprotocolli.cpp" line="664"/>
        <location filename="dialogelencoprotocolli.cpp" line="678"/>
        <location filename="dialogelencoprotocolli.cpp" line="692"/>
        <location filename="dialogelencoprotocolli.cpp" line="706"/>
        <location filename="dialogelencoprotocolli.cpp" line="858"/>
        <location filename="dialogelencoprotocolli.cpp" line="872"/>
        <location filename="dialogelencoprotocolli.cpp" line="886"/>
        <location filename="dialogelencoprotocolli.cpp" line="1054"/>
        <location filename="dialogelencoprotocolli.cpp" line="1068"/>
        <location filename="dialogelencoprotocolli.cpp" line="1223"/>
        <location filename="dialogelencoprotocolli.cpp" line="1371"/>
        <location filename="dialogelencoprotocolli.cpp" line="1385"/>
        <location filename="dialogelencoprotocolli.cpp" line="1399"/>
        <location filename="dialogelencoprotocolli.cpp" line="1413"/>
        <source>zona dolente</source>
        <translation>schmerzenden Bereich</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="233"/>
        <location filename="dialogelencoprotocolli.cpp" line="247"/>
        <location filename="dialogelencoprotocolli.cpp" line="311"/>
        <location filename="dialogelencoprotocolli.cpp" line="325"/>
        <location filename="dialogelencoprotocolli.cpp" line="339"/>
        <location filename="dialogelencoprotocolli.cpp" line="455"/>
        <location filename="dialogelencoprotocolli.cpp" line="469"/>
        <location filename="dialogelencoprotocolli.cpp" line="519"/>
        <location filename="dialogelencoprotocolli.cpp" line="533"/>
        <location filename="dialogelencoprotocolli.cpp" line="621"/>
        <location filename="dialogelencoprotocolli.cpp" line="665"/>
        <location filename="dialogelencoprotocolli.cpp" line="679"/>
        <location filename="dialogelencoprotocolli.cpp" line="730"/>
        <location filename="dialogelencoprotocolli.cpp" line="744"/>
        <location filename="dialogelencoprotocolli.cpp" line="758"/>
        <location filename="dialogelencoprotocolli.cpp" line="772"/>
        <location filename="dialogelencoprotocolli.cpp" line="859"/>
        <location filename="dialogelencoprotocolli.cpp" line="873"/>
        <location filename="dialogelencoprotocolli.cpp" line="924"/>
        <location filename="dialogelencoprotocolli.cpp" line="952"/>
        <location filename="dialogelencoprotocolli.cpp" line="1055"/>
        <location filename="dialogelencoprotocolli.cpp" line="1092"/>
        <location filename="dialogelencoprotocolli.cpp" line="1106"/>
        <location filename="dialogelencoprotocolli.cpp" line="1120"/>
        <location filename="dialogelencoprotocolli.cpp" line="1134"/>
        <location filename="dialogelencoprotocolli.cpp" line="1180"/>
        <location filename="dialogelencoprotocolli.cpp" line="1261"/>
        <location filename="dialogelencoprotocolli.cpp" line="1289"/>
        <location filename="dialogelencoprotocolli.cpp" line="1311"/>
        <location filename="dialogelencoprotocolli.cpp" line="1372"/>
        <location filename="dialogelencoprotocolli.cpp" line="1386"/>
        <location filename="dialogelencoprotocolli.cpp" line="1400"/>
        <location filename="dialogelencoprotocolli.cpp" line="1437"/>
        <location filename="dialogelencoprotocolli.cpp" line="1451"/>
        <location filename="dialogelencoprotocolli.cpp" line="1475"/>
        <location filename="dialogelencoprotocolli.cpp" line="1525"/>
        <location filename="dialogelencoprotocolli.cpp" line="1539"/>
        <location filename="dialogelencoprotocolli.cpp" line="1614"/>
        <location filename="dialogelencoprotocolli.cpp" line="1628"/>
        <location filename="dialogelencoprotocolli.cpp" line="1693"/>
        <location filename="dialogelencoprotocolli.cpp" line="1707"/>
        <location filename="dialogelencoprotocolli.cpp" line="1735"/>
        <location filename="dialogelencoprotocolli.cpp" line="1787"/>
        <location filename="dialogelencoprotocolli.cpp" line="1801"/>
        <location filename="dialogelencoprotocolli.cpp" line="1829"/>
        <location filename="dialogelencoprotocolli.cpp" line="1880"/>
        <location filename="dialogelencoprotocolli.cpp" line="1894"/>
        <location filename="dialogelencoprotocolli.cpp" line="1908"/>
        <location filename="dialogelencoprotocolli.cpp" line="1959"/>
        <location filename="dialogelencoprotocolli.cpp" line="1973"/>
        <location filename="dialogelencoprotocolli.cpp" line="1987"/>
        <location filename="dialogelencoprotocolli.cpp" line="2038"/>
        <source>DINAMICO 60</source>
        <translation>dynamisch 60</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="238"/>
        <location filename="dialogelencoprotocolli.cpp" line="266"/>
        <location filename="dialogelencoprotocolli.cpp" line="302"/>
        <location filename="dialogelencoprotocolli.cpp" line="330"/>
        <location filename="dialogelencoprotocolli.cpp" line="390"/>
        <location filename="dialogelencoprotocolli.cpp" line="460"/>
        <location filename="dialogelencoprotocolli.cpp" line="488"/>
        <location filename="dialogelencoprotocolli.cpp" line="524"/>
        <location filename="dialogelencoprotocolli.cpp" line="560"/>
        <location filename="dialogelencoprotocolli.cpp" line="584"/>
        <location filename="dialogelencoprotocolli.cpp" line="598"/>
        <location filename="dialogelencoprotocolli.cpp" line="612"/>
        <location filename="dialogelencoprotocolli.cpp" line="670"/>
        <location filename="dialogelencoprotocolli.cpp" line="698"/>
        <location filename="dialogelencoprotocolli.cpp" line="735"/>
        <location filename="dialogelencoprotocolli.cpp" line="763"/>
        <location filename="dialogelencoprotocolli.cpp" line="822"/>
        <location filename="dialogelencoprotocolli.cpp" line="878"/>
        <location filename="dialogelencoprotocolli.cpp" line="915"/>
        <location filename="dialogelencoprotocolli.cpp" line="943"/>
        <location filename="dialogelencoprotocolli.cpp" line="980"/>
        <location filename="dialogelencoprotocolli.cpp" line="1017"/>
        <location filename="dialogelencoprotocolli.cpp" line="1097"/>
        <location filename="dialogelencoprotocolli.cpp" line="1125"/>
        <location filename="dialogelencoprotocolli.cpp" line="1229"/>
        <location filename="dialogelencoprotocolli.cpp" line="1252"/>
        <location filename="dialogelencoprotocolli.cpp" line="1280"/>
        <location filename="dialogelencoprotocolli.cpp" line="1316"/>
        <location filename="dialogelencoprotocolli.cpp" line="1377"/>
        <location filename="dialogelencoprotocolli.cpp" line="1391"/>
        <location filename="dialogelencoprotocolli.cpp" line="1405"/>
        <location filename="dialogelencoprotocolli.cpp" line="1419"/>
        <location filename="dialogelencoprotocolli.cpp" line="1456"/>
        <location filename="dialogelencoprotocolli.cpp" line="1544"/>
        <location filename="dialogelencoprotocolli.cpp" line="1568"/>
        <location filename="dialogelencoprotocolli.cpp" line="1619"/>
        <location filename="dialogelencoprotocolli.cpp" line="1633"/>
        <location filename="dialogelencoprotocolli.cpp" line="1647"/>
        <location filename="dialogelencoprotocolli.cpp" line="1698"/>
        <location filename="dialogelencoprotocolli.cpp" line="1726"/>
        <location filename="dialogelencoprotocolli.cpp" line="1792"/>
        <location filename="dialogelencoprotocolli.cpp" line="1820"/>
        <location filename="dialogelencoprotocolli.cpp" line="1885"/>
        <location filename="dialogelencoprotocolli.cpp" line="1913"/>
        <location filename="dialogelencoprotocolli.cpp" line="1992"/>
        <location filename="dialogelencoprotocolli.cpp" line="2057"/>
        <source>RESISTIVO</source>
        <translation>RESISTIV</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="252"/>
        <location filename="dialogelencoprotocolli.cpp" line="280"/>
        <location filename="dialogelencoprotocolli.cpp" line="316"/>
        <location filename="dialogelencoprotocolli.cpp" line="344"/>
        <location filename="dialogelencoprotocolli.cpp" line="366"/>
        <location filename="dialogelencoprotocolli.cpp" line="404"/>
        <location filename="dialogelencoprotocolli.cpp" line="418"/>
        <location filename="dialogelencoprotocolli.cpp" line="474"/>
        <location filename="dialogelencoprotocolli.cpp" line="502"/>
        <location filename="dialogelencoprotocolli.cpp" line="538"/>
        <location filename="dialogelencoprotocolli.cpp" line="626"/>
        <location filename="dialogelencoprotocolli.cpp" line="684"/>
        <location filename="dialogelencoprotocolli.cpp" line="712"/>
        <location filename="dialogelencoprotocolli.cpp" line="749"/>
        <location filename="dialogelencoprotocolli.cpp" line="777"/>
        <location filename="dialogelencoprotocolli.cpp" line="799"/>
        <location filename="dialogelencoprotocolli.cpp" line="864"/>
        <location filename="dialogelencoprotocolli.cpp" line="892"/>
        <location filename="dialogelencoprotocolli.cpp" line="929"/>
        <location filename="dialogelencoprotocolli.cpp" line="957"/>
        <location filename="dialogelencoprotocolli.cpp" line="994"/>
        <location filename="dialogelencoprotocolli.cpp" line="1060"/>
        <location filename="dialogelencoprotocolli.cpp" line="1074"/>
        <location filename="dialogelencoprotocolli.cpp" line="1111"/>
        <location filename="dialogelencoprotocolli.cpp" line="1139"/>
        <location filename="dialogelencoprotocolli.cpp" line="1161"/>
        <location filename="dialogelencoprotocolli.cpp" line="1185"/>
        <location filename="dialogelencoprotocolli.cpp" line="1266"/>
        <location filename="dialogelencoprotocolli.cpp" line="1294"/>
        <location filename="dialogelencoprotocolli.cpp" line="1330"/>
        <location filename="dialogelencoprotocolli.cpp" line="1442"/>
        <location filename="dialogelencoprotocolli.cpp" line="1480"/>
        <location filename="dialogelencoprotocolli.cpp" line="1530"/>
        <location filename="dialogelencoprotocolli.cpp" line="1712"/>
        <location filename="dialogelencoprotocolli.cpp" line="1740"/>
        <location filename="dialogelencoprotocolli.cpp" line="1806"/>
        <location filename="dialogelencoprotocolli.cpp" line="1834"/>
        <location filename="dialogelencoprotocolli.cpp" line="1899"/>
        <location filename="dialogelencoprotocolli.cpp" line="1964"/>
        <location filename="dialogelencoprotocolli.cpp" line="1978"/>
        <location filename="dialogelencoprotocolli.cpp" line="2043"/>
        <source>CAPACITIVO</source>
        <translation>KAPAZITIVE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="275"/>
        <source>STATICO</source>
        <translation>STATISCH</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="294"/>
        <location filename="dialogelencoprotocolli.cpp" line="308"/>
        <location filename="dialogelencoprotocolli.cpp" line="336"/>
        <location filename="dialogelencoprotocolli.cpp" line="358"/>
        <location filename="dialogelencoprotocolli.cpp" line="382"/>
        <location filename="dialogelencoprotocolli.cpp" line="396"/>
        <location filename="dialogelencoprotocolli.cpp" line="410"/>
        <location filename="dialogelencoprotocolli.cpp" line="552"/>
        <location filename="dialogelencoprotocolli.cpp" line="576"/>
        <location filename="dialogelencoprotocolli.cpp" line="590"/>
        <location filename="dialogelencoprotocolli.cpp" line="604"/>
        <location filename="dialogelencoprotocolli.cpp" line="618"/>
        <location filename="dialogelencoprotocolli.cpp" line="727"/>
        <location filename="dialogelencoprotocolli.cpp" line="741"/>
        <location filename="dialogelencoprotocolli.cpp" line="791"/>
        <location filename="dialogelencoprotocolli.cpp" line="907"/>
        <location filename="dialogelencoprotocolli.cpp" line="972"/>
        <location filename="dialogelencoprotocolli.cpp" line="986"/>
        <location filename="dialogelencoprotocolli.cpp" line="1089"/>
        <location filename="dialogelencoprotocolli.cpp" line="1103"/>
        <location filename="dialogelencoprotocolli.cpp" line="1153"/>
        <location filename="dialogelencoprotocolli.cpp" line="1177"/>
        <location filename="dialogelencoprotocolli.cpp" line="1244"/>
        <location filename="dialogelencoprotocolli.cpp" line="1611"/>
        <location filename="dialogelencoprotocolli.cpp" line="1625"/>
        <location filename="dialogelencoprotocolli.cpp" line="1639"/>
        <location filename="dialogelencoprotocolli.cpp" line="1690"/>
        <location filename="dialogelencoprotocolli.cpp" line="1704"/>
        <location filename="dialogelencoprotocolli.cpp" line="1718"/>
        <location filename="dialogelencoprotocolli.cpp" line="1732"/>
        <location filename="dialogelencoprotocolli.cpp" line="1784"/>
        <location filename="dialogelencoprotocolli.cpp" line="1798"/>
        <location filename="dialogelencoprotocolli.cpp" line="1812"/>
        <location filename="dialogelencoprotocolli.cpp" line="1826"/>
        <location filename="dialogelencoprotocolli.cpp" line="1877"/>
        <location filename="dialogelencoprotocolli.cpp" line="1970"/>
        <source>supino</source>
        <translation>träge</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="295"/>
        <location filename="dialogelencoprotocolli.cpp" line="323"/>
        <location filename="dialogelencoprotocolli.cpp" line="383"/>
        <location filename="dialogelencoprotocolli.cpp" line="815"/>
        <source>diaframma</source>
        <translation>Membran</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="384"/>
        <location filename="dialogelencoprotocolli.cpp" line="398"/>
        <location filename="dialogelencoprotocolli.cpp" line="412"/>
        <source>sulla lesione</source>
        <translation>auf Verletzung</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="261"/>
        <location filename="dialogelencoprotocolli.cpp" line="297"/>
        <location filename="dialogelencoprotocolli.cpp" line="385"/>
        <location filename="dialogelencoprotocolli.cpp" line="399"/>
        <location filename="dialogelencoprotocolli.cpp" line="413"/>
        <location filename="dialogelencoprotocolli.cpp" line="483"/>
        <location filename="dialogelencoprotocolli.cpp" line="497"/>
        <location filename="dialogelencoprotocolli.cpp" line="579"/>
        <location filename="dialogelencoprotocolli.cpp" line="693"/>
        <location filename="dialogelencoprotocolli.cpp" line="707"/>
        <location filename="dialogelencoprotocolli.cpp" line="1069"/>
        <location filename="dialogelencoprotocolli.cpp" line="1224"/>
        <source>STATICO grande</source>
        <translation>große STATIC</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="128"/>
        <source>LOMBALGIA</source>
        <translation>Rückenschmerzen</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="129"/>
        <source>LOMBOSCIALTAGIA SUB ACUTA</source>
        <translation>Lenden-Sciatalgie - Subakut</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="130"/>
        <source>PERIARTRITE SPALLA FASE ACUTA (DINAMICI)</source>
        <translation>Periarthritis der Splitter - akute Phase (Dynamische Elektroden)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="131"/>
        <source>PERIARTRITE SPALLA FASE ACUTA (STATICI)</source>
        <translation>Periarthritis der Splitter - akute Phase - (Statische Elektroden)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="132"/>
        <source>PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (DINAMICI)</source>
        <translation>Periarthritis der Splitter -subakut (Dynamische Elektroden)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="133"/>
        <source>PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (STATICI)</source>
        <translation>Periarthritis der Splitter -subakut - (Statische Elektroden)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="134"/>
        <source>SPALLA FASE ACUTA</source>
        <translation>
Schulter akute Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="136"/>
        <source>SPALLA FASE SUB CUTA</source>
        <translation>subakute Schulter</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="137"/>
        <source>EPICONDILITE DA SOVRACCARICO</source>
        <translation>
Epikondilit</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="138"/>
        <source>POLSO FASE ACUTA</source>
        <translation>
Puls akute Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="139"/>
        <source>POLSO FASE SUB  ACUTA O CRONICA</source>
        <translation>
Pulsphase subakut und chronisch</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="140"/>
        <source>COXARTROSI SUB ACUTA E CRONICA</source>
        <translation>Sub akute und chronische Coxarthrose</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="141"/>
        <source>DISTORSIONE CAVIGLIA FASE ACUTA</source>
        <translation>akute Phase der Knöchelverstauchung</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="142"/>
        <source>DISTORSIONE CAVIGLIA FASE SUB ACUTA</source>
        <translation>Knöchelstauchungs phase subakut</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="162"/>
        <source>GINOCCHIO POST CHIRURGICO</source>
        <translation>Postoperatives Knie </translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="164"/>
        <source>PRIMA FASE - DRENAGGIO</source>
        <translation>Postoperatives Knie  - Erste Entwässerungsphase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="165"/>
        <source>PRIMA FASE - ANTINFIAMMATORIO</source>
        <translation>Postoperatives Knie - Erste Entzündungshemmende Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="166"/>
        <source>SECONDA FASE - MOBILIZZAZZIONE PASSIVA</source>
        <translation>Postoperatives Knie - Passive Mobilisierung der zweiten Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="167"/>
        <source>TERZA FASE - MOBILIZZAZZIONE ATTIVA</source>
        <translation>Postoperatives Knie.- Dritte aktive Mobilisierungsphase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="168"/>
        <source>QUARTA FASE - MOBILIZZAZZIONE ATTIVA CON RESISTENZA</source>
        <translation>Postoperatives Knie - Vierte Phase Aktive Mobilisierung mit Widerstand</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="169"/>
        <source>QUINTA FASE - RECUPERO DEFICIT DI FLESSIONE</source>
        <translation>Postoperatives Knie -Fünfte Phase Erholung Biegedefizit</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="170"/>
        <source>SESTA FASE - RECUPERO DEFICIT IN FLESSO/ESTENSIONE</source>
        <translation>Postoperatives Knie - sechste Phase Wiederherstellung des Flexions- / Dehnungsdefizits</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="172"/>
        <source>SESTA FASE - RECUPERO MOBILITA ARTICOLARE</source>
        <translation>Postoperatives Knie - sechste Phase Wiederherstellung der Gelenkmobilität</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="204"/>
        <source>TRATTAMENTO DELLE LESIONI SPINALI PERIFERICHE</source>
        <translation>
Behandlung von peripheren Wirbelsäulenverletzungen</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="310"/>
        <source>muscolo psoas</source>
        <translation>Psoas muscle</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="324"/>
        <source>L1-L4 e piliforme</source>
        <translation>L1 - L5 und piriformis</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="338"/>
        <source>zona lombare e glutei</source>
        <translation>Lumbaler Bereich und Gesäß</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="397"/>
        <location filename="dialogelencoprotocolli.cpp" line="606"/>
        <location filename="dialogelencoprotocolli.cpp" line="1692"/>
        <location filename="dialogelencoprotocolli.cpp" line="1720"/>
        <location filename="dialogelencoprotocolli.cpp" line="1786"/>
        <location filename="dialogelencoprotocolli.cpp" line="1814"/>
        <source>polso</source>
        <translation>Handgelenk</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="411"/>
        <location filename="dialogelencoprotocolli.cpp" line="1957"/>
        <location filename="dialogelencoprotocolli.cpp" line="2036"/>
        <location filename="dialogelencoprotocolli.cpp" line="2051"/>
        <source>pianta del piede</source>
        <translation>Sohle</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="518"/>
        <source>inserzione nervo sciatico fino irradiazione dolore</source>
        <translation>Insertion  sciatic nerve until  where arrive pain</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="532"/>
        <source>muscoli paravertebrali, glutei, bicipiti femorali, polpacci e tibiali</source>
        <translation>paravertebral muscles, gluteus, femural biceps, calf and  tibial muscle</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1247"/>
        <source>RESISTIVO piccolo</source>
        <translation>Resistive klein</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1309"/>
        <location filename="dialogelencoprotocolli.cpp" line="1323"/>
        <location filename="dialogelencoprotocolli.cpp" line="1561"/>
        <source>bicipite femorale</source>
        <translation>Oberschenkel Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1310"/>
        <source>inserzione vasto mediale,vasto laterale,retto femorale</source>
        <translation>Große mediale Insertion, weites laterales, femorales Rektum</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1436"/>
        <location filename="dialogelencoprotocolli.cpp" line="1450"/>
        <location filename="dialogelencoprotocolli.cpp" line="1524"/>
        <location filename="dialogelencoprotocolli.cpp" line="1538"/>
        <source>tricipite brachiale,sovraspinato,infraspinato,trapezio,pettorali,bicipite brachiale</source>
        <translation>Brachialis Trizeps, Supraspinatus, Infraspinatus, Trapezius, Brustmuskel, Brachii Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1474"/>
        <source>bicipite femorale,polpaccio,tibiale</source>
        <translation>Biceps femoris, Kalb, Tibia</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1613"/>
        <source>braccio e avambraccio</source>
        <translation>Arm und Unterarm</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1627"/>
        <source>inserzione tendini</source>
        <translation>Sehnen</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1878"/>
        <source>lombo sacrale</source>
        <translation>Sakralrücken</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1879"/>
        <source>zona trocanterica e ischiatica</source>
        <translation>Trochanter- und Ischiaszone</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1891"/>
        <source>supina</source>
        <translation>träge</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1907"/>
        <source>gluteo e bicipite femorale</source>
        <translation>Gluteus und Bizeps femoris</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1958"/>
        <location filename="dialogelencoprotocolli.cpp" line="2037"/>
        <source>gastrocnemius mediale e laterale e soleo</source>
        <translation>Medialer und lateraler Gastrocnemius, Soleus</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1971"/>
        <location filename="dialogelencoprotocolli.cpp" line="2050"/>
        <source>gastrocnemius</source>
        <translation>Gastrocnemius</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1972"/>
        <source>dorso del piede</source>
        <translation>Rückseite des Fußes</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1985"/>
        <source>gastrocnemius pos</source>
        <translation>Gastrocnemius</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1986"/>
        <source>pianta del piede, soleo e gastrocnemius laterale</source>
        <translation>Fußpflanze, Soleus und lateraler Gastrocnemius</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="577"/>
        <location filename="dialogelencoprotocolli.cpp" line="591"/>
        <location filename="dialogelencoprotocolli.cpp" line="605"/>
        <location filename="dialogelencoprotocolli.cpp" line="619"/>
        <source>zona cervico/dorsale</source>
        <translation>Bereich Gebärmutterhalskrebs / Rücken</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="578"/>
        <source>muscolo retto femorale</source>
        <translation>rectus femoris</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="592"/>
        <source>articolazione gleno omerale</source>
        <translation>humeral Artikulation gleno</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="361"/>
        <location filename="dialogelencoprotocolli.cpp" line="555"/>
        <location filename="dialogelencoprotocolli.cpp" line="593"/>
        <location filename="dialogelencoprotocolli.cpp" line="607"/>
        <location filename="dialogelencoprotocolli.cpp" line="794"/>
        <location filename="dialogelencoprotocolli.cpp" line="817"/>
        <location filename="dialogelencoprotocolli.cpp" line="887"/>
        <location filename="dialogelencoprotocolli.cpp" line="910"/>
        <location filename="dialogelencoprotocolli.cpp" line="938"/>
        <location filename="dialogelencoprotocolli.cpp" line="975"/>
        <location filename="dialogelencoprotocolli.cpp" line="989"/>
        <location filename="dialogelencoprotocolli.cpp" line="1012"/>
        <location filename="dialogelencoprotocolli.cpp" line="1156"/>
        <location filename="dialogelencoprotocolli.cpp" line="1275"/>
        <location filename="dialogelencoprotocolli.cpp" line="1325"/>
        <location filename="dialogelencoprotocolli.cpp" line="1414"/>
        <location filename="dialogelencoprotocolli.cpp" line="1563"/>
        <location filename="dialogelencoprotocolli.cpp" line="1642"/>
        <location filename="dialogelencoprotocolli.cpp" line="1721"/>
        <location filename="dialogelencoprotocolli.cpp" line="1815"/>
        <location filename="dialogelencoprotocolli.cpp" line="2052"/>
        <source>STATICO piccolo</source>
        <translation>kleine STATIC</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="516"/>
        <location filename="dialogelencoprotocolli.cpp" line="530"/>
        <source>decubito laterale prono</source>
        <translation>Seitlicher seitlicher Dekubitus</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="553"/>
        <location filename="dialogelencoprotocolli.cpp" line="973"/>
        <source>gadtrocnemius</source>
        <translation>Gastrocnemius</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="554"/>
        <location filename="dialogelencoprotocolli.cpp" line="793"/>
        <location filename="dialogelencoprotocolli.cpp" line="974"/>
        <location filename="dialogelencoprotocolli.cpp" line="988"/>
        <location filename="dialogelencoprotocolli.cpp" line="1155"/>
        <location filename="dialogelencoprotocolli.cpp" line="1324"/>
        <location filename="dialogelencoprotocolli.cpp" line="1473"/>
        <location filename="dialogelencoprotocolli.cpp" line="1562"/>
        <source>retto femorale</source>
        <translation>Femoral-Rektum</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="620"/>
        <source>arto superiore plegico</source>
        <translation>gelähmte Arm</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="935"/>
        <location filename="dialogelencoprotocolli.cpp" line="1272"/>
        <location filename="dialogelencoprotocolli.cpp" line="1308"/>
        <location filename="dialogelencoprotocolli.cpp" line="1322"/>
        <location filename="dialogelencoprotocolli.cpp" line="1434"/>
        <location filename="dialogelencoprotocolli.cpp" line="1448"/>
        <location filename="dialogelencoprotocolli.cpp" line="1522"/>
        <location filename="dialogelencoprotocolli.cpp" line="1536"/>
        <location filename="dialogelencoprotocolli.cpp" line="1560"/>
        <source>seduto</source>
        <translation>Sitzung</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="814"/>
        <location filename="dialogelencoprotocolli.cpp" line="1009"/>
        <source>confortevole</source>
        <translation>komfortabel</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="816"/>
        <source>compressione intervertebrale</source>
        <translation>Banddruck</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="309"/>
        <location filename="dialogelencoprotocolli.cpp" line="337"/>
        <location filename="dialogelencoprotocolli.cpp" line="359"/>
        <location filename="dialogelencoprotocolli.cpp" line="742"/>
        <location filename="dialogelencoprotocolli.cpp" line="792"/>
        <location filename="dialogelencoprotocolli.cpp" line="987"/>
        <location filename="dialogelencoprotocolli.cpp" line="1104"/>
        <location filename="dialogelencoprotocolli.cpp" line="1154"/>
        <source>zona lombare</source>
        <translation>den unteren Rücken</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="728"/>
        <location filename="dialogelencoprotocolli.cpp" line="1090"/>
        <location filename="dialogelencoprotocolli.cpp" line="1612"/>
        <location filename="dialogelencoprotocolli.cpp" line="1626"/>
        <location filename="dialogelencoprotocolli.cpp" line="1640"/>
        <source>scapola</source>
        <translation>Schulterblatt</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="729"/>
        <location filename="dialogelencoprotocolli.cpp" line="1091"/>
        <source>deltoide e bicipiti brachiali</source>
        <translation>Deltoideus und Oberarm Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="743"/>
        <location filename="dialogelencoprotocolli.cpp" line="1105"/>
        <source>pettorale e bicipiti brachiali</source>
        <translation>Brust- und Oberarm Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="756"/>
        <location filename="dialogelencoprotocolli.cpp" line="908"/>
        <location filename="dialogelencoprotocolli.cpp" line="1118"/>
        <location filename="dialogelencoprotocolli.cpp" line="1245"/>
        <location filename="dialogelencoprotocolli.cpp" line="1691"/>
        <location filename="dialogelencoprotocolli.cpp" line="1705"/>
        <location filename="dialogelencoprotocolli.cpp" line="1719"/>
        <location filename="dialogelencoprotocolli.cpp" line="1733"/>
        <location filename="dialogelencoprotocolli.cpp" line="1785"/>
        <location filename="dialogelencoprotocolli.cpp" line="1799"/>
        <location filename="dialogelencoprotocolli.cpp" line="1813"/>
        <location filename="dialogelencoprotocolli.cpp" line="1827"/>
        <location filename="dialogelencoprotocolli.cpp" line="1892"/>
        <source>braccio</source>
        <translation>Arm</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="757"/>
        <location filename="dialogelencoprotocolli.cpp" line="1119"/>
        <source>sotto scapolare e scapolare</source>
        <translation>Unter dem Skapulier und Skapulier</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="769"/>
        <location filename="dialogelencoprotocolli.cpp" line="921"/>
        <location filename="dialogelencoprotocolli.cpp" line="949"/>
        <location filename="dialogelencoprotocolli.cpp" line="1131"/>
        <location filename="dialogelencoprotocolli.cpp" line="1258"/>
        <location filename="dialogelencoprotocolli.cpp" line="1286"/>
        <source>su di un lato</source>
        <translation>Auf der einen Seite</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="770"/>
        <location filename="dialogelencoprotocolli.cpp" line="922"/>
        <location filename="dialogelencoprotocolli.cpp" line="950"/>
        <location filename="dialogelencoprotocolli.cpp" line="1132"/>
        <location filename="dialogelencoprotocolli.cpp" line="1259"/>
        <location filename="dialogelencoprotocolli.cpp" line="1287"/>
        <source>gomito</source>
        <translation>Ellbogen</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="771"/>
        <location filename="dialogelencoprotocolli.cpp" line="923"/>
        <location filename="dialogelencoprotocolli.cpp" line="951"/>
        <location filename="dialogelencoprotocolli.cpp" line="1133"/>
        <location filename="dialogelencoprotocolli.cpp" line="1260"/>
        <location filename="dialogelencoprotocolli.cpp" line="1288"/>
        <source>deltoide e zona scapolare</source>
        <translation>Deltoideus und Schulterblattbereich</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="909"/>
        <location filename="dialogelencoprotocolli.cpp" line="1246"/>
        <source>zona scapolare</source>
        <translation>Skapulierbereich</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="937"/>
        <location filename="dialogelencoprotocolli.cpp" line="1274"/>
        <source>bicipite brachiale</source>
        <translation>Bizeps Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1117"/>
        <location filename="dialogelencoprotocolli.cpp" line="1905"/>
        <source>prona</source>
        <translation>anfällig</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1178"/>
        <source>lombare</source>
        <translation>Lenden-</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1179"/>
        <source>muscoli denervati</source>
        <translation>denervierten Muskeln</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1010"/>
        <source>plesso brachiale posteriore</source>
        <translation>Hinten Plexus brachialis</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1011"/>
        <source>plesso brachiale anteriore</source>
        <translation>Plexus brachialis vorne</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="936"/>
        <location filename="dialogelencoprotocolli.cpp" line="1273"/>
        <location filename="dialogelencoprotocolli.cpp" line="1641"/>
        <location filename="dialogelencoprotocolli.cpp" line="1706"/>
        <location filename="dialogelencoprotocolli.cpp" line="1734"/>
        <location filename="dialogelencoprotocolli.cpp" line="1800"/>
        <location filename="dialogelencoprotocolli.cpp" line="1828"/>
        <source>avambraccio</source>
        <translation>Unterarm</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="296"/>
        <source>lombosacrale</source>
        <translation>lumbosacral</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1893"/>
        <source>coscia</source>
        <translation>Schenkel</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="360"/>
        <source>cavo popliteo</source>
        <translation>Kabel poplitea</translation>
    </message>
</context>
<context>
    <name>dialogMenuGuida</name>
    <message>
        <location filename="dialogmenuguida.cpp" line="37"/>
        <source>PROTOCOLLI</source>
        <translation>PROTOKOLLE</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="38"/>
        <source>GENERICI</source>
        <translation>GENERIC</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="39"/>
        <location filename="dialogmenuguida.cpp" line="41"/>
        <source>PROTOCOLLI PATOLOGIE</source>
        <translation>PROTOKOLLE KRANKHEITEN</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="40"/>
        <source>NEUROLOGICI</source>
        <translation>NEUROLOGISCHE</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="42"/>
        <source>ORTOPEDICI</source>
        <translation>ORTHOPEDIC</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="44"/>
        <source>Descrizione protocolli generici</source>
        <translation>Terapeuci Protokolle für die analgetische Behandlungen mit statischen Elektroden mit Physiotherapie</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="45"/>
        <source>Descrizione protocolli neurologici</source>
        <translation>terapeuci Protokolle von orthopädischen Erkrankungen unterteilt nur statische Elektroden</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="46"/>
        <source>Descrizione protocolli ortopedici</source>
        <translation>Protokolle für die Behandlung von Patienten mit neurologischen Erkrankungen</translation>
    </message>
</context>
<context>
    <name>dialogSelezionaProgramma</name>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="58"/>
        <location filename="dialogselezionaprogramma.cpp" line="59"/>
        <location filename="dialogselezionaprogramma.cpp" line="60"/>
        <location filename="dialogselezionaprogramma.cpp" line="61"/>
        <source>Posizione Neutro :</source>
        <translation>Neutrale Position:</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="53"/>
        <location filename="dialogselezionaprogramma.cpp" line="54"/>
        <location filename="dialogselezionaprogramma.cpp" line="55"/>
        <location filename="dialogselezionaprogramma.cpp" line="56"/>
        <source>Posizione Paziente :</source>
        <translation>Position des Patienten:</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="63"/>
        <location filename="dialogselezionaprogramma.cpp" line="64"/>
        <location filename="dialogselezionaprogramma.cpp" line="65"/>
        <location filename="dialogselezionaprogramma.cpp" line="66"/>
        <source>Posizione Elettrodo :</source>
        <translation>Elektroden Lage :</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="68"/>
        <location filename="dialogselezionaprogramma.cpp" line="69"/>
        <location filename="dialogselezionaprogramma.cpp" line="70"/>
        <location filename="dialogselezionaprogramma.cpp" line="71"/>
        <source>Elettrodo</source>
        <translation>ELEKTRODE</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="84"/>
        <location filename="dialogselezionaprogramma.cpp" line="164"/>
        <location filename="dialogselezionaprogramma.cpp" line="189"/>
        <location filename="dialogselezionaprogramma.cpp" line="214"/>
        <location filename="dialogselezionaprogramma.cpp" line="239"/>
        <source>POTENZA</source>
        <translation>POWER</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="85"/>
        <location filename="dialogselezionaprogramma.cpp" line="165"/>
        <location filename="dialogselezionaprogramma.cpp" line="190"/>
        <location filename="dialogselezionaprogramma.cpp" line="215"/>
        <location filename="dialogselezionaprogramma.cpp" line="240"/>
        <source>MODALITA</source>
        <translation>MODUS</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="86"/>
        <location filename="dialogselezionaprogramma.cpp" line="166"/>
        <location filename="dialogselezionaprogramma.cpp" line="167"/>
        <location filename="dialogselezionaprogramma.cpp" line="191"/>
        <location filename="dialogselezionaprogramma.cpp" line="192"/>
        <location filename="dialogselezionaprogramma.cpp" line="216"/>
        <location filename="dialogselezionaprogramma.cpp" line="217"/>
        <location filename="dialogselezionaprogramma.cpp" line="241"/>
        <location filename="dialogselezionaprogramma.cpp" line="242"/>
        <source>FREQUENZA</source>
        <translation>FREQUENZ</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="87"/>
        <location filename="dialogselezionaprogramma.cpp" line="168"/>
        <location filename="dialogselezionaprogramma.cpp" line="169"/>
        <location filename="dialogselezionaprogramma.cpp" line="170"/>
        <location filename="dialogselezionaprogramma.cpp" line="193"/>
        <location filename="dialogselezionaprogramma.cpp" line="194"/>
        <location filename="dialogselezionaprogramma.cpp" line="195"/>
        <location filename="dialogselezionaprogramma.cpp" line="218"/>
        <location filename="dialogselezionaprogramma.cpp" line="219"/>
        <location filename="dialogselezionaprogramma.cpp" line="220"/>
        <location filename="dialogselezionaprogramma.cpp" line="243"/>
        <location filename="dialogselezionaprogramma.cpp" line="244"/>
        <location filename="dialogselezionaprogramma.cpp" line="245"/>
        <source>SLF</source>
        <translation>SLF</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="163"/>
        <location filename="dialogselezionaprogramma.cpp" line="188"/>
        <location filename="dialogselezionaprogramma.cpp" line="213"/>
        <location filename="dialogselezionaprogramma.cpp" line="238"/>
        <source>TEMPO</source>
        <translation>ZEIT</translation>
    </message>
</context>
<context>
    <name>dialogSelezionaabcd</name>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="42"/>
        <source>CAPACITIVO DINAMICO</source>
        <translation>kapazitive dynamische</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="43"/>
        <source>CAPACITIVO STATICO GRANDE</source>
        <translation>große statische Kapazität</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="44"/>
        <source>CAPACITIVO STATICO PICCOLO</source>
        <translation>kleine statische Kapazität</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="45"/>
        <source>RESISTIVO DINAMICO</source>
        <translation>dynamische Widerstands</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="46"/>
        <source>RESISTIVO STATICO GRANDE</source>
        <translation>große statische Widerstands</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="47"/>
        <source>RESISTIVO STATICO PICCOLO</source>
        <translation>kleine statische Widerstands</translation>
    </message>
</context>
<context>
    <name>dialogSetup</name>
    <message>
        <location filename="dialogsetup.ui" line="47"/>
        <source>Setup</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="47"/>
        <source>Tipo 1</source>
        <translation>DIA-01</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="48"/>
        <source>Tipo 2</source>
        <translation>DIA-02</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="49"/>
        <source>Tipo 3</source>
        <translation>DIA-03</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="51"/>
        <source>Tipo di macchina</source>
        <translation>Maschinentyp</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="98"/>
        <source>Vecchia password</source>
        <translation>Altes Passwort</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="110"/>
        <source>Nuova password</source>
        <translation>Neues Kennwort</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="113"/>
        <source>Riscrivi password</source>
        <translation>Passwort wiederholen</translation>
    </message>
</context>
<context>
    <name>dialogStartCalibrazione</name>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="31"/>
        <source>PROCEDURA DI CALIBRAZIONE</source>
        <translation>KALIBRIERVERFAHREN</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="32"/>
        <source>Info calibrazione</source>
        <translation>Schließen Sie das Handstück kapazitive und resistive nicht eingesetzt Elektroden. Legen Sie eine Hand die Masseelektrode. Starten Sie das Kalibrierungsverfahren</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="79"/>
        <source>Vecchia password</source>
        <translation>Altes Passwort</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="92"/>
        <source>Nuova password</source>
        <translation>Neues Kennwort</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="95"/>
        <source>Riscrivi password</source>
        <translation>Passwort wiederholen</translation>
    </message>
</context>
<context>
    <name>dielogUser</name>
    <message>
        <location filename="dieloguser.cpp" line="79"/>
        <source>CALIBRA</source>
        <translation>SIZING</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="80"/>
        <source>OROLOGIO</source>
        <translation>UHR</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="142"/>
        <source>Vecchia password</source>
        <translation>Altes Passwort</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="154"/>
        <source>Nuova password</source>
        <translation>Neues Kennwort</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="157"/>
        <source>Riscrivi password</source>
        <translation>Passwort wiederholen</translation>
    </message>
</context>
<context>
    <name>ufgOrologio</name>
    <message>
        <location filename="ufgorologio.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
</TS>
