/****************************************************************************
** Meta object code from reading C++ file 'dialogorologio.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialogorologio.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialogorologio.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_DialogOrologio_t {
    QByteArrayData data[7];
    char stringdata0[58];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DialogOrologio_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DialogOrologio_t qt_meta_stringdata_DialogOrologio = {
    {
QT_MOC_LITERAL(0, 0, 14), // "DialogOrologio"
QT_MOC_LITERAL(1, 15, 4), // "esci"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 8), // "aggiorna"
QT_MOC_LITERAL(4, 30, 6), // "setOre"
QT_MOC_LITERAL(5, 37, 9), // "setMinuti"
QT_MOC_LITERAL(6, 47, 10) // "setSecondi"

    },
    "DialogOrologio\0esci\0\0aggiorna\0setOre\0"
    "setMinuti\0setSecondi"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DialogOrologio[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x08 /* Private */,
       3,    0,   40,    2, 0x08 /* Private */,
       4,    0,   41,    2, 0x08 /* Private */,
       5,    0,   42,    2, 0x08 /* Private */,
       6,    0,   43,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void DialogOrologio::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DialogOrologio *_t = static_cast<DialogOrologio *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->esci(); break;
        case 1: _t->aggiorna(); break;
        case 2: _t->setOre(); break;
        case 3: _t->setMinuti(); break;
        case 4: _t->setSecondi(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject DialogOrologio::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_DialogOrologio.data,
      qt_meta_data_DialogOrologio,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *DialogOrologio::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DialogOrologio::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_DialogOrologio.stringdata0))
        return static_cast<void*>(const_cast< DialogOrologio*>(this));
    return QDialog::qt_metacast(_clname);
}

int DialogOrologio::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
