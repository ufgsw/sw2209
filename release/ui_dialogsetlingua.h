/********************************************************************************
** Form generated from reading UI file 'dialogsetlingua.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGSETLINGUA_H
#define UI_DIALOGSETLINGUA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include "ufgbutton.h"

QT_BEGIN_NAMESPACE

class Ui_dialogSetLingua
{
public:
    ufgButton *buttonItalia;
    ufgButton *buttonCina;
    ufgButton *buttonInglese;

    void setupUi(QDialog *dialogSetLingua)
    {
        if (dialogSetLingua->objectName().isEmpty())
            dialogSetLingua->setObjectName(QStringLiteral("dialogSetLingua"));
        dialogSetLingua->setWindowModality(Qt::WindowModal);
        dialogSetLingua->resize(800, 480);
        buttonItalia = new ufgButton(dialogSetLingua);
        buttonItalia->setObjectName(QStringLiteral("buttonItalia"));
        buttonItalia->setGeometry(QRect(100, 170, 128, 128));
        buttonCina = new ufgButton(dialogSetLingua);
        buttonCina->setObjectName(QStringLiteral("buttonCina"));
        buttonCina->setGeometry(QRect(330, 170, 128, 128));
        buttonInglese = new ufgButton(dialogSetLingua);
        buttonInglese->setObjectName(QStringLiteral("buttonInglese"));
        buttonInglese->setGeometry(QRect(560, 170, 128, 128));

        retranslateUi(dialogSetLingua);

        QMetaObject::connectSlotsByName(dialogSetLingua);
    } // setupUi

    void retranslateUi(QDialog *dialogSetLingua)
    {
        dialogSetLingua->setWindowTitle(QApplication::translate("dialogSetLingua", "Dialog", 0));
    } // retranslateUi

};

namespace Ui {
    class dialogSetLingua: public Ui_dialogSetLingua {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGSETLINGUA_H
