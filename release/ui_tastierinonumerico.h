/********************************************************************************
** Form generated from reading UI file 'tastierinonumerico.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASTIERINONUMERICO_H
#define UI_TASTIERINONUMERICO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include "ddmbutton.h"

QT_BEGIN_NAMESPACE

class Ui_tastierinoNumerico
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_4;
    ddmButton *tasto4;
    ddmButton *tasto5;
    ddmButton *tasto6;
    QVBoxLayout *verticalLayout;
    ddmButton *tastopm;
    ddmButton *tastopunto;
    QHBoxLayout *horizontalLayout_6;
    ddmButton *tasto1;
    ddmButton *tasto2;
    ddmButton *tasto3;
    QHBoxLayout *horizontalLayout_2;
    ddmButton *tasto7;
    ddmButton *tasto8;
    ddmButton *tasto9;
    QHBoxLayout *horizontalLayout;
    QLabel *display;
    ddmButton *tastoenter;
    QHBoxLayout *horizontalLayout_7;
    ddmButton *tastomin;
    ddmButton *tasto0;
    ddmButton *tastomax;

    void setupUi(QDialog *tastierinoNumerico)
    {
        if (tastierinoNumerico->objectName().isEmpty())
            tastierinoNumerico->setObjectName(QStringLiteral("tastierinoNumerico"));
        tastierinoNumerico->setWindowModality(Qt::WindowModal);
        tastierinoNumerico->resize(509, 330);
        tastierinoNumerico->setWindowTitle(QStringLiteral("Dialog"));
        tastierinoNumerico->setStyleSheet(QStringLiteral(""));
        gridLayout = new QGridLayout(tastierinoNumerico);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        tasto4 = new ddmButton(tastierinoNumerico);
        tasto4->setObjectName(QStringLiteral("tasto4"));
        tasto4->setMinimumSize(QSize(0, 30));

        horizontalLayout_4->addWidget(tasto4);

        tasto5 = new ddmButton(tastierinoNumerico);
        tasto5->setObjectName(QStringLiteral("tasto5"));
        tasto5->setMinimumSize(QSize(0, 30));

        horizontalLayout_4->addWidget(tasto5);

        tasto6 = new ddmButton(tastierinoNumerico);
        tasto6->setObjectName(QStringLiteral("tasto6"));
        tasto6->setMinimumSize(QSize(0, 30));

        horizontalLayout_4->addWidget(tasto6);


        gridLayout->addLayout(horizontalLayout_4, 2, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tastopm = new ddmButton(tastierinoNumerico);
        tastopm->setObjectName(QStringLiteral("tastopm"));
        tastopm->setMinimumSize(QSize(60, 30));

        verticalLayout->addWidget(tastopm);

        tastopunto = new ddmButton(tastierinoNumerico);
        tastopunto->setObjectName(QStringLiteral("tastopunto"));
        tastopunto->setMinimumSize(QSize(60, 30));

        verticalLayout->addWidget(tastopunto);


        gridLayout->addLayout(verticalLayout, 1, 1, 2, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        tasto1 = new ddmButton(tastierinoNumerico);
        tasto1->setObjectName(QStringLiteral("tasto1"));
        tasto1->setMinimumSize(QSize(0, 30));

        horizontalLayout_6->addWidget(tasto1);

        tasto2 = new ddmButton(tastierinoNumerico);
        tasto2->setObjectName(QStringLiteral("tasto2"));
        tasto2->setMinimumSize(QSize(0, 30));

        horizontalLayout_6->addWidget(tasto2);

        tasto3 = new ddmButton(tastierinoNumerico);
        tasto3->setObjectName(QStringLiteral("tasto3"));
        tasto3->setMinimumSize(QSize(0, 30));

        horizontalLayout_6->addWidget(tasto3);


        gridLayout->addLayout(horizontalLayout_6, 3, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        tasto7 = new ddmButton(tastierinoNumerico);
        tasto7->setObjectName(QStringLiteral("tasto7"));
        tasto7->setMinimumSize(QSize(0, 30));

        horizontalLayout_2->addWidget(tasto7);

        tasto8 = new ddmButton(tastierinoNumerico);
        tasto8->setObjectName(QStringLiteral("tasto8"));
        tasto8->setMinimumSize(QSize(0, 30));

        horizontalLayout_2->addWidget(tasto8);

        tasto9 = new ddmButton(tastierinoNumerico);
        tasto9->setObjectName(QStringLiteral("tasto9"));
        tasto9->setMinimumSize(QSize(0, 30));

        horizontalLayout_2->addWidget(tasto9);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 5, -1, 5);
        display = new QLabel(tastierinoNumerico);
        display->setObjectName(QStringLiteral("display"));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(22);
        font.setBold(true);
        font.setWeight(75);
        display->setFont(font);
        display->setInputMethodHints(Qt::ImhNone);
        display->setFrameShape(QFrame::Box);
        display->setText(QStringLiteral("TextLabel"));
        display->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(display);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 2);

        tastoenter = new ddmButton(tastierinoNumerico);
        tastoenter->setObjectName(QStringLiteral("tastoenter"));
        tastoenter->setMinimumSize(QSize(60, 60));
        tastoenter->setMaximumSize(QSize(50, 16777215));

        gridLayout->addWidget(tastoenter, 3, 1, 2, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        tastomin = new ddmButton(tastierinoNumerico);
        tastomin->setObjectName(QStringLiteral("tastomin"));
        tastomin->setMinimumSize(QSize(0, 30));

        horizontalLayout_7->addWidget(tastomin);

        tasto0 = new ddmButton(tastierinoNumerico);
        tasto0->setObjectName(QStringLiteral("tasto0"));
        tasto0->setMinimumSize(QSize(0, 30));

        horizontalLayout_7->addWidget(tasto0);

        tastomax = new ddmButton(tastierinoNumerico);
        tastomax->setObjectName(QStringLiteral("tastomax"));
        tastomax->setMinimumSize(QSize(0, 30));

        horizontalLayout_7->addWidget(tastomax);


        gridLayout->addLayout(horizontalLayout_7, 4, 0, 1, 1);


        retranslateUi(tastierinoNumerico);

        QMetaObject::connectSlotsByName(tastierinoNumerico);
    } // setupUi

    void retranslateUi(QDialog *tastierinoNumerico)
    {
        Q_UNUSED(tastierinoNumerico);
    } // retranslateUi

};

namespace Ui {
    class tastierinoNumerico: public Ui_tastierinoNumerico {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASTIERINONUMERICO_H
