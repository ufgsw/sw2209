/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <ufgbutton.h>
#include "ufgbutton1label.h"
#include "ufgbutton2label.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *labelCareTherapy;
    ufgButton2Label *buttonTrattamento;
    ufgButton2Label *buttonFrequenza;
    ufgButton1label *buttonCalibra;
    ufgButton2Label *buttonPotenza;
    ufgButton2Label *buttonTempo;
    ufgButton *buttonStart;
    QLabel *imageModalita;
    QLabel *imageFrequenza;
    QLabel *imagePemf;
    QLabel *imagePotenza;
    ufgButton2Label *buttonPemf;
    QLabel *imageTempo;
    ufgButton1label *buttonGuida;
    QLabel *imageRun;
    ufgButton1label *buttonInfo;
    ufgButton *buttonSetup;
    QLabel *labelRelease;
    QLabel *labelTipoMacchina;
    QLabel *labelOrologio;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 480);
        MainWindow->setMinimumSize(QSize(800, 480));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans Mono"));
        font.setPointSize(16);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        font.setKerning(true);
        MainWindow->setFont(font);
        MainWindow->setFocusPolicy(Qt::NoFocus);
        MainWindow->setContextMenuPolicy(Qt::NoContextMenu);
        MainWindow->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        MainWindow->setLocale(QLocale(QLocale::Italian, QLocale::Italy));
        MainWindow->setDockOptions(QMainWindow::AllowTabbedDocks);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        labelCareTherapy = new QLabel(centralWidget);
        labelCareTherapy->setObjectName(QStringLiteral("labelCareTherapy"));
        labelCareTherapy->setGeometry(QRect(10, 10, 250, 70));
        labelCareTherapy->setPixmap(QPixmap(QString::fromUtf8(":/img/BEnergy.png")));
        labelCareTherapy->setScaledContents(true);
        buttonTrattamento = new ufgButton2Label(centralWidget);
        buttonTrattamento->setObjectName(QStringLiteral("buttonTrattamento"));
        buttonTrattamento->setGeometry(QRect(10, 180, 220, 80));
        buttonTrattamento->setFocusPolicy(Qt::TabFocus);
        buttonTrattamento->setAutoFillBackground(false);
        buttonFrequenza = new ufgButton2Label(centralWidget);
        buttonFrequenza->setObjectName(QStringLiteral("buttonFrequenza"));
        buttonFrequenza->setGeometry(QRect(10, 270, 220, 80));
        buttonFrequenza->setFocusPolicy(Qt::TabFocus);
        buttonCalibra = new ufgButton1label(centralWidget);
        buttonCalibra->setObjectName(QStringLiteral("buttonCalibra"));
        buttonCalibra->setGeometry(QRect(570, 180, 220, 80));
        buttonCalibra->setFocusPolicy(Qt::TabFocus);
        buttonPotenza = new ufgButton2Label(centralWidget);
        buttonPotenza->setObjectName(QStringLiteral("buttonPotenza"));
        buttonPotenza->setGeometry(QRect(10, 90, 220, 80));
        buttonPotenza->setFocusPolicy(Qt::TabFocus);
        buttonTempo = new ufgButton2Label(centralWidget);
        buttonTempo->setObjectName(QStringLiteral("buttonTempo"));
        buttonTempo->setGeometry(QRect(570, 90, 220, 80));
        buttonTempo->setFocusPolicy(Qt::TabFocus);
        buttonStart = new ufgButton(centralWidget);
        buttonStart->setObjectName(QStringLiteral("buttonStart"));
        buttonStart->setGeometry(QRect(337, 90, 120, 120));
        buttonStart->setFocusPolicy(Qt::StrongFocus);
        imageModalita = new QLabel(centralWidget);
        imageModalita->setObjectName(QStringLiteral("imageModalita"));
        imageModalita->setGeometry(QRect(240, 190, 60, 60));
        imageModalita->setPixmap(QPixmap(QString::fromUtf8(":/img/ico/ico1.png")));
        imageModalita->setScaledContents(true);
        imageFrequenza = new QLabel(centralWidget);
        imageFrequenza->setObjectName(QStringLiteral("imageFrequenza"));
        imageFrequenza->setGeometry(QRect(240, 280, 60, 60));
        imageFrequenza->setPixmap(QPixmap(QString::fromUtf8(":/img/ico/ico3.png")));
        imageFrequenza->setScaledContents(true);
        imagePemf = new QLabel(centralWidget);
        imagePemf->setObjectName(QStringLiteral("imagePemf"));
        imagePemf->setGeometry(QRect(240, 370, 60, 60));
        imagePemf->setPixmap(QPixmap(QString::fromUtf8(":/img/ico/ico2.png")));
        imagePemf->setScaledContents(true);
        imagePotenza = new QLabel(centralWidget);
        imagePotenza->setObjectName(QStringLiteral("imagePotenza"));
        imagePotenza->setGeometry(QRect(240, 100, 60, 60));
        imagePotenza->setPixmap(QPixmap(QString::fromUtf8(":/img/ico/ico4.png")));
        imagePotenza->setScaledContents(true);
        buttonPemf = new ufgButton2Label(centralWidget);
        buttonPemf->setObjectName(QStringLiteral("buttonPemf"));
        buttonPemf->setGeometry(QRect(10, 360, 220, 80));
        buttonPemf->setFocusPolicy(Qt::TabFocus);
        imageTempo = new QLabel(centralWidget);
        imageTempo->setObjectName(QStringLiteral("imageTempo"));
        imageTempo->setGeometry(QRect(490, 100, 60, 60));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(imageTempo->sizePolicy().hasHeightForWidth());
        imageTempo->setSizePolicy(sizePolicy);
        imageTempo->setPixmap(QPixmap(QString::fromUtf8(":/img/ico/ico8.png")));
        imageTempo->setScaledContents(false);
        buttonGuida = new ufgButton1label(centralWidget);
        buttonGuida->setObjectName(QStringLiteral("buttonGuida"));
        buttonGuida->setGeometry(QRect(570, 360, 220, 80));
        buttonGuida->setFocusPolicy(Qt::TabFocus);
        imageRun = new QLabel(centralWidget);
        imageRun->setObjectName(QStringLiteral("imageRun"));
        imageRun->setGeometry(QRect(320, 330, 200, 120));
        imageRun->setScaledContents(true);
        buttonInfo = new ufgButton1label(centralWidget);
        buttonInfo->setObjectName(QStringLiteral("buttonInfo"));
        buttonInfo->setGeometry(QRect(570, 270, 220, 80));
        buttonInfo->setFocusPolicy(Qt::TabFocus);
        buttonSetup = new ufgButton(centralWidget);
        buttonSetup->setObjectName(QStringLiteral("buttonSetup"));
        buttonSetup->setGeometry(QRect(670, 10, 115, 69));
        buttonSetup->setFocusPolicy(Qt::StrongFocus);
        labelRelease = new QLabel(centralWidget);
        labelRelease->setObjectName(QStringLiteral("labelRelease"));
        labelRelease->setGeometry(QRect(10, 455, 431, 20));
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        labelRelease->setFont(font1);
        labelRelease->setText(QStringLiteral("TextLabel"));
        labelTipoMacchina = new QLabel(centralWidget);
        labelTipoMacchina->setObjectName(QStringLiteral("labelTipoMacchina"));
        labelTipoMacchina->setGeometry(QRect(690, 455, 91, 20));
        labelTipoMacchina->setFont(font1);
        labelTipoMacchina->setText(QStringLiteral("TextLabel"));
        labelTipoMacchina->setAlignment(Qt::AlignCenter);
        labelOrologio = new QLabel(centralWidget);
        labelOrologio->setObjectName(QStringLiteral("labelOrologio"));
        labelOrologio->setGeometry(QRect(580, 455, 91, 20));
        labelOrologio->setFont(font1);
        labelOrologio->setText(QStringLiteral("00:00:00"));
        labelOrologio->setAlignment(Qt::AlignCenter);
        MainWindow->setCentralWidget(centralWidget);
        QWidget::setTabOrder(buttonStart, buttonPotenza);
        QWidget::setTabOrder(buttonPotenza, buttonTrattamento);
        QWidget::setTabOrder(buttonTrattamento, buttonFrequenza);
        QWidget::setTabOrder(buttonFrequenza, buttonPemf);
        QWidget::setTabOrder(buttonPemf, buttonTempo);
        QWidget::setTabOrder(buttonTempo, buttonCalibra);
        QWidget::setTabOrder(buttonCalibra, buttonInfo);
        QWidget::setTabOrder(buttonInfo, buttonGuida);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Diatermia", 0));
        labelCareTherapy->setText(QString());
        imageModalita->setText(QString());
        imageFrequenza->setText(QString());
        imagePemf->setText(QString());
        imagePotenza->setText(QString());
        imageTempo->setText(QString());
        imageRun->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
