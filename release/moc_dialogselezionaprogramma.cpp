/****************************************************************************
** Meta object code from reading C++ file 'dialogselezionaprogramma.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialogselezionaprogramma.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialogselezionaprogramma.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_dialogSelezionaProgramma_t {
    QByteArrayData data[13];
    char stringdata0[147];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_dialogSelezionaProgramma_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_dialogSelezionaProgramma_t qt_meta_stringdata_dialogSelezionaProgramma = {
    {
QT_MOC_LITERAL(0, 0, 24), // "dialogSelezionaProgramma"
QT_MOC_LITERAL(1, 25, 20), // "programmaSelezionato"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 6), // "numero"
QT_MOC_LITERAL(4, 54, 7), // "onclose"
QT_MOC_LITERAL(5, 62, 11), // "rotellaMove"
QT_MOC_LITERAL(6, 74, 2), // "up"
QT_MOC_LITERAL(7, 77, 12), // "rotellaClick"
QT_MOC_LITERAL(8, 90, 12), // "selezionato1"
QT_MOC_LITERAL(9, 103, 12), // "selezionato2"
QT_MOC_LITERAL(10, 116, 12), // "selezionato3"
QT_MOC_LITERAL(11, 129, 12), // "selezionato4"
QT_MOC_LITERAL(12, 142, 4) // "esci"

    },
    "dialogSelezionaProgramma\0programmaSelezionato\0"
    "\0numero\0onclose\0rotellaMove\0up\0"
    "rotellaClick\0selezionato1\0selezionato2\0"
    "selezionato3\0selezionato4\0esci"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_dialogSelezionaProgramma[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       4,    0,   62,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   63,    2, 0x0a /* Public */,
       7,    0,   66,    2, 0x0a /* Public */,
       8,    0,   67,    2, 0x0a /* Public */,
       9,    0,   68,    2, 0x0a /* Public */,
      10,    0,   69,    2, 0x0a /* Public */,
      11,    0,   70,    2, 0x0a /* Public */,
      12,    0,   71,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void dialogSelezionaProgramma::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        dialogSelezionaProgramma *_t = static_cast<dialogSelezionaProgramma *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->programmaSelezionato((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->onclose(); break;
        case 2: _t->rotellaMove((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->rotellaClick(); break;
        case 4: _t->selezionato1(); break;
        case 5: _t->selezionato2(); break;
        case 6: _t->selezionato3(); break;
        case 7: _t->selezionato4(); break;
        case 8: _t->esci(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (dialogSelezionaProgramma::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialogSelezionaProgramma::programmaSelezionato)) {
                *result = 0;
            }
        }
        {
            typedef void (dialogSelezionaProgramma::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialogSelezionaProgramma::onclose)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject dialogSelezionaProgramma::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_dialogSelezionaProgramma.data,
      qt_meta_data_dialogSelezionaProgramma,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *dialogSelezionaProgramma::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *dialogSelezionaProgramma::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_dialogSelezionaProgramma.stringdata0))
        return static_cast<void*>(const_cast< dialogSelezionaProgramma*>(this));
    return QDialog::qt_metacast(_clname);
}

int dialogSelezionaProgramma::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void dialogSelezionaProgramma::programmaSelezionato(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void dialogSelezionaProgramma::onclose()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
