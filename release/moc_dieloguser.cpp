/****************************************************************************
** Meta object code from reading C++ file 'dieloguser.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dieloguser.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dieloguser.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_dielogUser_t {
    QByteArrayData data[13];
    char stringdata0[177];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_dielogUser_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_dielogUser_t qt_meta_stringdata_dielogUser = {
    {
QT_MOC_LITERAL(0, 0, 10), // "dielogUser"
QT_MOC_LITERAL(1, 11, 9), // "onCalibra"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 7), // "onClose"
QT_MOC_LITERAL(4, 30, 20), // "buttonItalia_clicked"
QT_MOC_LITERAL(5, 51, 21), // "buttonInglese_clicked"
QT_MOC_LITERAL(6, 73, 21), // "buttonTedesco_clicked"
QT_MOC_LITERAL(7, 95, 20), // "buttonCinese_clicked"
QT_MOC_LITERAL(8, 116, 19), // "selezionatoPassword"
QT_MOC_LITERAL(9, 136, 11), // "showCalibra"
QT_MOC_LITERAL(10, 148, 10), // "exeCalibra"
QT_MOC_LITERAL(11, 159, 12), // "showOrologio"
QT_MOC_LITERAL(12, 172, 4) // "exit"

    },
    "dielogUser\0onCalibra\0\0onClose\0"
    "buttonItalia_clicked\0buttonInglese_clicked\0"
    "buttonTedesco_clicked\0buttonCinese_clicked\0"
    "selezionatoPassword\0showCalibra\0"
    "exeCalibra\0showOrologio\0exit"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_dielogUser[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x06 /* Public */,
       3,    0,   70,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   71,    2, 0x08 /* Private */,
       5,    0,   72,    2, 0x08 /* Private */,
       6,    0,   73,    2, 0x08 /* Private */,
       7,    0,   74,    2, 0x08 /* Private */,
       8,    0,   75,    2, 0x08 /* Private */,
       9,    0,   76,    2, 0x08 /* Private */,
      10,    0,   77,    2, 0x08 /* Private */,
      11,    0,   78,    2, 0x08 /* Private */,
      12,    0,   79,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void dielogUser::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        dielogUser *_t = static_cast<dielogUser *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onCalibra(); break;
        case 1: _t->onClose(); break;
        case 2: _t->buttonItalia_clicked(); break;
        case 3: _t->buttonInglese_clicked(); break;
        case 4: _t->buttonTedesco_clicked(); break;
        case 5: _t->buttonCinese_clicked(); break;
        case 6: _t->selezionatoPassword(); break;
        case 7: _t->showCalibra(); break;
        case 8: _t->exeCalibra(); break;
        case 9: _t->showOrologio(); break;
        case 10: _t->exit(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (dielogUser::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dielogUser::onCalibra)) {
                *result = 0;
            }
        }
        {
            typedef void (dielogUser::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dielogUser::onClose)) {
                *result = 1;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject dielogUser::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_dielogUser.data,
      qt_meta_data_dielogUser,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *dielogUser::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *dielogUser::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_dielogUser.stringdata0))
        return static_cast<void*>(const_cast< dielogUser*>(this));
    return QDialog::qt_metacast(_clname);
}

int dielogUser::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void dielogUser::onCalibra()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void dielogUser::onClose()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
