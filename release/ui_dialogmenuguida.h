/********************************************************************************
** Form generated from reading UI file 'dialogmenuguida.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGMENUGUIDA_H
#define UI_DIALOGMENUGUIDA_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <ufgbutton.h>
#include "ufgbutton2label.h"

QT_BEGIN_NAMESPACE

class Ui_dialogMenuGuida
{
public:
    ufgButton2Label *buttonNeurologici;
    QLabel *labelNeurologici;
    ufgButton2Label *buttonGenerici;
    QLabel *labelOrtopedici;
    QLabel *labelGenerici;
    ufgButton2Label *buttonOrtopedici;
    ufgButton *buttonExit;

    void setupUi(QDialog *dialogMenuGuida)
    {
        if (dialogMenuGuida->objectName().isEmpty())
            dialogMenuGuida->setObjectName(QStringLiteral("dialogMenuGuida"));
        dialogMenuGuida->setWindowModality(Qt::WindowModal);
        dialogMenuGuida->resize(800, 480);
        dialogMenuGuida->setWindowTitle(QStringLiteral("Dialog"));
        dialogMenuGuida->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        buttonNeurologici = new ufgButton2Label(dialogMenuGuida);
        buttonNeurologici->setObjectName(QStringLiteral("buttonNeurologici"));
        buttonNeurologici->setGeometry(QRect(550, 150, 240, 80));
        buttonNeurologici->setFocusPolicy(Qt::TabFocus);
        buttonNeurologici->setAutoFillBackground(false);
        labelNeurologici = new QLabel(dialogMenuGuida);
        labelNeurologici->setObjectName(QStringLiteral("labelNeurologici"));
        labelNeurologici->setGeometry(QRect(550, 250, 240, 150));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        labelNeurologici->setFont(font);
        labelNeurologici->setStyleSheet(QStringLiteral("color: rgb(142, 180, 227);"));
        labelNeurologici->setFrameShape(QFrame::NoFrame);
        labelNeurologici->setFrameShadow(QFrame::Sunken);
        labelNeurologici->setText(QStringLiteral("Protocolli per il trattamento di pazienti con patologie neurologiche"));
        labelNeurologici->setTextFormat(Qt::PlainText);
        labelNeurologici->setScaledContents(false);
        labelNeurologici->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        labelNeurologici->setWordWrap(true);
        buttonGenerici = new ufgButton2Label(dialogMenuGuida);
        buttonGenerici->setObjectName(QStringLiteral("buttonGenerici"));
        buttonGenerici->setGeometry(QRect(10, 150, 240, 80));
        buttonGenerici->setFocusPolicy(Qt::TabFocus);
        buttonGenerici->setAutoFillBackground(false);
        labelOrtopedici = new QLabel(dialogMenuGuida);
        labelOrtopedici->setObjectName(QStringLiteral("labelOrtopedici"));
        labelOrtopedici->setGeometry(QRect(280, 250, 240, 150));
        labelOrtopedici->setFont(font);
        labelOrtopedici->setStyleSheet(QStringLiteral("color: rgb(142, 180, 227);"));
        labelOrtopedici->setFrameShape(QFrame::NoFrame);
        labelOrtopedici->setText(QStringLiteral("Protocolli terapeuci divisi per patologie ortopediche utilizzando solo elettrodi statici"));
        labelOrtopedici->setTextFormat(Qt::PlainText);
        labelOrtopedici->setScaledContents(false);
        labelOrtopedici->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        labelOrtopedici->setWordWrap(true);
        labelGenerici = new QLabel(dialogMenuGuida);
        labelGenerici->setObjectName(QStringLiteral("labelGenerici"));
        labelGenerici->setGeometry(QRect(10, 250, 240, 150));
        labelGenerici->setFont(font);
        labelGenerici->setStyleSheet(QStringLiteral("color: rgb(142, 180, 227);"));
        labelGenerici->setLocale(QLocale(QLocale::Italian, QLocale::Italy));
        labelGenerici->setFrameShape(QFrame::NoFrame);
        labelGenerici->setFrameShadow(QFrame::Plain);
        labelGenerici->setText(QStringLiteral("Protocolli terapeuci per trattamenti antalgici utilizzando elettrodi statici con kinesiterapia"));
        labelGenerici->setTextFormat(Qt::PlainText);
        labelGenerici->setScaledContents(false);
        labelGenerici->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        labelGenerici->setWordWrap(true);
        buttonOrtopedici = new ufgButton2Label(dialogMenuGuida);
        buttonOrtopedici->setObjectName(QStringLiteral("buttonOrtopedici"));
        buttonOrtopedici->setGeometry(QRect(280, 150, 240, 80));
        buttonOrtopedici->setFocusPolicy(Qt::TabFocus);
        buttonOrtopedici->setAutoFillBackground(false);
        buttonExit = new ufgButton(dialogMenuGuida);
        buttonExit->setObjectName(QStringLiteral("buttonExit"));
        buttonExit->setGeometry(QRect(10, 10, 80, 80));
        buttonExit->setFocusPolicy(Qt::StrongFocus);
        QWidget::setTabOrder(buttonGenerici, buttonOrtopedici);
        QWidget::setTabOrder(buttonOrtopedici, buttonNeurologici);
        QWidget::setTabOrder(buttonNeurologici, buttonExit);

        retranslateUi(dialogMenuGuida);

        QMetaObject::connectSlotsByName(dialogMenuGuida);
    } // setupUi

    void retranslateUi(QDialog *dialogMenuGuida)
    {
        Q_UNUSED(dialogMenuGuida);
    } // retranslateUi

};

namespace Ui {
    class dialogMenuGuida: public Ui_dialogMenuGuida {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGMENUGUIDA_H
