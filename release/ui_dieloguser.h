/********************************************************************************
** Form generated from reading UI file 'dieloguser.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIELOGUSER_H
#define UI_DIELOGUSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <ufgbutton.h>
#include "ufgbutton1label.h"

QT_BEGIN_NAMESPACE

class Ui_dielogUser
{
public:
    QGroupBox *boxLingua;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    ufgButton *buttonItalia;
    QSpacerItem *horizontalSpacer_2;
    ufgButton *buttonInglese;
    QSpacerItem *horizontalSpacer_3;
    ufgButton *buttonTedesco;
    QSpacerItem *horizontalSpacer_4;
    ufgButton *buttonCinese;
    QSpacerItem *horizontalSpacer_5;
    ufgButton *buttonPassword;
    ufgButton *buttonExit;
    ufgButton1label *buttonCalibra;
    ufgButton1label *buttonOrologio;

    void setupUi(QDialog *dielogUser)
    {
        if (dielogUser->objectName().isEmpty())
            dielogUser->setObjectName(QStringLiteral("dielogUser"));
        dielogUser->resize(800, 480);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(dielogUser->sizePolicy().hasHeightForWidth());
        dielogUser->setSizePolicy(sizePolicy);
        dielogUser->setMinimumSize(QSize(800, 480));
        dielogUser->setWindowTitle(QStringLiteral("Dialog"));
        boxLingua = new QGroupBox(dielogUser);
        boxLingua->setObjectName(QStringLiteral("boxLingua"));
        boxLingua->setGeometry(QRect(10, 290, 780, 181));
        boxLingua->setAutoFillBackground(false);
        boxLingua->setStyleSheet(QLatin1String("border-color: rgb(0, 0, 200);\n"
"border-top-color: rgb(239, 37, 37);"));
        boxLingua->setTitle(QStringLiteral(""));
        boxLingua->setFlat(false);
        boxLingua->setCheckable(false);
        layoutWidget = new QWidget(boxLingua);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 29, 721, 130));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        buttonItalia = new ufgButton(layoutWidget);
        buttonItalia->setObjectName(QStringLiteral("buttonItalia"));
        buttonItalia->setMinimumSize(QSize(128, 128));

        horizontalLayout->addWidget(buttonItalia);

        horizontalSpacer_2 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        buttonInglese = new ufgButton(layoutWidget);
        buttonInglese->setObjectName(QStringLiteral("buttonInglese"));
        buttonInglese->setMinimumSize(QSize(128, 128));

        horizontalLayout->addWidget(buttonInglese);

        horizontalSpacer_3 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        buttonTedesco = new ufgButton(layoutWidget);
        buttonTedesco->setObjectName(QStringLiteral("buttonTedesco"));
        buttonTedesco->setMinimumSize(QSize(128, 0));

        horizontalLayout->addWidget(buttonTedesco);

        horizontalSpacer_4 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        buttonCinese = new ufgButton(layoutWidget);
        buttonCinese->setObjectName(QStringLiteral("buttonCinese"));
        buttonCinese->setMinimumSize(QSize(128, 128));

        horizontalLayout->addWidget(buttonCinese);

        horizontalSpacer_5 = new QSpacerItem(28, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);

        buttonPassword = new ufgButton(dielogUser);
        buttonPassword->setObjectName(QStringLiteral("buttonPassword"));
        buttonPassword->setGeometry(QRect(710, 10, 80, 80));
        buttonExit = new ufgButton(dielogUser);
        buttonExit->setObjectName(QStringLiteral("buttonExit"));
        buttonExit->setGeometry(QRect(10, 10, 80, 80));
        buttonCalibra = new ufgButton1label(dielogUser);
        buttonCalibra->setObjectName(QStringLiteral("buttonCalibra"));
        buttonCalibra->setGeometry(QRect(20, 170, 341, 91));
        buttonCalibra->setFocusPolicy(Qt::TabFocus);
        buttonOrologio = new ufgButton1label(dielogUser);
        buttonOrologio->setObjectName(QStringLiteral("buttonOrologio"));
        buttonOrologio->setGeometry(QRect(430, 170, 341, 91));
        buttonOrologio->setFocusPolicy(Qt::TabFocus);

        retranslateUi(dielogUser);

        QMetaObject::connectSlotsByName(dielogUser);
    } // setupUi

    void retranslateUi(QDialog *dielogUser)
    {
        Q_UNUSED(dielogUser);
    } // retranslateUi

};

namespace Ui {
    class dielogUser: public Ui_dielogUser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIELOGUSER_H
