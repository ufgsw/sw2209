/********************************************************************************
** Form generated from reading UI file 'dialogwarning.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGWARNING_H
#define UI_DIALOGWARNING_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_dialogWarning
{
public:
    QGridLayout *gridLayout;
    QLabel *label;

    void setupUi(QDialog *dialogWarning)
    {
        if (dialogWarning->objectName().isEmpty())
            dialogWarning->setObjectName(QStringLiteral("dialogWarning"));
        dialogWarning->setWindowModality(Qt::NonModal);
        dialogWarning->resize(400, 300);
        dialogWarning->setWindowTitle(QStringLiteral("Dialog"));
        gridLayout = new QGridLayout(dialogWarning);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(20, 20, 20, 20);
        label = new QLabel(dialogWarning);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(18);
        label->setFont(font);
        label->setStyleSheet(QStringLiteral("color: rgb(255, 0, 0);"));
        label->setText(QStringLiteral("TextLabel"));
        label->setAlignment(Qt::AlignCenter);
        label->setWordWrap(true);

        gridLayout->addWidget(label, 0, 0, 1, 1);


        retranslateUi(dialogWarning);

        QMetaObject::connectSlotsByName(dialogWarning);
    } // setupUi

    void retranslateUi(QDialog *dialogWarning)
    {
        Q_UNUSED(dialogWarning);
    } // retranslateUi

};

namespace Ui {
    class dialogWarning: public Ui_dialogWarning {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGWARNING_H
