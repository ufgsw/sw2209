/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[53];
    char stringdata0[609];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 14), // "allarmeInCorso"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 14), // "warningInCorso"
QT_MOC_LITERAL(4, 42, 9), // "newConfig"
QT_MOC_LITERAL(5, 52, 12), // "setFrequenza"
QT_MOC_LITERAL(6, 65, 16), // "setFrequenzaPemf"
QT_MOC_LITERAL(7, 82, 14), // "setTrattamento"
QT_MOC_LITERAL(8, 97, 10), // "setPotenza"
QT_MOC_LITERAL(9, 108, 3), // "val"
QT_MOC_LITERAL(10, 112, 8), // "setTempo"
QT_MOC_LITERAL(11, 121, 7), // "secondi"
QT_MOC_LITERAL(12, 129, 16), // "getFrequenzaPemf"
QT_MOC_LITERAL(13, 146, 12), // "getFrequenza"
QT_MOC_LITERAL(14, 159, 14), // "getTrattamento"
QT_MOC_LITERAL(15, 174, 10), // "getPotenza"
QT_MOC_LITERAL(16, 185, 12), // "getElettrodi"
QT_MOC_LITERAL(17, 198, 8), // "getTempo"
QT_MOC_LITERAL(18, 207, 17), // "incrementaPotenza"
QT_MOC_LITERAL(19, 225, 17), // "decrementaPotenza"
QT_MOC_LITERAL(20, 243, 13), // "showMenuGuida"
QT_MOC_LITERAL(21, 257, 8), // "showInfo"
QT_MOC_LITERAL(22, 266, 11), // "showAllarme"
QT_MOC_LITERAL(23, 278, 11), // "showWarning"
QT_MOC_LITERAL(24, 290, 9), // "showSetup"
QT_MOC_LITERAL(25, 300, 11), // "showCalibra"
QT_MOC_LITERAL(26, 312, 14), // "updateTabFocus"
QT_MOC_LITERAL(27, 327, 9), // "setCicala"
QT_MOC_LITERAL(28, 337, 5), // "tempo"
QT_MOC_LITERAL(29, 343, 11), // "rotellaMove"
QT_MOC_LITERAL(30, 355, 2), // "up"
QT_MOC_LITERAL(31, 358, 12), // "rotellaClick"
QT_MOC_LITERAL(32, 371, 12), // "procSerialRx"
QT_MOC_LITERAL(33, 384, 5), // "char*"
QT_MOC_LITERAL(34, 390, 4), // "line"
QT_MOC_LITERAL(35, 395, 3), // "len"
QT_MOC_LITERAL(36, 399, 9), // "proc485Rx"
QT_MOC_LITERAL(37, 409, 7), // "comTask"
QT_MOC_LITERAL(38, 417, 11), // "refreshTask"
QT_MOC_LITERAL(39, 429, 9), // "startStop"
QT_MOC_LITERAL(40, 439, 15), // "startEsecuzione"
QT_MOC_LITERAL(41, 455, 14), // "stopEsecuzione"
QT_MOC_LITERAL(42, 470, 15), // "pausaEsecuzione"
QT_MOC_LITERAL(43, 486, 15), // "caricaProgramma"
QT_MOC_LITERAL(44, 502, 10), // "sProgramma"
QT_MOC_LITERAL(45, 513, 9), // "programma"
QT_MOC_LITERAL(46, 523, 9), // "setLingua"
QT_MOC_LITERAL(47, 533, 6), // "lingua"
QT_MOC_LITERAL(48, 540, 6), // "reload"
QT_MOC_LITERAL(49, 547, 15), // "exeCalibrazione"
QT_MOC_LITERAL(50, 563, 15), // "riprendoRotella"
QT_MOC_LITERAL(51, 579, 12), // "resetAllarmi"
QT_MOC_LITERAL(52, 592, 16) // "richiediPassword"

    },
    "MainWindow\0allarmeInCorso\0\0warningInCorso\0"
    "newConfig\0setFrequenza\0setFrequenzaPemf\0"
    "setTrattamento\0setPotenza\0val\0setTempo\0"
    "secondi\0getFrequenzaPemf\0getFrequenza\0"
    "getTrattamento\0getPotenza\0getElettrodi\0"
    "getTempo\0incrementaPotenza\0decrementaPotenza\0"
    "showMenuGuida\0showInfo\0showAllarme\0"
    "showWarning\0showSetup\0showCalibra\0"
    "updateTabFocus\0setCicala\0tempo\0"
    "rotellaMove\0up\0rotellaClick\0procSerialRx\0"
    "char*\0line\0len\0proc485Rx\0comTask\0"
    "refreshTask\0startStop\0startEsecuzione\0"
    "stopEsecuzione\0pausaEsecuzione\0"
    "caricaProgramma\0sProgramma\0programma\0"
    "setLingua\0lingua\0reload\0exeCalibrazione\0"
    "riprendoRotella\0resetAllarmi\0"
    "richiediPassword"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      40,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  214,    2, 0x06 /* Public */,
       3,    0,  215,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,  216,    2, 0x0a /* Public */,
       5,    0,  217,    2, 0x0a /* Public */,
       6,    0,  218,    2, 0x0a /* Public */,
       7,    0,  219,    2, 0x0a /* Public */,
       8,    1,  220,    2, 0x0a /* Public */,
      10,    1,  223,    2, 0x0a /* Public */,
      12,    0,  226,    2, 0x0a /* Public */,
      13,    0,  227,    2, 0x0a /* Public */,
      14,    0,  228,    2, 0x0a /* Public */,
      15,    0,  229,    2, 0x0a /* Public */,
      16,    0,  230,    2, 0x0a /* Public */,
      17,    0,  231,    2, 0x0a /* Public */,
      18,    0,  232,    2, 0x0a /* Public */,
      19,    0,  233,    2, 0x0a /* Public */,
      20,    0,  234,    2, 0x0a /* Public */,
      21,    0,  235,    2, 0x0a /* Public */,
      22,    0,  236,    2, 0x0a /* Public */,
      23,    0,  237,    2, 0x0a /* Public */,
      24,    0,  238,    2, 0x0a /* Public */,
      25,    0,  239,    2, 0x0a /* Public */,
      26,    0,  240,    2, 0x0a /* Public */,
      27,    1,  241,    2, 0x0a /* Public */,
      29,    1,  244,    2, 0x0a /* Public */,
      31,    0,  247,    2, 0x0a /* Public */,
      32,    2,  248,    2, 0x0a /* Public */,
      36,    2,  253,    2, 0x0a /* Public */,
      37,    0,  258,    2, 0x0a /* Public */,
      38,    0,  259,    2, 0x0a /* Public */,
      39,    0,  260,    2, 0x0a /* Public */,
      40,    0,  261,    2, 0x0a /* Public */,
      41,    0,  262,    2, 0x0a /* Public */,
      42,    0,  263,    2, 0x0a /* Public */,
      43,    1,  264,    2, 0x0a /* Public */,
      46,    2,  267,    2, 0x0a /* Public */,
      49,    0,  272,    2, 0x08 /* Private */,
      50,    0,  273,    2, 0x08 /* Private */,
      51,    0,  274,    2, 0x08 /* Private */,
      52,    0,  275,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   28,
    QMetaType::Void, QMetaType::Bool,   30,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 33, QMetaType::Int,   34,   35,
    QMetaType::Void, 0x80000000 | 33, QMetaType::Int,   34,   35,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 44,   45,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,   47,   48,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->allarmeInCorso(); break;
        case 1: _t->warningInCorso(); break;
        case 2: _t->newConfig(); break;
        case 3: _t->setFrequenza(); break;
        case 4: _t->setFrequenzaPemf(); break;
        case 5: _t->setTrattamento(); break;
        case 6: _t->setPotenza((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->setTempo((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->getFrequenzaPemf(); break;
        case 9: _t->getFrequenza(); break;
        case 10: _t->getTrattamento(); break;
        case 11: _t->getPotenza(); break;
        case 12: _t->getElettrodi(); break;
        case 13: _t->getTempo(); break;
        case 14: _t->incrementaPotenza(); break;
        case 15: _t->decrementaPotenza(); break;
        case 16: _t->showMenuGuida(); break;
        case 17: _t->showInfo(); break;
        case 18: _t->showAllarme(); break;
        case 19: _t->showWarning(); break;
        case 20: _t->showSetup(); break;
        case 21: _t->showCalibra(); break;
        case 22: _t->updateTabFocus(); break;
        case 23: _t->setCicala((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->rotellaMove((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->rotellaClick(); break;
        case 26: _t->procSerialRx((*reinterpret_cast< char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 27: _t->proc485Rx((*reinterpret_cast< char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 28: _t->comTask(); break;
        case 29: _t->refreshTask(); break;
        case 30: _t->startStop(); break;
        case 31: _t->startEsecuzione(); break;
        case 32: _t->stopEsecuzione(); break;
        case 33: _t->pausaEsecuzione(); break;
        case 34: _t->caricaProgramma((*reinterpret_cast< sProgramma(*)>(_a[1]))); break;
        case 35: _t->setLingua((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 36: _t->exeCalibrazione(); break;
        case 37: _t->riprendoRotella(); break;
        case 38: _t->resetAllarmi(); break;
        case 39: _t->richiediPassword(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::allarmeInCorso)) {
                *result = 0;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::warningInCorso)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 40)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 40;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 40)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 40;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::allarmeInCorso()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void MainWindow::warningInCorso()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
