/********************************************************************************
** Form generated from reading UI file 'dialogelencoprotocolli.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGELENCOPROTOCOLLI_H
#define UI_DIALOGELENCOPROTOCOLLI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <ufgbutton.h>
#include "ufgbutton1label.h"

QT_BEGIN_NAMESPACE

class Ui_dialogElencoProtocolli
{
public:
    ufgButton1label *button4;
    ufgButton *buttonExit;
    ufgButton1label *button6;
    ufgButton1label *button1;
    ufgButton1label *button3;
    ufgButton1label *button2;
    ufgButton1label *button5;
    ufgButton1label *button7;
    QLabel *labelTitolo;
    ufgButton1label *button8;
    ufgButton1label *button14;
    ufgButton1label *button13;
    ufgButton1label *button10;
    ufgButton1label *button12;
    ufgButton1label *button11;
    ufgButton1label *button9;
    ufgButton *buttonNext;

    void setupUi(QDialog *dialogElencoProtocolli)
    {
        if (dialogElencoProtocolli->objectName().isEmpty())
            dialogElencoProtocolli->setObjectName(QStringLiteral("dialogElencoProtocolli"));
        dialogElencoProtocolli->setWindowModality(Qt::WindowModal);
        dialogElencoProtocolli->resize(800, 480);
        dialogElencoProtocolli->setWindowTitle(QStringLiteral("Dialog"));
        button4 = new ufgButton1label(dialogElencoProtocolli);
        button4->setObjectName(QStringLiteral("button4"));
        button4->setGeometry(QRect(10, 260, 380, 50));
        button4->setFocusPolicy(Qt::TabFocus);
        buttonExit = new ufgButton(dialogElencoProtocolli);
        buttonExit->setObjectName(QStringLiteral("buttonExit"));
        buttonExit->setGeometry(QRect(10, 10, 80, 80));
        buttonExit->setFocusPolicy(Qt::StrongFocus);
        button6 = new ufgButton1label(dialogElencoProtocolli);
        button6->setObjectName(QStringLiteral("button6"));
        button6->setGeometry(QRect(10, 370, 380, 50));
        button6->setFocusPolicy(Qt::TabFocus);
        button1 = new ufgButton1label(dialogElencoProtocolli);
        button1->setObjectName(QStringLiteral("button1"));
        button1->setGeometry(QRect(10, 95, 380, 50));
        button1->setFocusPolicy(Qt::TabFocus);
        button3 = new ufgButton1label(dialogElencoProtocolli);
        button3->setObjectName(QStringLiteral("button3"));
        button3->setGeometry(QRect(10, 205, 380, 50));
        button3->setFocusPolicy(Qt::TabFocus);
        button2 = new ufgButton1label(dialogElencoProtocolli);
        button2->setObjectName(QStringLiteral("button2"));
        button2->setGeometry(QRect(10, 150, 380, 50));
        button2->setFocusPolicy(Qt::TabFocus);
        button5 = new ufgButton1label(dialogElencoProtocolli);
        button5->setObjectName(QStringLiteral("button5"));
        button5->setGeometry(QRect(10, 315, 380, 50));
        button5->setFocusPolicy(Qt::TabFocus);
        button7 = new ufgButton1label(dialogElencoProtocolli);
        button7->setObjectName(QStringLiteral("button7"));
        button7->setGeometry(QRect(10, 425, 380, 50));
        button7->setFocusPolicy(Qt::TabFocus);
        labelTitolo = new QLabel(dialogElencoProtocolli);
        labelTitolo->setObjectName(QStringLiteral("labelTitolo"));
        labelTitolo->setGeometry(QRect(116, 10, 571, 81));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(20);
        font.setBold(false);
        font.setWeight(50);
        labelTitolo->setFont(font);
        labelTitolo->setStyleSheet(QStringLiteral("color: rgba(0, 0, 255, 231);"));
        labelTitolo->setFrameShape(QFrame::Box);
        labelTitolo->setFrameShadow(QFrame::Plain);
        labelTitolo->setText(QStringLiteral("TextLabel"));
        button8 = new ufgButton1label(dialogElencoProtocolli);
        button8->setObjectName(QStringLiteral("button8"));
        button8->setGeometry(QRect(410, 95, 380, 50));
        button8->setFocusPolicy(Qt::TabFocus);
        button14 = new ufgButton1label(dialogElencoProtocolli);
        button14->setObjectName(QStringLiteral("button14"));
        button14->setGeometry(QRect(410, 425, 380, 50));
        button14->setFocusPolicy(Qt::TabFocus);
        button13 = new ufgButton1label(dialogElencoProtocolli);
        button13->setObjectName(QStringLiteral("button13"));
        button13->setGeometry(QRect(410, 370, 380, 50));
        button13->setFocusPolicy(Qt::TabFocus);
        button10 = new ufgButton1label(dialogElencoProtocolli);
        button10->setObjectName(QStringLiteral("button10"));
        button10->setGeometry(QRect(410, 205, 380, 50));
        button10->setFocusPolicy(Qt::TabFocus);
        button12 = new ufgButton1label(dialogElencoProtocolli);
        button12->setObjectName(QStringLiteral("button12"));
        button12->setGeometry(QRect(410, 315, 380, 50));
        button12->setFocusPolicy(Qt::TabFocus);
        button11 = new ufgButton1label(dialogElencoProtocolli);
        button11->setObjectName(QStringLiteral("button11"));
        button11->setGeometry(QRect(410, 260, 380, 50));
        button11->setFocusPolicy(Qt::TabFocus);
        button9 = new ufgButton1label(dialogElencoProtocolli);
        button9->setObjectName(QStringLiteral("button9"));
        button9->setGeometry(QRect(410, 150, 380, 50));
        button9->setFocusPolicy(Qt::TabFocus);
        buttonNext = new ufgButton(dialogElencoProtocolli);
        buttonNext->setObjectName(QStringLiteral("buttonNext"));
        buttonNext->setGeometry(QRect(710, 10, 80, 80));
        buttonNext->setFocusPolicy(Qt::StrongFocus);
        QWidget::setTabOrder(buttonExit, button1);
        QWidget::setTabOrder(button1, button2);
        QWidget::setTabOrder(button2, button3);
        QWidget::setTabOrder(button3, button4);
        QWidget::setTabOrder(button4, button5);
        QWidget::setTabOrder(button5, button6);
        QWidget::setTabOrder(button6, button7);
        QWidget::setTabOrder(button7, button8);
        QWidget::setTabOrder(button8, button9);
        QWidget::setTabOrder(button9, button10);
        QWidget::setTabOrder(button10, button11);
        QWidget::setTabOrder(button11, button12);
        QWidget::setTabOrder(button12, button13);
        QWidget::setTabOrder(button13, button14);
        QWidget::setTabOrder(button14, buttonNext);

        retranslateUi(dialogElencoProtocolli);

        QMetaObject::connectSlotsByName(dialogElencoProtocolli);
    } // setupUi

    void retranslateUi(QDialog *dialogElencoProtocolli)
    {
        Q_UNUSED(dialogElencoProtocolli);
    } // retranslateUi

};

namespace Ui {
    class dialogElencoProtocolli: public Ui_dialogElencoProtocolli {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGELENCOPROTOCOLLI_H
