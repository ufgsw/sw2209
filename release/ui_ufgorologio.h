/********************************************************************************
** Form generated from reading UI file 'ufgorologio.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UFGOROLOGIO_H
#define UI_UFGOROLOGIO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ufgOrologio
{
public:

    void setupUi(QWidget *ufgOrologio)
    {
        if (ufgOrologio->objectName().isEmpty())
            ufgOrologio->setObjectName(QStringLiteral("ufgOrologio"));
        ufgOrologio->resize(400, 300);

        retranslateUi(ufgOrologio);

        QMetaObject::connectSlotsByName(ufgOrologio);
    } // setupUi

    void retranslateUi(QWidget *ufgOrologio)
    {
        ufgOrologio->setWindowTitle(QApplication::translate("ufgOrologio", "Form", 0));
    } // retranslateUi

};

namespace Ui {
    class ufgOrologio: public Ui_ufgOrologio {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UFGOROLOGIO_H
