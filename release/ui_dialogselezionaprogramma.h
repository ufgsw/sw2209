/********************************************************************************
** Form generated from reading UI file 'dialogselezionaprogramma.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGSELEZIONAPROGRAMMA_H
#define UI_DIALOGSELEZIONAPROGRAMMA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <ufgbutton.h>

QT_BEGIN_NAMESPACE

class Ui_dialogSelezionaProgramma
{
public:
    ufgButton *buttonExit;
    QLabel *labelProtocollo;
    QLabel *labelProogramma;
    QLabel *textPosizionePaziente1;
    QLabel *labelPosizioneNeutra1;
    QLabel *textPosizioneNeutro1;
    QLabel *labelPosizioneElettrodo1;
    QLabel *textPosizioneElettrodo1;
    QLabel *labelPosizionePaziente1;
    QLabel *labelElettrodo1;
    QLabel *textElettrodo1;
    QLabel *textTempo1;
    QLabel *textPotenza1;
    QLabel *textModalita1;
    QLabel *textFrequenza1;
    ufgButton *buttonProgramma1;
    QLabel *textPotenza2;
    QLabel *textElettrodo2;
    QLabel *textPosizioneElettrodo2;
    ufgButton *buttonProgramma2;
    QLabel *labelPosizionePaziente2;
    QLabel *labelElettrodo2;
    QLabel *textPosizionePaziente2;
    QLabel *labelPosizioneNeutra2;
    QLabel *textModalita2;
    QLabel *textFrequenza2;
    QLabel *textTempo2;
    QLabel *textPosizioneNeutro2;
    QLabel *labelPosizioneElettrodo2;
    QLabel *textPotenza3;
    QLabel *textElettrodo3;
    QLabel *textPosizioneElettrodo3;
    ufgButton *buttonProgramma3;
    QLabel *labelPosizionePaziente3;
    QLabel *labelElettrodo3;
    QLabel *textPosizionePaziente3;
    QLabel *labelPosizioneNeutra3;
    QLabel *textModalita3;
    QLabel *textFrequenza3;
    QLabel *textTempo3;
    QLabel *textPosizioneNeutro3;
    QLabel *labelPosizioneElettrodo3;
    QLabel *textPotenza4;
    QLabel *textElettrodo4;
    QLabel *textPosizioneElettrodo4;
    ufgButton *buttonProgramma4;
    QLabel *labelPosizionePaziente4;
    QLabel *labelElettrodo4;
    QLabel *textPosizionePaziente4;
    QLabel *labelPosizioneNeutra4;
    QLabel *textModalita4;
    QLabel *textFrequenza4;
    QLabel *textTempo4;
    QLabel *textPosizioneNeutro4;
    QLabel *labelPosizioneElettrodo4;
    QLabel *textSlf1;
    QLabel *textSlf2;
    QLabel *textSlf3;
    QLabel *textSlf4;

    void setupUi(QDialog *dialogSelezionaProgramma)
    {
        if (dialogSelezionaProgramma->objectName().isEmpty())
            dialogSelezionaProgramma->setObjectName(QStringLiteral("dialogSelezionaProgramma"));
        dialogSelezionaProgramma->setWindowModality(Qt::WindowModal);
        dialogSelezionaProgramma->resize(800, 480);
        dialogSelezionaProgramma->setMinimumSize(QSize(800, 480));
        dialogSelezionaProgramma->setMaximumSize(QSize(800, 480));
        dialogSelezionaProgramma->setContextMenuPolicy(Qt::NoContextMenu);
        dialogSelezionaProgramma->setWindowTitle(QStringLiteral("Dialog"));
        buttonExit = new ufgButton(dialogSelezionaProgramma);
        buttonExit->setObjectName(QStringLiteral("buttonExit"));
        buttonExit->setGeometry(QRect(10, 10, 80, 80));
        buttonExit->setFocusPolicy(Qt::StrongFocus);
        labelProtocollo = new QLabel(dialogSelezionaProgramma);
        labelProtocollo->setObjectName(QStringLiteral("labelProtocollo"));
        labelProtocollo->setGeometry(QRect(150, 10, 641, 35));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(18);
        labelProtocollo->setFont(font);
        labelProtocollo->setText(QStringLiteral("TextLabel"));
        labelProogramma = new QLabel(dialogSelezionaProgramma);
        labelProogramma->setObjectName(QStringLiteral("labelProogramma"));
        labelProogramma->setGeometry(QRect(150, 50, 641, 35));
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        font1.setPointSize(14);
        labelProogramma->setFont(font1);
        labelProogramma->setStyleSheet(QStringLiteral("color: rgba(0, 0, 255, 231);"));
        labelProogramma->setText(QStringLiteral("TextLabel"));
        textPosizionePaziente1 = new QLabel(dialogSelezionaProgramma);
        textPosizionePaziente1->setObjectName(QStringLiteral("textPosizionePaziente1"));
        textPosizionePaziente1->setGeometry(QRect(15, 120, 180, 17));
        QFont font2;
        font2.setFamily(QStringLiteral("DejaVu Sans"));
        font2.setPointSize(9);
        textPosizionePaziente1->setFont(font2);
        textPosizionePaziente1->setText(QStringLiteral("TextLabel"));
        labelPosizioneNeutra1 = new QLabel(dialogSelezionaProgramma);
        labelPosizioneNeutra1->setObjectName(QStringLiteral("labelPosizioneNeutra1"));
        labelPosizioneNeutra1->setGeometry(QRect(15, 140, 170, 17));
        labelPosizioneNeutra1->setFont(font2);
        labelPosizioneNeutra1->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizioneNeutra1->setText(QStringLiteral("Posizione Neutro :"));
        textPosizioneNeutro1 = new QLabel(dialogSelezionaProgramma);
        textPosizioneNeutro1->setObjectName(QStringLiteral("textPosizioneNeutro1"));
        textPosizioneNeutro1->setGeometry(QRect(15, 160, 180, 17));
        textPosizioneNeutro1->setFont(font2);
        textPosizioneNeutro1->setText(QStringLiteral("TextLabel"));
        labelPosizioneElettrodo1 = new QLabel(dialogSelezionaProgramma);
        labelPosizioneElettrodo1->setObjectName(QStringLiteral("labelPosizioneElettrodo1"));
        labelPosizioneElettrodo1->setGeometry(QRect(15, 220, 170, 17));
        labelPosizioneElettrodo1->setFont(font2);
        labelPosizioneElettrodo1->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizioneElettrodo1->setText(QStringLiteral("Posizione elettrodo :"));
        textPosizioneElettrodo1 = new QLabel(dialogSelezionaProgramma);
        textPosizioneElettrodo1->setObjectName(QStringLiteral("textPosizioneElettrodo1"));
        textPosizioneElettrodo1->setGeometry(QRect(15, 240, 180, 45));
        textPosizioneElettrodo1->setFont(font2);
        textPosizioneElettrodo1->setText(QLatin1String("abcdefghilmnopqrstuvz\n"
"riga2\n"
"riga3"));
        textPosizioneElettrodo1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        labelPosizionePaziente1 = new QLabel(dialogSelezionaProgramma);
        labelPosizionePaziente1->setObjectName(QStringLiteral("labelPosizionePaziente1"));
        labelPosizionePaziente1->setGeometry(QRect(15, 100, 170, 17));
        labelPosizionePaziente1->setFont(font2);
        labelPosizionePaziente1->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizionePaziente1->setText(QStringLiteral("Posizione paziente :"));
        labelElettrodo1 = new QLabel(dialogSelezionaProgramma);
        labelElettrodo1->setObjectName(QStringLiteral("labelElettrodo1"));
        labelElettrodo1->setGeometry(QRect(15, 180, 170, 17));
        labelElettrodo1->setFont(font2);
        labelElettrodo1->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelElettrodo1->setText(QStringLiteral("Elettrodo :"));
        textElettrodo1 = new QLabel(dialogSelezionaProgramma);
        textElettrodo1->setObjectName(QStringLiteral("textElettrodo1"));
        textElettrodo1->setGeometry(QRect(15, 200, 180, 17));
        textElettrodo1->setFont(font2);
        textElettrodo1->setText(QStringLiteral("TextLabel"));
        textTempo1 = new QLabel(dialogSelezionaProgramma);
        textTempo1->setObjectName(QStringLiteral("textTempo1"));
        textTempo1->setGeometry(QRect(15, 300, 170, 17));
        QFont font3;
        font3.setFamily(QStringLiteral("DejaVu Sans"));
        font3.setPointSize(10);
        textTempo1->setFont(font3);
        textTempo1->setText(QStringLiteral("TEMPO : 20:00"));
        textPotenza1 = new QLabel(dialogSelezionaProgramma);
        textPotenza1->setObjectName(QStringLiteral("textPotenza1"));
        textPotenza1->setGeometry(QRect(15, 320, 170, 17));
        textPotenza1->setFont(font3);
        textPotenza1->setText(QStringLiteral("POTENZA : 20%"));
        textModalita1 = new QLabel(dialogSelezionaProgramma);
        textModalita1->setObjectName(QStringLiteral("textModalita1"));
        textModalita1->setGeometry(QRect(15, 340, 170, 17));
        textModalita1->setFont(font3);
        textModalita1->setText(QStringLiteral("MODALITA' : RESISTIVO"));
        textFrequenza1 = new QLabel(dialogSelezionaProgramma);
        textFrequenza1->setObjectName(QStringLiteral("textFrequenza1"));
        textFrequenza1->setGeometry(QRect(15, 360, 170, 17));
        textFrequenza1->setFont(font3);
        textFrequenza1->setText(QStringLiteral("FREQUENZA : 0,46MHz"));
        buttonProgramma1 = new ufgButton(dialogSelezionaProgramma);
        buttonProgramma1->setObjectName(QStringLiteral("buttonProgramma1"));
        buttonProgramma1->setGeometry(QRect(50, 400, 71, 71));
        buttonProgramma1->setFocusPolicy(Qt::StrongFocus);
        textPotenza2 = new QLabel(dialogSelezionaProgramma);
        textPotenza2->setObjectName(QStringLiteral("textPotenza2"));
        textPotenza2->setGeometry(QRect(215, 320, 170, 17));
        textPotenza2->setFont(font3);
        textPotenza2->setText(QStringLiteral("POTENZA : 20%"));
        textElettrodo2 = new QLabel(dialogSelezionaProgramma);
        textElettrodo2->setObjectName(QStringLiteral("textElettrodo2"));
        textElettrodo2->setGeometry(QRect(215, 200, 180, 17));
        textElettrodo2->setFont(font2);
        textElettrodo2->setText(QStringLiteral("TextLabel"));
        textPosizioneElettrodo2 = new QLabel(dialogSelezionaProgramma);
        textPosizioneElettrodo2->setObjectName(QStringLiteral("textPosizioneElettrodo2"));
        textPosizioneElettrodo2->setGeometry(QRect(215, 240, 180, 45));
        textPosizioneElettrodo2->setFont(font2);
        textPosizioneElettrodo2->setText(QStringLiteral("TextLabel"));
        textPosizioneElettrodo2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        buttonProgramma2 = new ufgButton(dialogSelezionaProgramma);
        buttonProgramma2->setObjectName(QStringLiteral("buttonProgramma2"));
        buttonProgramma2->setGeometry(QRect(255, 398, 71, 71));
        buttonProgramma2->setFocusPolicy(Qt::StrongFocus);
        labelPosizionePaziente2 = new QLabel(dialogSelezionaProgramma);
        labelPosizionePaziente2->setObjectName(QStringLiteral("labelPosizionePaziente2"));
        labelPosizionePaziente2->setGeometry(QRect(215, 100, 180, 17));
        labelPosizionePaziente2->setFont(font2);
        labelPosizionePaziente2->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizionePaziente2->setText(QStringLiteral("Posizione paziente :"));
        labelElettrodo2 = new QLabel(dialogSelezionaProgramma);
        labelElettrodo2->setObjectName(QStringLiteral("labelElettrodo2"));
        labelElettrodo2->setGeometry(QRect(215, 180, 180, 17));
        labelElettrodo2->setFont(font2);
        labelElettrodo2->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelElettrodo2->setText(QStringLiteral("Elettrodo :"));
        textPosizionePaziente2 = new QLabel(dialogSelezionaProgramma);
        textPosizionePaziente2->setObjectName(QStringLiteral("textPosizionePaziente2"));
        textPosizionePaziente2->setGeometry(QRect(215, 120, 180, 17));
        textPosizionePaziente2->setFont(font2);
        textPosizionePaziente2->setText(QStringLiteral("TextLabel"));
        labelPosizioneNeutra2 = new QLabel(dialogSelezionaProgramma);
        labelPosizioneNeutra2->setObjectName(QStringLiteral("labelPosizioneNeutra2"));
        labelPosizioneNeutra2->setGeometry(QRect(215, 140, 180, 17));
        labelPosizioneNeutra2->setFont(font2);
        labelPosizioneNeutra2->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizioneNeutra2->setText(QStringLiteral("Posizione Neutro :"));
        textModalita2 = new QLabel(dialogSelezionaProgramma);
        textModalita2->setObjectName(QStringLiteral("textModalita2"));
        textModalita2->setGeometry(QRect(215, 340, 170, 17));
        textModalita2->setFont(font3);
        textModalita2->setText(QStringLiteral("MODALITA' : RESISTIVO"));
        textFrequenza2 = new QLabel(dialogSelezionaProgramma);
        textFrequenza2->setObjectName(QStringLiteral("textFrequenza2"));
        textFrequenza2->setGeometry(QRect(215, 360, 170, 17));
        textFrequenza2->setFont(font3);
        textFrequenza2->setText(QStringLiteral("FREQUENZA : 0,46MHz"));
        textTempo2 = new QLabel(dialogSelezionaProgramma);
        textTempo2->setObjectName(QStringLiteral("textTempo2"));
        textTempo2->setGeometry(QRect(215, 300, 170, 17));
        textTempo2->setFont(font3);
        textTempo2->setText(QStringLiteral("TEMPO : 20:00"));
        textPosizioneNeutro2 = new QLabel(dialogSelezionaProgramma);
        textPosizioneNeutro2->setObjectName(QStringLiteral("textPosizioneNeutro2"));
        textPosizioneNeutro2->setGeometry(QRect(215, 160, 180, 17));
        textPosizioneNeutro2->setFont(font2);
        textPosizioneNeutro2->setText(QStringLiteral("TextLabel"));
        labelPosizioneElettrodo2 = new QLabel(dialogSelezionaProgramma);
        labelPosizioneElettrodo2->setObjectName(QStringLiteral("labelPosizioneElettrodo2"));
        labelPosizioneElettrodo2->setGeometry(QRect(215, 220, 180, 17));
        labelPosizioneElettrodo2->setFont(font2);
        labelPosizioneElettrodo2->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizioneElettrodo2->setText(QStringLiteral("Posizione elettrodo :"));
        textPotenza3 = new QLabel(dialogSelezionaProgramma);
        textPotenza3->setObjectName(QStringLiteral("textPotenza3"));
        textPotenza3->setGeometry(QRect(415, 320, 170, 17));
        textPotenza3->setFont(font3);
        textPotenza3->setText(QStringLiteral("POTENZA : 20%"));
        textElettrodo3 = new QLabel(dialogSelezionaProgramma);
        textElettrodo3->setObjectName(QStringLiteral("textElettrodo3"));
        textElettrodo3->setGeometry(QRect(415, 200, 180, 17));
        textElettrodo3->setFont(font2);
        textElettrodo3->setText(QStringLiteral("TextLabel"));
        textPosizioneElettrodo3 = new QLabel(dialogSelezionaProgramma);
        textPosizioneElettrodo3->setObjectName(QStringLiteral("textPosizioneElettrodo3"));
        textPosizioneElettrodo3->setGeometry(QRect(415, 240, 180, 45));
        textPosizioneElettrodo3->setFont(font2);
        textPosizioneElettrodo3->setText(QStringLiteral("TextLabel"));
        textPosizioneElettrodo3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        buttonProgramma3 = new ufgButton(dialogSelezionaProgramma);
        buttonProgramma3->setObjectName(QStringLiteral("buttonProgramma3"));
        buttonProgramma3->setGeometry(QRect(455, 398, 71, 71));
        buttonProgramma3->setFocusPolicy(Qt::StrongFocus);
        labelPosizionePaziente3 = new QLabel(dialogSelezionaProgramma);
        labelPosizionePaziente3->setObjectName(QStringLiteral("labelPosizionePaziente3"));
        labelPosizionePaziente3->setGeometry(QRect(415, 100, 180, 17));
        labelPosizionePaziente3->setFont(font2);
        labelPosizionePaziente3->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizionePaziente3->setText(QStringLiteral("Posizione paziente :"));
        labelElettrodo3 = new QLabel(dialogSelezionaProgramma);
        labelElettrodo3->setObjectName(QStringLiteral("labelElettrodo3"));
        labelElettrodo3->setGeometry(QRect(415, 180, 180, 17));
        labelElettrodo3->setFont(font2);
        labelElettrodo3->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelElettrodo3->setText(QStringLiteral("Elettrodo :"));
        textPosizionePaziente3 = new QLabel(dialogSelezionaProgramma);
        textPosizionePaziente3->setObjectName(QStringLiteral("textPosizionePaziente3"));
        textPosizionePaziente3->setGeometry(QRect(415, 120, 180, 17));
        textPosizionePaziente3->setFont(font2);
        textPosizionePaziente3->setText(QStringLiteral("TextLabel"));
        labelPosizioneNeutra3 = new QLabel(dialogSelezionaProgramma);
        labelPosizioneNeutra3->setObjectName(QStringLiteral("labelPosizioneNeutra3"));
        labelPosizioneNeutra3->setGeometry(QRect(415, 140, 180, 17));
        labelPosizioneNeutra3->setFont(font2);
        labelPosizioneNeutra3->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizioneNeutra3->setText(QStringLiteral("Posizione Neutro :"));
        textModalita3 = new QLabel(dialogSelezionaProgramma);
        textModalita3->setObjectName(QStringLiteral("textModalita3"));
        textModalita3->setGeometry(QRect(415, 340, 170, 17));
        textModalita3->setFont(font3);
        textModalita3->setText(QStringLiteral("MODALITA' : RESISTIVO"));
        textFrequenza3 = new QLabel(dialogSelezionaProgramma);
        textFrequenza3->setObjectName(QStringLiteral("textFrequenza3"));
        textFrequenza3->setGeometry(QRect(415, 360, 170, 17));
        textFrequenza3->setFont(font3);
        textFrequenza3->setText(QStringLiteral("FREQUENZA : 0,46MHz"));
        textTempo3 = new QLabel(dialogSelezionaProgramma);
        textTempo3->setObjectName(QStringLiteral("textTempo3"));
        textTempo3->setGeometry(QRect(415, 300, 170, 17));
        textTempo3->setFont(font3);
        textTempo3->setText(QStringLiteral("TEMPO : 20:00"));
        textPosizioneNeutro3 = new QLabel(dialogSelezionaProgramma);
        textPosizioneNeutro3->setObjectName(QStringLiteral("textPosizioneNeutro3"));
        textPosizioneNeutro3->setGeometry(QRect(415, 160, 180, 17));
        textPosizioneNeutro3->setFont(font2);
        textPosizioneNeutro3->setText(QStringLiteral("TextLabel"));
        labelPosizioneElettrodo3 = new QLabel(dialogSelezionaProgramma);
        labelPosizioneElettrodo3->setObjectName(QStringLiteral("labelPosizioneElettrodo3"));
        labelPosizioneElettrodo3->setGeometry(QRect(415, 220, 180, 17));
        labelPosizioneElettrodo3->setFont(font2);
        labelPosizioneElettrodo3->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizioneElettrodo3->setText(QStringLiteral("Posizione elettrodo :"));
        textPotenza4 = new QLabel(dialogSelezionaProgramma);
        textPotenza4->setObjectName(QStringLiteral("textPotenza4"));
        textPotenza4->setGeometry(QRect(615, 320, 170, 17));
        textPotenza4->setFont(font3);
        textPotenza4->setText(QStringLiteral("POTENZA : 20%"));
        textElettrodo4 = new QLabel(dialogSelezionaProgramma);
        textElettrodo4->setObjectName(QStringLiteral("textElettrodo4"));
        textElettrodo4->setGeometry(QRect(615, 200, 180, 17));
        textElettrodo4->setFont(font2);
        textElettrodo4->setText(QStringLiteral("TextLabel"));
        textPosizioneElettrodo4 = new QLabel(dialogSelezionaProgramma);
        textPosizioneElettrodo4->setObjectName(QStringLiteral("textPosizioneElettrodo4"));
        textPosizioneElettrodo4->setGeometry(QRect(615, 240, 180, 45));
        textPosizioneElettrodo4->setFont(font2);
        textPosizioneElettrodo4->setText(QStringLiteral("TextLabel"));
        textPosizioneElettrodo4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        buttonProgramma4 = new ufgButton(dialogSelezionaProgramma);
        buttonProgramma4->setObjectName(QStringLiteral("buttonProgramma4"));
        buttonProgramma4->setGeometry(QRect(655, 398, 71, 71));
        buttonProgramma4->setFocusPolicy(Qt::StrongFocus);
        labelPosizionePaziente4 = new QLabel(dialogSelezionaProgramma);
        labelPosizionePaziente4->setObjectName(QStringLiteral("labelPosizionePaziente4"));
        labelPosizionePaziente4->setGeometry(QRect(615, 100, 180, 17));
        labelPosizionePaziente4->setFont(font2);
        labelPosizionePaziente4->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizionePaziente4->setText(QStringLiteral("Posizione paziente :"));
        labelElettrodo4 = new QLabel(dialogSelezionaProgramma);
        labelElettrodo4->setObjectName(QStringLiteral("labelElettrodo4"));
        labelElettrodo4->setGeometry(QRect(615, 180, 180, 17));
        labelElettrodo4->setFont(font2);
        labelElettrodo4->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelElettrodo4->setText(QStringLiteral("Elettrodo :"));
        textPosizionePaziente4 = new QLabel(dialogSelezionaProgramma);
        textPosizionePaziente4->setObjectName(QStringLiteral("textPosizionePaziente4"));
        textPosizionePaziente4->setGeometry(QRect(615, 120, 180, 17));
        textPosizionePaziente4->setFont(font2);
        textPosizionePaziente4->setText(QStringLiteral("TextLabel"));
        labelPosizioneNeutra4 = new QLabel(dialogSelezionaProgramma);
        labelPosizioneNeutra4->setObjectName(QStringLiteral("labelPosizioneNeutra4"));
        labelPosizioneNeutra4->setGeometry(QRect(615, 140, 180, 17));
        labelPosizioneNeutra4->setFont(font2);
        labelPosizioneNeutra4->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizioneNeutra4->setText(QStringLiteral("Posizione Neutro :"));
        textModalita4 = new QLabel(dialogSelezionaProgramma);
        textModalita4->setObjectName(QStringLiteral("textModalita4"));
        textModalita4->setGeometry(QRect(615, 340, 170, 17));
        textModalita4->setFont(font3);
        textModalita4->setText(QStringLiteral("MODALITA' : RESISTIVO"));
        textFrequenza4 = new QLabel(dialogSelezionaProgramma);
        textFrequenza4->setObjectName(QStringLiteral("textFrequenza4"));
        textFrequenza4->setGeometry(QRect(615, 360, 170, 17));
        textFrequenza4->setFont(font3);
        textFrequenza4->setText(QStringLiteral("FREQUENZA : 0,46MHz"));
        textTempo4 = new QLabel(dialogSelezionaProgramma);
        textTempo4->setObjectName(QStringLiteral("textTempo4"));
        textTempo4->setGeometry(QRect(615, 300, 170, 17));
        textTempo4->setFont(font3);
        textTempo4->setText(QStringLiteral("TEMPO : 20:00"));
        textPosizioneNeutro4 = new QLabel(dialogSelezionaProgramma);
        textPosizioneNeutro4->setObjectName(QStringLiteral("textPosizioneNeutro4"));
        textPosizioneNeutro4->setGeometry(QRect(615, 160, 180, 17));
        textPosizioneNeutro4->setFont(font2);
        textPosizioneNeutro4->setText(QStringLiteral("TextLabel"));
        labelPosizioneElettrodo4 = new QLabel(dialogSelezionaProgramma);
        labelPosizioneElettrodo4->setObjectName(QStringLiteral("labelPosizioneElettrodo4"));
        labelPosizioneElettrodo4->setGeometry(QRect(615, 220, 180, 17));
        labelPosizioneElettrodo4->setFont(font2);
        labelPosizioneElettrodo4->setStyleSheet(QStringLiteral("color: rgb(72, 209, 204);"));
        labelPosizioneElettrodo4->setText(QStringLiteral("Posizione elettrodo :"));
        textSlf1 = new QLabel(dialogSelezionaProgramma);
        textSlf1->setObjectName(QStringLiteral("textSlf1"));
        textSlf1->setGeometry(QRect(15, 380, 170, 17));
        textSlf1->setFont(font3);
        textSlf1->setText(QStringLiteral("SLF"));
        textSlf2 = new QLabel(dialogSelezionaProgramma);
        textSlf2->setObjectName(QStringLiteral("textSlf2"));
        textSlf2->setGeometry(QRect(215, 380, 170, 17));
        textSlf2->setFont(font3);
        textSlf2->setText(QStringLiteral("SLF"));
        textSlf3 = new QLabel(dialogSelezionaProgramma);
        textSlf3->setObjectName(QStringLiteral("textSlf3"));
        textSlf3->setGeometry(QRect(415, 380, 170, 17));
        textSlf3->setFont(font3);
        textSlf3->setText(QStringLiteral("SLF"));
        textSlf4 = new QLabel(dialogSelezionaProgramma);
        textSlf4->setObjectName(QStringLiteral("textSlf4"));
        textSlf4->setGeometry(QRect(615, 380, 170, 17));
        textSlf4->setFont(font3);
        textSlf4->setText(QStringLiteral("SLF"));

        retranslateUi(dialogSelezionaProgramma);

        QMetaObject::connectSlotsByName(dialogSelezionaProgramma);
    } // setupUi

    void retranslateUi(QDialog *dialogSelezionaProgramma)
    {
        Q_UNUSED(dialogSelezionaProgramma);
    } // retranslateUi

};

namespace Ui {
    class dialogSelezionaProgramma: public Ui_dialogSelezionaProgramma {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGSELEZIONAPROGRAMMA_H
