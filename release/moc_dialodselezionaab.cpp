/****************************************************************************
** Meta object code from reading C++ file 'dialodselezionaab.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../sw2209_cina/dialodselezionaab.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialodselezionaab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_dialodSelezionaAB_t {
    QByteArrayData data[9];
    char stringdata0[86];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_dialodSelezionaAB_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_dialodSelezionaAB_t qt_meta_stringdata_dialodSelezionaAB = {
    {
QT_MOC_LITERAL(0, 0, 17), // "dialodSelezionaAB"
QT_MOC_LITERAL(1, 18, 7), // "onClose"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 12), // "selezionatoA"
QT_MOC_LITERAL(4, 40, 12), // "selezionatoB"
QT_MOC_LITERAL(5, 53, 4), // "esci"
QT_MOC_LITERAL(6, 58, 11), // "rotellaMove"
QT_MOC_LITERAL(7, 70, 2), // "up"
QT_MOC_LITERAL(8, 73, 12) // "rotellaClick"

    },
    "dialodSelezionaAB\0onClose\0\0selezionatoA\0"
    "selezionatoB\0esci\0rotellaMove\0up\0"
    "rotellaClick"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_dialodSelezionaAB[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   45,    2, 0x0a /* Public */,
       4,    0,   46,    2, 0x0a /* Public */,
       5,    0,   47,    2, 0x0a /* Public */,
       6,    1,   48,    2, 0x0a /* Public */,
       8,    0,   51,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void,

       0        // eod
};

void dialodSelezionaAB::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        dialodSelezionaAB *_t = static_cast<dialodSelezionaAB *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onClose(); break;
        case 1: _t->selezionatoA(); break;
        case 2: _t->selezionatoB(); break;
        case 3: _t->esci(); break;
        case 4: _t->rotellaMove((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->rotellaClick(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (dialodSelezionaAB::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialodSelezionaAB::onClose)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject dialodSelezionaAB::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_dialodSelezionaAB.data,
      qt_meta_data_dialodSelezionaAB,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *dialodSelezionaAB::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *dialodSelezionaAB::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_dialodSelezionaAB.stringdata0))
        return static_cast<void*>(const_cast< dialodSelezionaAB*>(this));
    return QDialog::qt_metacast(_clname);
}

int dialodSelezionaAB::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void dialodSelezionaAB::onClose()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
