/********************************************************************************
** Form generated from reading UI file 'dialogstartcalibrazione.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGSTARTCALIBRAZIONE_H
#define UI_DIALOGSTARTCALIBRAZIONE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <ufgbutton.h>

QT_BEGIN_NAMESPACE

class Ui_dialogStartCalibrazione
{
public:
    ufgButton *buttonExit;
    ufgButton *buttonCalibra;
    QLabel *labelMessaggio;
    QLabel *labelTitolo;

    void setupUi(QDialog *dialogStartCalibrazione)
    {
        if (dialogStartCalibrazione->objectName().isEmpty())
            dialogStartCalibrazione->setObjectName(QStringLiteral("dialogStartCalibrazione"));
        dialogStartCalibrazione->setWindowModality(Qt::WindowModal);
        dialogStartCalibrazione->resize(800, 480);
        dialogStartCalibrazione->setWindowTitle(QStringLiteral("Dialog"));
        buttonExit = new ufgButton(dialogStartCalibrazione);
        buttonExit->setObjectName(QStringLiteral("buttonExit"));
        buttonExit->setGeometry(QRect(10, 10, 80, 80));
        buttonCalibra = new ufgButton(dialogStartCalibrazione);
        buttonCalibra->setObjectName(QStringLiteral("buttonCalibra"));
        buttonCalibra->setGeometry(QRect(340, 370, 80, 80));
        labelMessaggio = new QLabel(dialogStartCalibrazione);
        labelMessaggio->setObjectName(QStringLiteral("labelMessaggio"));
        labelMessaggio->setGeometry(QRect(116, 126, 521, 201));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(18);
        labelMessaggio->setFont(font);
        labelMessaggio->setStyleSheet(QStringLiteral("color: rgb(255, 0, 0);"));
        labelMessaggio->setInputMethodHints(Qt::ImhMultiLine);
        labelMessaggio->setText(QStringLiteral("MSG"));
        labelMessaggio->setTextFormat(Qt::AutoText);
        labelMessaggio->setScaledContents(false);
        labelMessaggio->setAlignment(Qt::AlignCenter);
        labelMessaggio->setWordWrap(true);
        labelMessaggio->setMargin(2);
        labelMessaggio->setIndent(2);
        labelMessaggio->setTextInteractionFlags(Qt::NoTextInteraction);
        labelTitolo = new QLabel(dialogStartCalibrazione);
        labelTitolo->setObjectName(QStringLiteral("labelTitolo"));
        labelTitolo->setGeometry(QRect(106, 9, 581, 81));
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        font1.setPointSize(20);
        labelTitolo->setFont(font1);
        labelTitolo->setText(QStringLiteral("TextLabel"));
        labelTitolo->setAlignment(Qt::AlignCenter);
        labelTitolo->setWordWrap(true);

        retranslateUi(dialogStartCalibrazione);

        QMetaObject::connectSlotsByName(dialogStartCalibrazione);
    } // setupUi

    void retranslateUi(QDialog *dialogStartCalibrazione)
    {
        Q_UNUSED(dialogStartCalibrazione);
    } // retranslateUi

};

namespace Ui {
    class dialogStartCalibrazione: public Ui_dialogStartCalibrazione {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGSTARTCALIBRAZIONE_H
