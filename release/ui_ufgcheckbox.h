/********************************************************************************
** Form generated from reading UI file 'ufgcheckbox.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UFGCHECKBOX_H
#define UI_UFGCHECKBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ufgCheckBox
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *labelImmagine;
    QLabel *labelTesto;

    void setupUi(QWidget *ufgCheckBox)
    {
        if (ufgCheckBox->objectName().isEmpty())
            ufgCheckBox->setObjectName(QStringLiteral("ufgCheckBox"));
        ufgCheckBox->resize(300, 60);
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        ufgCheckBox->setFont(font);
        ufgCheckBox->setWindowTitle(QStringLiteral(""));
        gridLayout = new QGridLayout(ufgCheckBox);
        gridLayout->setSpacing(3);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(12, 3, 3, 3);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(4);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(4, 2, 4, 2);
        labelImmagine = new QLabel(ufgCheckBox);
        labelImmagine->setObjectName(QStringLiteral("labelImmagine"));
        labelImmagine->setMaximumSize(QSize(48, 48));
        labelImmagine->setText(QStringLiteral(""));
        labelImmagine->setPixmap(QPixmap(QString::fromUtf8(":/img/checkDS.png")));
        labelImmagine->setScaledContents(true);

        horizontalLayout->addWidget(labelImmagine);

        labelTesto = new QLabel(ufgCheckBox);
        labelTesto->setObjectName(QStringLiteral("labelTesto"));
        labelTesto->setMinimumSize(QSize(0, 50));
        labelTesto->setMaximumSize(QSize(16777215, 50));
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setWeight(50);
        labelTesto->setFont(font1);
        labelTesto->setStyleSheet(QStringLiteral("color: rgb(255, 255, 255);"));
        labelTesto->setText(QStringLiteral("TextLabel"));
        labelTesto->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        labelTesto->setMargin(10);
        labelTesto->setTextInteractionFlags(Qt::NoTextInteraction);

        horizontalLayout->addWidget(labelTesto);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);


        retranslateUi(ufgCheckBox);

        QMetaObject::connectSlotsByName(ufgCheckBox);
    } // setupUi

    void retranslateUi(QWidget *ufgCheckBox)
    {
        Q_UNUSED(ufgCheckBox);
    } // retranslateUi

};

namespace Ui {
    class ufgCheckBox: public Ui_ufgCheckBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UFGCHECKBOX_H
