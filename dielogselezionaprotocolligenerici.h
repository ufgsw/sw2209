#ifndef DIELOGSELEZIONAPROTOCOLLIGENERICI_H
#define DIELOGSELEZIONAPROTOCOLLIGENERICI_H

#include <QDialog>


namespace Ui {
class dielogSelezionaProtocolliGenerici;
}

class dielogSelezionaProtocolliGenerici : public QDialog
{
    Q_OBJECT

public:
    explicit dielogSelezionaProtocolliGenerici(QWidget *parent = 0);
    ~dielogSelezionaProtocolliGenerici();

private:
    Ui::dielogSelezionaProtocolliGenerici *ui;
};

#endif // DIELOGSELEZIONAPROTOCOLLIGENERICI_H
