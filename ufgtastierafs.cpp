#include "ufgtastierafs.h"
#include "ui_ufgtastierafs.h"

#include <QPainter>


ufgTastieraFs::ufgTastieraFs(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ufgTastieraFs)
{
    ui->setupUi(this);
    //this->setModal(true);

    QColor bga1(100,100,100);
    QColor bga2(150,150,150);
    QColor bda(255,255,255);
    QColor fca(255,0,0);

    QColor bgb1(250,250,250);
    QColor bgb2(200,200,200);
    QColor bdb(255,255,255);
    QColor fcb(255,0,0);

    setFN( T_DEFAULT );

    ui->tastoA1->setColors( bga1,bga2,bda,fca );
    ui->tastoA2->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA3->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA4->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA5->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA6->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA7->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA8->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA9->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA10->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA11->setColors( bgb1,bgb2,bdb,fcb );
    ui->tastoA12->setColors( bga1,bga2,bda,fca );

    ui->tastoB1->setColors(bga1,bga2,bda,fca);
    ui->tastoB2->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoB3->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoB4->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoB5->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoB6->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoB7->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoB8->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoB9->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoB10->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoB11->setColors(bgb1,bgb2,bdb,fcb);

    ui->tastoC1->setColors(bga1,bga2,bda,fca);
    ui->tastoC2->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoC3->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoC4->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoC5->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoC6->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoC7->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoC8->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoC9->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoC10->setColors(bgb1,bgb2,bdb,fcb);

    ui->tastoEN->setColors(bga1,bga2,bda,fca);

    ui->tastoD1->setColors(bga1,bga2,bda,fca);
    ui->tastoD2->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoD3->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoD4->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoD5->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoD6->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoD7->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoD8->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoD9->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoD10->setColors(bgb1,bgb2,bdb,fcb);
    ui->tastoD11->setColors(bgb1,bgb2,bdb,fcb);

    ui->tastoE1->setColors(bga1,bga2,bda,fca);
    ui->tastoE2->setColors(bga1,bga2,bda,fca);
    ui->tastoE3->setColors(bga1,bga2,bda,fca);
    ui->tastoE4->setColors(bga1,bga2,bda,fca);
    ui->tastoE5->setColors(bga1,bga2,bda,fca);
    ui->tastoE6->setColors(bga1,bga2,bda,fca);
    ui->tastoE7->setColors(bga1,bga2,bda,fca);


    connect(ui->tastoA1,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA2,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA3,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA4,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA5,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA6,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA7,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA8,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA9,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA10,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA11,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoA12,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));

    connect(ui->tastoB1,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB2,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB3,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB4,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB5,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB6,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB7,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB8,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB9,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB10,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoB11,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));

    connect(ui->tastoC1,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoC2,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoC3,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoC4,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoC5,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoC6,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoC7,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoC8,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoC9,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoC10,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));

    connect(ui->tastoD1,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD2,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD3,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD4,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD5,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD6,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD7,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD8,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD9,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD10,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoD11,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));

    connect(ui->tastoE1,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoE2,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoE3,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoE4,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoE5,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoE6,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));
    connect(ui->tastoE7,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));

    connect(ui->tastoEN,SIGNAL(buttonClicked(ufgButtonTastiera*)),this,SLOT(tastoPremuto(ufgButtonTastiera*)));

    ui->tastoC1->setChecked(true);
    ui->tastoE1->setChecked(true);

//    timer = new QTimer(this);
//    connect(timer, SIGNAL(timeout()), this, SLOT(animazione()));

//    visibile = false;

}

ufgTastieraFs::~ufgTastieraFs()
{
    qDebug("tastieraFS deallocata");
    delete ui;
}

void ufgTastieraFs::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QRect area = this->rect();

    area.setX(2);
    area.setY(2);
    area.setWidth( area.width()-2 );
    area.setHeight( area.height()-2 );

    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(2);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);

    painter.setPen(pen);
    painter.fillRect(area,Qt::gray);
    painter.drawRect(area);

}

void ufgTastieraFs::defaultText(QString testo)
{
    this->ui->lineEdit->setText(testo);
}

void ufgTastieraFs::setFN(tipoTastiera _fn)
{
    oldTipo = tipo;
    tipo    = _fn;
    switch(tipo)
    {
    case T_DEFAULT:
        ui->tastoA1->setTasto("<",TASTO_CURSORE_SX);
        ui->tastoA2->setTasto("1",TASTO_NUMERICO);
        ui->tastoA3->setTasto("2",TASTO_NUMERICO);
        ui->tastoA4->setTasto("3",TASTO_NUMERICO);
        ui->tastoA5->setTasto("4",TASTO_NUMERICO);
        ui->tastoA6->setTasto("5",TASTO_NUMERICO );
        ui->tastoA7->setTasto("6", TASTO_NUMERICO);
        ui->tastoA8->setTasto("7",TASTO_NUMERICO);
        ui->tastoA9->setTasto("8", TASTO_NUMERICO);
        ui->tastoA10->setTasto("9", TASTO_NUMERICO);
        ui->tastoA11->setTasto("0", TASTO_NUMERICO);
        ui->tastoA12->setTasto(">", TASTO_CURSORE_DX);

        ui->tastoB1->setTasto("un", TASTO_UNDO);
        ui->tastoB2->setTasto("q",TASTO_ALFANUMERICO);
        ui->tastoB3->setTasto("w",TASTO_ALFANUMERICO);
        ui->tastoB4->setTasto("e",TASTO_ALFANUMERICO);
        ui->tastoB5->setTasto("r",TASTO_ALFANUMERICO);
        ui->tastoB6->setTasto("t",TASTO_ALFANUMERICO);
        ui->tastoB7->setTasto("y",TASTO_ALFANUMERICO);
        ui->tastoB8->setTasto("u",TASTO_ALFANUMERICO);
        ui->tastoB9->setTasto("i",TASTO_ALFANUMERICO);
        ui->tastoB10->setTasto("o",TASTO_ALFANUMERICO);
        ui->tastoB11->setTasto("p",TASTO_ALFANUMERICO);

        ui->tastoC1->setTasto("caps",TASTO_CAPS);
        ui->tastoC2->setTasto("a",TASTO_ALFANUMERICO);
        ui->tastoC3->setTasto("s",TASTO_ALFANUMERICO);
        ui->tastoC4->setTasto("d",TASTO_ALFANUMERICO);
        ui->tastoC5->setTasto("f",TASTO_ALFANUMERICO);
        ui->tastoC6->setTasto("g",TASTO_ALFANUMERICO);
        ui->tastoC7->setTasto("h",TASTO_ALFANUMERICO);
        ui->tastoC8->setTasto("j",TASTO_ALFANUMERICO);
        ui->tastoC9->setTasto("k",TASTO_ALFANUMERICO);
        ui->tastoC10->setTasto("l",TASTO_ALFANUMERICO);

        ui->tastoEN->setTasto("ENT",TASTO_ENTER);

        ui->tastoD1->setTasto("^",TASTO_SHIFT);
        ui->tastoD2->setTasto("+",TASTO_ALFANUMERICO);
        ui->tastoD3->setTasto("z",TASTO_ALFANUMERICO);
        ui->tastoD4->setTasto("x",TASTO_ALFANUMERICO);
        ui->tastoD5->setTasto("c",TASTO_ALFANUMERICO);
        ui->tastoD6->setTasto("v",TASTO_ALFANUMERICO);
        ui->tastoD7->setTasto("b",TASTO_ALFANUMERICO);
        ui->tastoD8->setTasto("n",TASTO_ALFANUMERICO);
        ui->tastoD9->setTasto("m",TASTO_ALFANUMERICO);
        ui->tastoD10->setTasto("@",TASTO_ALFANUMERICO);
        ui->tastoD11->setTasto("#",TASTO_ALFANUMERICO);

        ui->tastoE1->setTasto("FN",TASTO_FN);
        ui->tastoE2->setTasto("BACK",TASTO_BACK);
        ui->tastoE3->setTasto("DEL",TASTO_DEL);
        ui->tastoE4->setTasto("space",TASTO_SPACE);
        ui->tastoE5->setTasto(".",TASTO_ALFANUMERICO);
        ui->tastoE6->setTasto(",",TASTO_ALFANUMERICO);
        ui->tastoE7->setTasto("CLR",TASTO_CLEAR);
        break;
    case T_MAIUSCOLE:
        ui->tastoB1->setTasto("un", TASTO_UNDO);
        ui->tastoB2->setTasto("Q",TASTO_ALFANUMERICO);
        ui->tastoB3->setTasto("W",TASTO_ALFANUMERICO);
        ui->tastoB4->setTasto("E",TASTO_ALFANUMERICO);
        ui->tastoB5->setTasto("R",TASTO_ALFANUMERICO);
        ui->tastoB6->setTasto("T",TASTO_ALFANUMERICO);
        ui->tastoB7->setTasto("Y",TASTO_ALFANUMERICO);
        ui->tastoB8->setTasto("U",TASTO_ALFANUMERICO);
        ui->tastoB9->setTasto("I",TASTO_ALFANUMERICO);
        ui->tastoB10->setTasto("O",TASTO_ALFANUMERICO);
        ui->tastoB11->setTasto("P",TASTO_ALFANUMERICO);

        ui->tastoC1->setTasto("caps",TASTO_CAPS);
        ui->tastoC2->setTasto("A",TASTO_ALFANUMERICO);
        ui->tastoC3->setTasto("S",TASTO_ALFANUMERICO);
        ui->tastoC4->setTasto("D",TASTO_ALFANUMERICO);
        ui->tastoC5->setTasto("F",TASTO_ALFANUMERICO);
        ui->tastoC6->setTasto("G",TASTO_ALFANUMERICO);
        ui->tastoC7->setTasto("H",TASTO_ALFANUMERICO);
        ui->tastoC8->setTasto("J",TASTO_ALFANUMERICO);
        ui->tastoC9->setTasto("K",TASTO_ALFANUMERICO);
        ui->tastoC10->setTasto("L",TASTO_ALFANUMERICO);

        ui->tastoEN->setTasto("ENT",TASTO_ENTER );

        ui->tastoD1->setTasto("^",TASTO_SHIFT);
        ui->tastoD2->setTasto("+",TASTO_ALFANUMERICO);
        ui->tastoD3->setTasto("Z",TASTO_ALFANUMERICO);
        ui->tastoD4->setTasto("X",TASTO_ALFANUMERICO);
        ui->tastoD5->setTasto("C",TASTO_ALFANUMERICO);
        ui->tastoD6->setTasto("V",TASTO_ALFANUMERICO);
        ui->tastoD7->setTasto("B",TASTO_ALFANUMERICO);
        ui->tastoD8->setTasto("N",TASTO_ALFANUMERICO);
        ui->tastoD9->setTasto("M",TASTO_ALFANUMERICO);
        ui->tastoD10->setTasto("@",TASTO_ALFANUMERICO);
        ui->tastoD11->setTasto("#",TASTO_ALFANUMERICO);

        break;

    case T_MINUSCOLE:

        ui->tastoB1->setTasto("un", TASTO_UNDO);
        ui->tastoB2->setTasto("q",TASTO_ALFANUMERICO);
        ui->tastoB3->setTasto("w",TASTO_ALFANUMERICO);
        ui->tastoB4->setTasto("e",TASTO_ALFANUMERICO);
        ui->tastoB5->setTasto("r",TASTO_ALFANUMERICO);
        ui->tastoB6->setTasto("t",TASTO_ALFANUMERICO);
        ui->tastoB7->setTasto("y",TASTO_ALFANUMERICO);
        ui->tastoB8->setTasto("u",TASTO_ALFANUMERICO);
        ui->tastoB9->setTasto("i",TASTO_ALFANUMERICO);
        ui->tastoB10->setTasto("o",TASTO_ALFANUMERICO);
        ui->tastoB11->setTasto("p",TASTO_ALFANUMERICO);

        ui->tastoC1->setTasto("caps",TASTO_CAPS);
        ui->tastoC2->setTasto("a",TASTO_ALFANUMERICO);
        ui->tastoC3->setTasto("s",TASTO_ALFANUMERICO);
        ui->tastoC4->setTasto("d",TASTO_ALFANUMERICO);
        ui->tastoC5->setTasto("f",TASTO_ALFANUMERICO);
        ui->tastoC6->setTasto("g",TASTO_ALFANUMERICO);
        ui->tastoC7->setTasto("h",TASTO_ALFANUMERICO);
        ui->tastoC8->setTasto("j",TASTO_ALFANUMERICO);
        ui->tastoC9->setTasto("k",TASTO_ALFANUMERICO);
        ui->tastoC10->setTasto("l",TASTO_ALFANUMERICO);

        ui->tastoEN->setTasto("ENT",TASTO_ENTER);

        ui->tastoD1->setTasto("^",TASTO_SHIFT);
        ui->tastoD2->setTasto("+",TASTO_ALFANUMERICO);
        ui->tastoD3->setTasto("z",TASTO_ALFANUMERICO);
        ui->tastoD4->setTasto("x",TASTO_ALFANUMERICO);
        ui->tastoD5->setTasto("c",TASTO_ALFANUMERICO);
        ui->tastoD6->setTasto("v",TASTO_ALFANUMERICO);
        ui->tastoD7->setTasto("b",TASTO_ALFANUMERICO);
        ui->tastoD8->setTasto("n",TASTO_ALFANUMERICO);
        ui->tastoD9->setTasto("m",TASTO_ALFANUMERICO);
        ui->tastoD10->setTasto("@",TASTO_ALFANUMERICO);
        ui->tastoD11->setTasto("#",TASTO_ALFANUMERICO);

        break;
    case T_ALT1:
        ui->tastoA1->setTasto("<",  TASTO_CURSORE_SX);
        ui->tastoA2->setTasto("!",  TASTO_ALFANUMERICO);
        ui->tastoA3->setTasto("\"", TASTO_ALFANUMERICO);
        ui->tastoA4->setTasto("£",  TASTO_ALFANUMERICO);
        ui->tastoA5->setTasto("$",  TASTO_ALFANUMERICO);
        ui->tastoA6->setTasto("%",  TASTO_ALFANUMERICO );
        ui->tastoA7->setTasto("&",  TASTO_ALFANUMERICO);
        ui->tastoA8->setTasto("/",  TASTO_ALFANUMERICO);
        ui->tastoA9->setTasto("(",  TASTO_ALFANUMERICO);
        ui->tastoA10->setTasto(")", TASTO_ALFANUMERICO);
        ui->tastoA11->setTasto("=", TASTO_ALFANUMERICO);
        ui->tastoA12->setTasto(">", TASTO_CURSORE_DX);
        break;

    case T_ALT2:
        ui->tastoA1->setTasto("<",TASTO_CURSORE_SX);
        ui->tastoA2->setTasto("1",TASTO_NUMERICO);
        ui->tastoA3->setTasto("2",TASTO_NUMERICO);
        ui->tastoA4->setTasto("3",TASTO_NUMERICO);
        ui->tastoA5->setTasto("4",TASTO_NUMERICO);
        ui->tastoA6->setTasto("5",TASTO_NUMERICO );
        ui->tastoA7->setTasto("6", TASTO_NUMERICO);
        ui->tastoA8->setTasto("7",TASTO_NUMERICO);
        ui->tastoA9->setTasto("8", TASTO_NUMERICO);
        ui->tastoA10->setTasto("9", TASTO_NUMERICO);
        ui->tastoA11->setTasto("0", TASTO_NUMERICO);
        ui->tastoA12->setTasto(">", TASTO_CURSORE_DX);

        break;
    }


}


void ufgTastieraFs::tastoPremuto(ufgButtonTastiera* tasto)
{
    QString copiaTesto;
    int     posizioneCursore;


    copiaTesto       = ui->lineEdit->text();
    posizioneCursore = ui->lineEdit->cursorPosition();

    switch (tasto->getTipo()) {
    case TASTO_NUMERICO:
        copiaTesto.insert(posizioneCursore,tasto->getText());
        ui->lineEdit->setText(copiaTesto);
        ui->lineEdit->setCursorPosition(posizioneCursore+1);
        break;

    case TASTO_ALFANUMERICO:
        copiaTesto.insert(posizioneCursore,tasto->getText());
        ui->lineEdit->setText(copiaTesto);
        ui->lineEdit->setCursorPosition(posizioneCursore+1);
        if( shift == true )
        {
            if( caps == true ) setFN(T_MAIUSCOLE);
            else               setFN(T_MINUSCOLE);
            shift = false;
        }

        break;
    case TASTO_SPACE:
        copiaTesto.insert(posizioneCursore," ");
        ui->lineEdit->setText(copiaTesto);
        ui->lineEdit->setCursorPosition(posizioneCursore+1);
        break;

    case TASTO_CAPS:
        if( tasto->isChecked() == true )
        {
            caps = true;
            this->setFN(T_MAIUSCOLE);
        }
        else
        {
            caps = false;
            this->setFN(T_MINUSCOLE);
        }
        break;

    case TASTO_ENTER:
        //this->showanimation(rectStart,rectEnd);
        emit endEdit( copiaTesto );
        this->close();
        break;

    case TASTO_BACK:
        copiaTesto = copiaTesto.remove(posizioneCursore-1, 1);
        ui->lineEdit->setText(copiaTesto);
        ui->lineEdit->setCursorPosition(posizioneCursore-1);
        break;

    case TASTO_DEL:
        copiaTesto = copiaTesto.remove(posizioneCursore, 1);
        ui->lineEdit->setText(copiaTesto);
        ui->lineEdit->setCursorPosition(posizioneCursore);
        break;

    case TASTO_ESC:
        //this->showanimation(rectStart,rectEnd);
        break;
    case TASTO_CLEAR:
        ui->lineEdit->setText("");
        break;
    case TASTO_SHIFT:
        if( caps == false ) setFN(T_MAIUSCOLE );
        else                setFN(T_MINUSCOLE );
        shift = true;
        break;

    case TASTO_CURSORE_DX:
        ui->lineEdit->setCursorPosition(posizioneCursore+1);
        //ui->lineEdit->update();
        //edit->setSelection(  edit->cursorPosition() ,1);
        break;

    case TASTO_CURSORE_SX:
        ui->lineEdit->setCursorPosition(posizioneCursore-1);
        //this->update();
        //edit->setSelection( edit->cursorPosition() ,-1);

        break;

    case TASTO_FN:
        if( tasto->isChecked() == true )
        {
            this->setFN(T_ALT1);
        }
        else
        {
            this->setFN(T_ALT2);
        }
    break;
    default:
        break;
    }
    ui->lineEdit->setFocus();
}


