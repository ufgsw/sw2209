#include "forminfo.h"
#include "ui_forminfo.h"

//#include "ddmbutton.h"
#include "QPainter"

formInfo::formInfo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::formInfo)
{
    ui->setupUi(this);
}

formInfo::~formInfo()
{
    delete ui;
}

void formInfo::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QRect brect = this->rect();
    brect.setWidth( this->rect().width() - 4  );
    brect.setHeight( this->rect().height() - 4 );
    brect.setLeft(4);
    brect.setTop(4);

    QPen pen;
    pen.setWidth(4);
    pen.setBrush(Qt::blue);

    painter.setPen(pen);
    painter.drawRoundRect( brect ,15,15);
}

void formInfo::setTextInfo(QString testo)
{
    ui->textInfo->setPlainText(testo);
}

void formInfo::setim
