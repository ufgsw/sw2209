<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh" sourcelanguage="it">
<context>
    <name>DialogOrologio</name>
    <message>
        <location filename="dialogorologio.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="12"/>
        <source>Orologio</source>
        <translation>時鐘</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="82"/>
        <source>Ore</source>
        <translation>小時</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="98"/>
        <source>Minuti</source>
        <translation>小時</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="114"/>
        <source>Secondi</source>
        <translation>秒</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="36"/>
        <source>Diatermia</source>
        <translation>透熱</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="42"/>
        <location filename="mainwindow.cpp" line="1828"/>
        <source>MODALITA&apos;</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="43"/>
        <location filename="mainwindow.cpp" line="815"/>
        <location filename="mainwindow.cpp" line="1829"/>
        <source>FREQUENZA</source>
        <translation>频率</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="49"/>
        <location filename="mainwindow.cpp" line="1835"/>
        <source>IMPOSTAZIONI</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>FREQUENZA PEMF</source>
        <translation>SUPER LOW FREQUENCY</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="45"/>
        <location filename="mainwindow.cpp" line="867"/>
        <location filename="mainwindow.cpp" line="1831"/>
        <source>POTENZA</source>
        <translation>功率</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="46"/>
        <location filename="mainwindow.cpp" line="732"/>
        <location filename="mainwindow.cpp" line="1832"/>
        <source>TEMPO</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="47"/>
        <location filename="mainwindow.cpp" line="1833"/>
        <source>GUIDA</source>
        <translation>处方</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <location filename="mainwindow.cpp" line="520"/>
        <location filename="mainwindow.cpp" line="1834"/>
        <source>INFO</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="153"/>
        <location filename="mainwindow.cpp" line="1022"/>
        <location filename="mainwindow.cpp" line="1878"/>
        <source>Versione</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="308"/>
        <location filename="mainwindow.cpp" line="315"/>
        <location filename="mainwindow.cpp" line="322"/>
        <location filename="mainwindow.cpp" line="579"/>
        <location filename="mainwindow.cpp" line="586"/>
        <location filename="mainwindow.cpp" line="593"/>
        <location filename="mainwindow.cpp" line="1839"/>
        <location filename="mainwindow.cpp" line="1844"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <location filename="mainwindow.cpp" line="1854"/>
        <source>CAPACITIVO</source>
        <translation>电容</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="309"/>
        <location filename="mainwindow.cpp" line="330"/>
        <location filename="mainwindow.cpp" line="580"/>
        <location filename="mainwindow.cpp" line="601"/>
        <location filename="mainwindow.cpp" line="1840"/>
        <location filename="mainwindow.cpp" line="1845"/>
        <location filename="mainwindow.cpp" line="1860"/>
        <location filename="mainwindow.cpp" line="1865"/>
        <source>DINAMICO</source>
        <translation>动态</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="329"/>
        <location filename="mainwindow.cpp" line="336"/>
        <location filename="mainwindow.cpp" line="343"/>
        <location filename="mainwindow.cpp" line="600"/>
        <location filename="mainwindow.cpp" line="607"/>
        <location filename="mainwindow.cpp" line="614"/>
        <location filename="mainwindow.cpp" line="1859"/>
        <location filename="mainwindow.cpp" line="1864"/>
        <location filename="mainwindow.cpp" line="1869"/>
        <location filename="mainwindow.cpp" line="1874"/>
        <source>RESISTIVO</source>
        <translation>电阻</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="539"/>
        <location filename="mainwindow.cpp" line="560"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="764"/>
        <source>MODALITA</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="897"/>
        <source>DINAMICI</source>
        <translation>动态</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="898"/>
        <source>STATICI</source>
        <translation>静止</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="972"/>
        <source>CALIBRAZIONE TERMINATA</source>
        <translation>校准已完成</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="990"/>
        <source>CALIBRAZIONE IN CORSO</source>
        <translation>正在校准</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1771"/>
        <source>Verifica contatto sonda</source>
        <translation>檢查探頭接觸</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="521"/>
        <source>desrizione info</source>
        <translation>该医疗设备若使用不当会造成患者损伤，请仔细阅读产品说明书及操作方法</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1600"/>
        <source>descrizione allarme B001</source>
        <translation>报警 內部通訊錯誤給系統</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1613"/>
        <source>descrizione allarme A001</source>
        <translation>报警 通過短路檢測出的系統</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1643"/>
        <source>descrizione allarme A002</source>
        <translation>报警 該系統已檢測到過熱</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1623"/>
        <source>descrizione allarme A003</source>
        <translation>报警 該系統檢測到異常電流消耗</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1638"/>
        <source>descrizione allarme A004</source>
        <translation>报警 該系統檢測到異常電壓</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1618"/>
        <location filename="mainwindow.cpp" line="1628"/>
        <location filename="mainwindow.cpp" line="1633"/>
        <location filename="mainwindow.cpp" line="1648"/>
        <location filename="mainwindow.cpp" line="1653"/>
        <location filename="mainwindow.cpp" line="1685"/>
        <source>descrizione allarme B002</source>
        <oldsource>descrizione allarme trans</oldsource>
        <translation>报警 通用硬件錯誤</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="44"/>
        <location filename="mainwindow.cpp" line="1830"/>
        <source>FREQUENZA LOW</source>
        <translation>调制频率</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <location filename="mainwindow.cpp" line="337"/>
        <location filename="mainwindow.cpp" line="587"/>
        <location filename="mainwindow.cpp" line="608"/>
        <location filename="mainwindow.cpp" line="1855"/>
        <location filename="mainwindow.cpp" line="1870"/>
        <source>STATICO GRANDE</source>
        <translation>大静态</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="323"/>
        <location filename="mainwindow.cpp" line="344"/>
        <location filename="mainwindow.cpp" line="594"/>
        <location filename="mainwindow.cpp" line="615"/>
        <location filename="mainwindow.cpp" line="1850"/>
        <location filename="mainwindow.cpp" line="1875"/>
        <source>STATICO PICCOLO</source>
        <translation>小静态</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1658"/>
        <source>descrizione allarme A005</source>
        <translation>报警 該系統檢測到異常電流的患者</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1675"/>
        <source>descrizione allarme A006</source>
        <translation>报警 該系統檢測到異常操作頻率</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1680"/>
        <location filename="mainwindow.cpp" line="1745"/>
        <source>descrizione warning elettrodo massa</source>
        <translation>注意 接地電極斷開</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1765"/>
        <source>descrizione allarme emergenza</source>
        <translation>檢查患者的緊急按鈕</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1665"/>
        <location filename="mainwindow.cpp" line="1697"/>
        <location filename="mainwindow.cpp" line="1753"/>
        <source>descrizione warning capacitivo</source>
        <translation>注意 電容電極斷開</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1669"/>
        <location filename="mainwindow.cpp" line="1702"/>
        <location filename="mainwindow.cpp" line="1758"/>
        <source>descrizione warning resistivo</source>
        <translation>注意 電阻電極斷開</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1690"/>
        <source>descrizione warning mano scollegata</source>
        <translation>注意 緊急按鈕當前離線</translation>
    </message>
</context>
<context>
    <name>dialogElencoProtocolli</name>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="97"/>
        <source>PROTOCOLLI GENERICI</source>
        <translation>常规处方</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="99"/>
        <source>FASE ACUTA</source>
        <translation>急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="100"/>
        <source>FASE CRONICA</source>
        <translation>恢复期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="101"/>
        <source>RIDUZIONE DEL DOLORE</source>
        <translation>镇痛</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="102"/>
        <source>CONTRATTURA</source>
        <translation>痉挛控制</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="103"/>
        <source>LINFODRENAGGIO</source>
        <translation>淋巴引流</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="104"/>
        <source>RADICOLOPATIA FASE ACUTA</source>
        <translation>急性神经根压迫</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="105"/>
        <source>BLOCCO ARTICOLARE / RIGIDITA</source>
        <translation>关节僵硬</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="126"/>
        <source>PROTOCOLLI PER PATOLOGIA</source>
        <translation>常见病症处方</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="192"/>
        <source>PROTOCOLLI NEUROLOGICI</source>
        <translation>神经损伤处方</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="200"/>
        <source>MIELO LESIONE PARZIALE</source>
        <translation>不完全神经损伤</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="201"/>
        <source>TRATTAMENTO POST ICTUS</source>
        <translation>脑卒中</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="202"/>
        <source>LESIONI NERVOSE PERIFERICHE ( ERNIE DISCALI )</source>
        <translation>周围神经损伤（椎间盘突出）</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="203"/>
        <source>LESIONI NERVOSE PERIFERICHE (PLESSO BRACHIALE )</source>
        <translation>周围神经损伤（臂丛）</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="230"/>
        <location filename="dialogelencoprotocolli.cpp" line="244"/>
        <location filename="dialogelencoprotocolli.cpp" line="258"/>
        <location filename="dialogelencoprotocolli.cpp" line="272"/>
        <location filename="dialogelencoprotocolli.cpp" line="322"/>
        <location filename="dialogelencoprotocolli.cpp" line="452"/>
        <location filename="dialogelencoprotocolli.cpp" line="466"/>
        <location filename="dialogelencoprotocolli.cpp" line="480"/>
        <location filename="dialogelencoprotocolli.cpp" line="494"/>
        <location filename="dialogelencoprotocolli.cpp" line="662"/>
        <location filename="dialogelencoprotocolli.cpp" line="676"/>
        <location filename="dialogelencoprotocolli.cpp" line="690"/>
        <location filename="dialogelencoprotocolli.cpp" line="704"/>
        <location filename="dialogelencoprotocolli.cpp" line="755"/>
        <location filename="dialogelencoprotocolli.cpp" line="856"/>
        <location filename="dialogelencoprotocolli.cpp" line="870"/>
        <location filename="dialogelencoprotocolli.cpp" line="884"/>
        <location filename="dialogelencoprotocolli.cpp" line="1052"/>
        <location filename="dialogelencoprotocolli.cpp" line="1066"/>
        <location filename="dialogelencoprotocolli.cpp" line="1221"/>
        <location filename="dialogelencoprotocolli.cpp" line="1369"/>
        <location filename="dialogelencoprotocolli.cpp" line="1383"/>
        <location filename="dialogelencoprotocolli.cpp" line="1397"/>
        <location filename="dialogelencoprotocolli.cpp" line="1411"/>
        <location filename="dialogelencoprotocolli.cpp" line="1472"/>
        <location filename="dialogelencoprotocolli.cpp" line="1956"/>
        <location filename="dialogelencoprotocolli.cpp" line="1984"/>
        <location filename="dialogelencoprotocolli.cpp" line="2035"/>
        <location filename="dialogelencoprotocolli.cpp" line="2049"/>
        <source>prono</source>
        <translation>俯卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="231"/>
        <location filename="dialogelencoprotocolli.cpp" line="245"/>
        <location filename="dialogelencoprotocolli.cpp" line="259"/>
        <location filename="dialogelencoprotocolli.cpp" line="273"/>
        <location filename="dialogelencoprotocolli.cpp" line="453"/>
        <location filename="dialogelencoprotocolli.cpp" line="467"/>
        <location filename="dialogelencoprotocolli.cpp" line="481"/>
        <location filename="dialogelencoprotocolli.cpp" line="495"/>
        <location filename="dialogelencoprotocolli.cpp" line="517"/>
        <location filename="dialogelencoprotocolli.cpp" line="531"/>
        <location filename="dialogelencoprotocolli.cpp" line="663"/>
        <location filename="dialogelencoprotocolli.cpp" line="677"/>
        <location filename="dialogelencoprotocolli.cpp" line="691"/>
        <location filename="dialogelencoprotocolli.cpp" line="705"/>
        <location filename="dialogelencoprotocolli.cpp" line="857"/>
        <location filename="dialogelencoprotocolli.cpp" line="871"/>
        <location filename="dialogelencoprotocolli.cpp" line="885"/>
        <location filename="dialogelencoprotocolli.cpp" line="1053"/>
        <location filename="dialogelencoprotocolli.cpp" line="1067"/>
        <location filename="dialogelencoprotocolli.cpp" line="1222"/>
        <location filename="dialogelencoprotocolli.cpp" line="1370"/>
        <location filename="dialogelencoprotocolli.cpp" line="1384"/>
        <location filename="dialogelencoprotocolli.cpp" line="1398"/>
        <location filename="dialogelencoprotocolli.cpp" line="1412"/>
        <location filename="dialogelencoprotocolli.cpp" line="1435"/>
        <location filename="dialogelencoprotocolli.cpp" line="1449"/>
        <location filename="dialogelencoprotocolli.cpp" line="1523"/>
        <location filename="dialogelencoprotocolli.cpp" line="1537"/>
        <location filename="dialogelencoprotocolli.cpp" line="1906"/>
        <source>addome</source>
        <translation>腹部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="232"/>
        <location filename="dialogelencoprotocolli.cpp" line="246"/>
        <location filename="dialogelencoprotocolli.cpp" line="260"/>
        <location filename="dialogelencoprotocolli.cpp" line="274"/>
        <location filename="dialogelencoprotocolli.cpp" line="454"/>
        <location filename="dialogelencoprotocolli.cpp" line="468"/>
        <location filename="dialogelencoprotocolli.cpp" line="482"/>
        <location filename="dialogelencoprotocolli.cpp" line="496"/>
        <location filename="dialogelencoprotocolli.cpp" line="664"/>
        <location filename="dialogelencoprotocolli.cpp" line="678"/>
        <location filename="dialogelencoprotocolli.cpp" line="692"/>
        <location filename="dialogelencoprotocolli.cpp" line="706"/>
        <location filename="dialogelencoprotocolli.cpp" line="858"/>
        <location filename="dialogelencoprotocolli.cpp" line="872"/>
        <location filename="dialogelencoprotocolli.cpp" line="886"/>
        <location filename="dialogelencoprotocolli.cpp" line="1054"/>
        <location filename="dialogelencoprotocolli.cpp" line="1068"/>
        <location filename="dialogelencoprotocolli.cpp" line="1223"/>
        <location filename="dialogelencoprotocolli.cpp" line="1371"/>
        <location filename="dialogelencoprotocolli.cpp" line="1385"/>
        <location filename="dialogelencoprotocolli.cpp" line="1399"/>
        <location filename="dialogelencoprotocolli.cpp" line="1413"/>
        <source>zona dolente</source>
        <translation>疼痛部位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="233"/>
        <location filename="dialogelencoprotocolli.cpp" line="247"/>
        <location filename="dialogelencoprotocolli.cpp" line="311"/>
        <location filename="dialogelencoprotocolli.cpp" line="325"/>
        <location filename="dialogelencoprotocolli.cpp" line="339"/>
        <location filename="dialogelencoprotocolli.cpp" line="455"/>
        <location filename="dialogelencoprotocolli.cpp" line="469"/>
        <location filename="dialogelencoprotocolli.cpp" line="519"/>
        <location filename="dialogelencoprotocolli.cpp" line="533"/>
        <location filename="dialogelencoprotocolli.cpp" line="621"/>
        <location filename="dialogelencoprotocolli.cpp" line="665"/>
        <location filename="dialogelencoprotocolli.cpp" line="679"/>
        <location filename="dialogelencoprotocolli.cpp" line="730"/>
        <location filename="dialogelencoprotocolli.cpp" line="744"/>
        <location filename="dialogelencoprotocolli.cpp" line="758"/>
        <location filename="dialogelencoprotocolli.cpp" line="772"/>
        <location filename="dialogelencoprotocolli.cpp" line="859"/>
        <location filename="dialogelencoprotocolli.cpp" line="873"/>
        <location filename="dialogelencoprotocolli.cpp" line="924"/>
        <location filename="dialogelencoprotocolli.cpp" line="952"/>
        <location filename="dialogelencoprotocolli.cpp" line="1055"/>
        <location filename="dialogelencoprotocolli.cpp" line="1092"/>
        <location filename="dialogelencoprotocolli.cpp" line="1106"/>
        <location filename="dialogelencoprotocolli.cpp" line="1120"/>
        <location filename="dialogelencoprotocolli.cpp" line="1134"/>
        <location filename="dialogelencoprotocolli.cpp" line="1180"/>
        <location filename="dialogelencoprotocolli.cpp" line="1261"/>
        <location filename="dialogelencoprotocolli.cpp" line="1289"/>
        <location filename="dialogelencoprotocolli.cpp" line="1311"/>
        <location filename="dialogelencoprotocolli.cpp" line="1372"/>
        <location filename="dialogelencoprotocolli.cpp" line="1386"/>
        <location filename="dialogelencoprotocolli.cpp" line="1400"/>
        <location filename="dialogelencoprotocolli.cpp" line="1437"/>
        <location filename="dialogelencoprotocolli.cpp" line="1451"/>
        <location filename="dialogelencoprotocolli.cpp" line="1475"/>
        <location filename="dialogelencoprotocolli.cpp" line="1525"/>
        <location filename="dialogelencoprotocolli.cpp" line="1539"/>
        <location filename="dialogelencoprotocolli.cpp" line="1614"/>
        <location filename="dialogelencoprotocolli.cpp" line="1628"/>
        <location filename="dialogelencoprotocolli.cpp" line="1693"/>
        <location filename="dialogelencoprotocolli.cpp" line="1707"/>
        <location filename="dialogelencoprotocolli.cpp" line="1735"/>
        <location filename="dialogelencoprotocolli.cpp" line="1787"/>
        <location filename="dialogelencoprotocolli.cpp" line="1801"/>
        <location filename="dialogelencoprotocolli.cpp" line="1829"/>
        <location filename="dialogelencoprotocolli.cpp" line="1880"/>
        <location filename="dialogelencoprotocolli.cpp" line="1894"/>
        <location filename="dialogelencoprotocolli.cpp" line="1908"/>
        <location filename="dialogelencoprotocolli.cpp" line="1959"/>
        <location filename="dialogelencoprotocolli.cpp" line="1973"/>
        <location filename="dialogelencoprotocolli.cpp" line="1987"/>
        <location filename="dialogelencoprotocolli.cpp" line="2038"/>
        <source>DINAMICO 60</source>
        <translation>动态电极</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="238"/>
        <location filename="dialogelencoprotocolli.cpp" line="266"/>
        <location filename="dialogelencoprotocolli.cpp" line="302"/>
        <location filename="dialogelencoprotocolli.cpp" line="330"/>
        <location filename="dialogelencoprotocolli.cpp" line="390"/>
        <location filename="dialogelencoprotocolli.cpp" line="460"/>
        <location filename="dialogelencoprotocolli.cpp" line="488"/>
        <location filename="dialogelencoprotocolli.cpp" line="524"/>
        <location filename="dialogelencoprotocolli.cpp" line="560"/>
        <location filename="dialogelencoprotocolli.cpp" line="584"/>
        <location filename="dialogelencoprotocolli.cpp" line="598"/>
        <location filename="dialogelencoprotocolli.cpp" line="612"/>
        <location filename="dialogelencoprotocolli.cpp" line="670"/>
        <location filename="dialogelencoprotocolli.cpp" line="698"/>
        <location filename="dialogelencoprotocolli.cpp" line="735"/>
        <location filename="dialogelencoprotocolli.cpp" line="763"/>
        <location filename="dialogelencoprotocolli.cpp" line="822"/>
        <location filename="dialogelencoprotocolli.cpp" line="878"/>
        <location filename="dialogelencoprotocolli.cpp" line="915"/>
        <location filename="dialogelencoprotocolli.cpp" line="943"/>
        <location filename="dialogelencoprotocolli.cpp" line="980"/>
        <location filename="dialogelencoprotocolli.cpp" line="1017"/>
        <location filename="dialogelencoprotocolli.cpp" line="1097"/>
        <location filename="dialogelencoprotocolli.cpp" line="1125"/>
        <location filename="dialogelencoprotocolli.cpp" line="1229"/>
        <location filename="dialogelencoprotocolli.cpp" line="1252"/>
        <location filename="dialogelencoprotocolli.cpp" line="1280"/>
        <location filename="dialogelencoprotocolli.cpp" line="1316"/>
        <location filename="dialogelencoprotocolli.cpp" line="1377"/>
        <location filename="dialogelencoprotocolli.cpp" line="1391"/>
        <location filename="dialogelencoprotocolli.cpp" line="1405"/>
        <location filename="dialogelencoprotocolli.cpp" line="1419"/>
        <location filename="dialogelencoprotocolli.cpp" line="1456"/>
        <location filename="dialogelencoprotocolli.cpp" line="1544"/>
        <location filename="dialogelencoprotocolli.cpp" line="1568"/>
        <location filename="dialogelencoprotocolli.cpp" line="1619"/>
        <location filename="dialogelencoprotocolli.cpp" line="1633"/>
        <location filename="dialogelencoprotocolli.cpp" line="1647"/>
        <location filename="dialogelencoprotocolli.cpp" line="1698"/>
        <location filename="dialogelencoprotocolli.cpp" line="1726"/>
        <location filename="dialogelencoprotocolli.cpp" line="1792"/>
        <location filename="dialogelencoprotocolli.cpp" line="1820"/>
        <location filename="dialogelencoprotocolli.cpp" line="1885"/>
        <location filename="dialogelencoprotocolli.cpp" line="1913"/>
        <location filename="dialogelencoprotocolli.cpp" line="1992"/>
        <location filename="dialogelencoprotocolli.cpp" line="2057"/>
        <source>RESISTIVO</source>
        <translation>阻</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="252"/>
        <location filename="dialogelencoprotocolli.cpp" line="280"/>
        <location filename="dialogelencoprotocolli.cpp" line="316"/>
        <location filename="dialogelencoprotocolli.cpp" line="344"/>
        <location filename="dialogelencoprotocolli.cpp" line="366"/>
        <location filename="dialogelencoprotocolli.cpp" line="404"/>
        <location filename="dialogelencoprotocolli.cpp" line="418"/>
        <location filename="dialogelencoprotocolli.cpp" line="474"/>
        <location filename="dialogelencoprotocolli.cpp" line="502"/>
        <location filename="dialogelencoprotocolli.cpp" line="538"/>
        <location filename="dialogelencoprotocolli.cpp" line="626"/>
        <location filename="dialogelencoprotocolli.cpp" line="684"/>
        <location filename="dialogelencoprotocolli.cpp" line="712"/>
        <location filename="dialogelencoprotocolli.cpp" line="749"/>
        <location filename="dialogelencoprotocolli.cpp" line="777"/>
        <location filename="dialogelencoprotocolli.cpp" line="799"/>
        <location filename="dialogelencoprotocolli.cpp" line="864"/>
        <location filename="dialogelencoprotocolli.cpp" line="892"/>
        <location filename="dialogelencoprotocolli.cpp" line="929"/>
        <location filename="dialogelencoprotocolli.cpp" line="957"/>
        <location filename="dialogelencoprotocolli.cpp" line="994"/>
        <location filename="dialogelencoprotocolli.cpp" line="1060"/>
        <location filename="dialogelencoprotocolli.cpp" line="1074"/>
        <location filename="dialogelencoprotocolli.cpp" line="1111"/>
        <location filename="dialogelencoprotocolli.cpp" line="1139"/>
        <location filename="dialogelencoprotocolli.cpp" line="1161"/>
        <location filename="dialogelencoprotocolli.cpp" line="1185"/>
        <location filename="dialogelencoprotocolli.cpp" line="1266"/>
        <location filename="dialogelencoprotocolli.cpp" line="1294"/>
        <location filename="dialogelencoprotocolli.cpp" line="1330"/>
        <location filename="dialogelencoprotocolli.cpp" line="1442"/>
        <location filename="dialogelencoprotocolli.cpp" line="1480"/>
        <location filename="dialogelencoprotocolli.cpp" line="1530"/>
        <location filename="dialogelencoprotocolli.cpp" line="1712"/>
        <location filename="dialogelencoprotocolli.cpp" line="1740"/>
        <location filename="dialogelencoprotocolli.cpp" line="1806"/>
        <location filename="dialogelencoprotocolli.cpp" line="1834"/>
        <location filename="dialogelencoprotocolli.cpp" line="1899"/>
        <location filename="dialogelencoprotocolli.cpp" line="1964"/>
        <location filename="dialogelencoprotocolli.cpp" line="1978"/>
        <location filename="dialogelencoprotocolli.cpp" line="2043"/>
        <source>CAPACITIVO</source>
        <translation>电容</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="275"/>
        <source>STATICO</source>
        <translation>静态电极</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="294"/>
        <location filename="dialogelencoprotocolli.cpp" line="308"/>
        <location filename="dialogelencoprotocolli.cpp" line="336"/>
        <location filename="dialogelencoprotocolli.cpp" line="358"/>
        <location filename="dialogelencoprotocolli.cpp" line="382"/>
        <location filename="dialogelencoprotocolli.cpp" line="396"/>
        <location filename="dialogelencoprotocolli.cpp" line="410"/>
        <location filename="dialogelencoprotocolli.cpp" line="552"/>
        <location filename="dialogelencoprotocolli.cpp" line="576"/>
        <location filename="dialogelencoprotocolli.cpp" line="590"/>
        <location filename="dialogelencoprotocolli.cpp" line="604"/>
        <location filename="dialogelencoprotocolli.cpp" line="618"/>
        <location filename="dialogelencoprotocolli.cpp" line="727"/>
        <location filename="dialogelencoprotocolli.cpp" line="741"/>
        <location filename="dialogelencoprotocolli.cpp" line="791"/>
        <location filename="dialogelencoprotocolli.cpp" line="907"/>
        <location filename="dialogelencoprotocolli.cpp" line="972"/>
        <location filename="dialogelencoprotocolli.cpp" line="986"/>
        <location filename="dialogelencoprotocolli.cpp" line="1089"/>
        <location filename="dialogelencoprotocolli.cpp" line="1103"/>
        <location filename="dialogelencoprotocolli.cpp" line="1153"/>
        <location filename="dialogelencoprotocolli.cpp" line="1177"/>
        <location filename="dialogelencoprotocolli.cpp" line="1244"/>
        <location filename="dialogelencoprotocolli.cpp" line="1611"/>
        <location filename="dialogelencoprotocolli.cpp" line="1625"/>
        <location filename="dialogelencoprotocolli.cpp" line="1639"/>
        <location filename="dialogelencoprotocolli.cpp" line="1690"/>
        <location filename="dialogelencoprotocolli.cpp" line="1704"/>
        <location filename="dialogelencoprotocolli.cpp" line="1718"/>
        <location filename="dialogelencoprotocolli.cpp" line="1732"/>
        <location filename="dialogelencoprotocolli.cpp" line="1784"/>
        <location filename="dialogelencoprotocolli.cpp" line="1798"/>
        <location filename="dialogelencoprotocolli.cpp" line="1812"/>
        <location filename="dialogelencoprotocolli.cpp" line="1826"/>
        <location filename="dialogelencoprotocolli.cpp" line="1877"/>
        <location filename="dialogelencoprotocolli.cpp" line="1970"/>
        <source>supino</source>
        <translation>仰卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="295"/>
        <location filename="dialogelencoprotocolli.cpp" line="323"/>
        <location filename="dialogelencoprotocolli.cpp" line="383"/>
        <location filename="dialogelencoprotocolli.cpp" line="815"/>
        <source>diaframma</source>
        <translation>膈肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="384"/>
        <location filename="dialogelencoprotocolli.cpp" line="398"/>
        <location filename="dialogelencoprotocolli.cpp" line="412"/>
        <source>sulla lesione</source>
        <translation>在损伤处</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="261"/>
        <location filename="dialogelencoprotocolli.cpp" line="297"/>
        <location filename="dialogelencoprotocolli.cpp" line="385"/>
        <location filename="dialogelencoprotocolli.cpp" line="399"/>
        <location filename="dialogelencoprotocolli.cpp" line="413"/>
        <location filename="dialogelencoprotocolli.cpp" line="483"/>
        <location filename="dialogelencoprotocolli.cpp" line="497"/>
        <location filename="dialogelencoprotocolli.cpp" line="579"/>
        <location filename="dialogelencoprotocolli.cpp" line="693"/>
        <location filename="dialogelencoprotocolli.cpp" line="707"/>
        <location filename="dialogelencoprotocolli.cpp" line="1069"/>
        <location filename="dialogelencoprotocolli.cpp" line="1224"/>
        <source>STATICO grande</source>
        <translation>大静态电极</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="128"/>
        <source>LOMBALGIA</source>
        <translation>下腰背疼痛</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="129"/>
        <source>LOMBOSCIALTAGIA SUB ACUTA</source>
        <translation>腰背痛 亚急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="130"/>
        <source>PERIARTRITE SPALLA FASE ACUTA (DINAMICI)</source>
        <translation>肩周炎 急性期 动态</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="131"/>
        <source>PERIARTRITE SPALLA FASE ACUTA (STATICI)</source>
        <translation>肩周炎 急性期 静止</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="132"/>
        <source>PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (DINAMICI)</source>
        <translation>肩周炎 亚急性期或慢性期 动态</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="133"/>
        <source>PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (STATICI)</source>
        <translation>肩周炎 亚急性期或慢性期 静止</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="134"/>
        <source>SPALLA FASE ACUTA</source>
        <translation>肩部 急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="136"/>
        <source>SPALLA FASE SUB CUTA</source>
        <translation>肩周炎 亚急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="137"/>
        <source>EPICONDILITE DA SOVRACCARICO</source>
        <translation>过载引起的上踝炎</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="138"/>
        <source>POLSO FASE ACUTA</source>
        <translation>手腕疼痛 急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="139"/>
        <source>POLSO FASE SUB  ACUTA O CRONICA</source>
        <translation>手腕疼痛 亚急性期或恢复期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="140"/>
        <source>COXARTROSI SUB ACUTA E CRONICA</source>
        <translation>股骨头坏死 亚急性期或恢复期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="141"/>
        <source>DISTORSIONE CAVIGLIA FASE ACUTA</source>
        <translation>踝关节扭伤 急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="142"/>
        <source>DISTORSIONE CAVIGLIA FASE SUB ACUTA</source>
        <translation>踝关节变形 亚急性阶段</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="162"/>
        <source>GINOCCHIO POST CHIRURGICO</source>
        <translation>膝部（术后）</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="164"/>
        <source>PRIMA FASE - DRENAGGIO</source>
        <translation>第一阶段：淋巴引流</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="165"/>
        <source>PRIMA FASE - ANTINFIAMMATORIO</source>
        <translation>第一阶段：抗发炎</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="166"/>
        <source>SECONDA FASE - MOBILIZZAZZIONE PASSIVA</source>
        <translation>第二阶段：被动运动</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="167"/>
        <source>TERZA FASE - MOBILIZZAZZIONE ATTIVA</source>
        <translation>第三阶段：主动运动</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="168"/>
        <source>QUARTA FASE - MOBILIZZAZZIONE ATTIVA CON RESISTENZA</source>
        <translation>第四阶段：主动抗阻运动</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="169"/>
        <source>QUINTA FASE - RECUPERO DEFICIT DI FLESSIONE</source>
        <translation>第五阶段：恢复屈曲不足</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="170"/>
        <source>SESTA FASE - RECUPERO DEFICIT IN FLESSO/ESTENSIONE</source>
        <translation>第六阶段：恢复屈/伸不足</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="172"/>
        <source>SESTA FASE - RECUPERO MOBILITA ARTICOLARE</source>
        <translation>第六阶段：恢复关节活动度</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="204"/>
        <source>TRATTAMENTO DELLE LESIONI SPINALI PERIFERICHE</source>
        <translation>周围脊柱损伤</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="310"/>
        <source>muscolo psoas</source>
        <translation>腰大肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="324"/>
        <source>L1-L4 e piliforme</source>
        <translation>L1-L4梨状肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="338"/>
        <source>zona lombare e glutei</source>
        <translation>腰部和臀肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="397"/>
        <location filename="dialogelencoprotocolli.cpp" line="606"/>
        <location filename="dialogelencoprotocolli.cpp" line="1692"/>
        <location filename="dialogelencoprotocolli.cpp" line="1720"/>
        <location filename="dialogelencoprotocolli.cpp" line="1786"/>
        <location filename="dialogelencoprotocolli.cpp" line="1814"/>
        <source>polso</source>
        <translation>手腕</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="411"/>
        <location filename="dialogelencoprotocolli.cpp" line="1957"/>
        <location filename="dialogelencoprotocolli.cpp" line="2036"/>
        <location filename="dialogelencoprotocolli.cpp" line="2051"/>
        <source>pianta del piede</source>
        <translation>足底</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="518"/>
        <source>inserzione nervo sciatico fino irradiazione dolore</source>
        <translation>痛点位置</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="532"/>
        <source>muscoli paravertebrali, glutei, bicipiti femorali, polpacci e tibiali</source>
        <translation>椎旁肌肉，臀肌，股二头肌，小腿肌肉，胫骨肌肉</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1247"/>
        <source>RESISTIVO piccolo</source>
        <translation>小电阻</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1309"/>
        <location filename="dialogelencoprotocolli.cpp" line="1323"/>
        <location filename="dialogelencoprotocolli.cpp" line="1561"/>
        <source>bicipite femorale</source>
        <translation>肱二头肌部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1310"/>
        <source>inserzione vasto mediale,vasto laterale,retto femorale</source>
        <translation>股内侧肌，股外侧肌附着点</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1436"/>
        <location filename="dialogelencoprotocolli.cpp" line="1450"/>
        <location filename="dialogelencoprotocolli.cpp" line="1524"/>
        <location filename="dialogelencoprotocolli.cpp" line="1538"/>
        <source>tricipite brachiale,sovraspinato,infraspinato,trapezio,pettorali,bicipite brachiale</source>
        <translation>肱三头肌，冈上肌，冈下肌，斜方肌，胸肌，肱二头肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1474"/>
        <source>bicipite femorale,polpaccio,tibiale</source>
        <translation>股二头肌，小腿肌肉，胫骨肌肉</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1613"/>
        <source>braccio e avambraccio</source>
        <translation>上臂和前臂</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1627"/>
        <source>inserzione tendini</source>
        <translation>肌腱附着点</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1878"/>
        <source>lombo sacrale</source>
        <translation>腰骶部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1879"/>
        <source>zona trocanterica e ischiatica</source>
        <translation>股骨大转子和坐骨部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1891"/>
        <source>supina</source>
        <translation>仰卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1907"/>
        <source>gluteo e bicipite femorale</source>
        <translation>臀肌和股二头肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1958"/>
        <location filename="dialogelencoprotocolli.cpp" line="2037"/>
        <source>gastrocnemius mediale e laterale e soleo</source>
        <translation>内外侧腓肠肌和比目鱼肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1971"/>
        <location filename="dialogelencoprotocolli.cpp" line="2050"/>
        <source>gastrocnemius</source>
        <translation>腓肠肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1972"/>
        <source>dorso del piede</source>
        <translation>脚背</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1985"/>
        <source>gastrocnemius pos</source>
        <translation>腓肠肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1986"/>
        <source>pianta del piede, soleo e gastrocnemius laterale</source>
        <translation>足底，比目鱼肌和外侧腓肠肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="577"/>
        <location filename="dialogelencoprotocolli.cpp" line="591"/>
        <location filename="dialogelencoprotocolli.cpp" line="605"/>
        <location filename="dialogelencoprotocolli.cpp" line="619"/>
        <source>zona cervico/dorsale</source>
        <translation>颈/背部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="578"/>
        <source>muscolo retto femorale</source>
        <translation>股直肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="592"/>
        <source>articolazione gleno omerale</source>
        <translation>盂肱关节</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="361"/>
        <location filename="dialogelencoprotocolli.cpp" line="555"/>
        <location filename="dialogelencoprotocolli.cpp" line="593"/>
        <location filename="dialogelencoprotocolli.cpp" line="607"/>
        <location filename="dialogelencoprotocolli.cpp" line="794"/>
        <location filename="dialogelencoprotocolli.cpp" line="817"/>
        <location filename="dialogelencoprotocolli.cpp" line="887"/>
        <location filename="dialogelencoprotocolli.cpp" line="910"/>
        <location filename="dialogelencoprotocolli.cpp" line="938"/>
        <location filename="dialogelencoprotocolli.cpp" line="975"/>
        <location filename="dialogelencoprotocolli.cpp" line="989"/>
        <location filename="dialogelencoprotocolli.cpp" line="1012"/>
        <location filename="dialogelencoprotocolli.cpp" line="1156"/>
        <location filename="dialogelencoprotocolli.cpp" line="1275"/>
        <location filename="dialogelencoprotocolli.cpp" line="1325"/>
        <location filename="dialogelencoprotocolli.cpp" line="1414"/>
        <location filename="dialogelencoprotocolli.cpp" line="1563"/>
        <location filename="dialogelencoprotocolli.cpp" line="1642"/>
        <location filename="dialogelencoprotocolli.cpp" line="1721"/>
        <location filename="dialogelencoprotocolli.cpp" line="1815"/>
        <location filename="dialogelencoprotocolli.cpp" line="2052"/>
        <source>STATICO piccolo</source>
        <translation>小静态电极</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="516"/>
        <location filename="dialogelencoprotocolli.cpp" line="530"/>
        <source>decubito laterale prono</source>
        <translation>侧卧位或俯卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="553"/>
        <location filename="dialogelencoprotocolli.cpp" line="973"/>
        <source>gadtrocnemius</source>
        <translation>腓肠肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="554"/>
        <location filename="dialogelencoprotocolli.cpp" line="793"/>
        <location filename="dialogelencoprotocolli.cpp" line="974"/>
        <location filename="dialogelencoprotocolli.cpp" line="988"/>
        <location filename="dialogelencoprotocolli.cpp" line="1155"/>
        <location filename="dialogelencoprotocolli.cpp" line="1324"/>
        <location filename="dialogelencoprotocolli.cpp" line="1473"/>
        <location filename="dialogelencoprotocolli.cpp" line="1562"/>
        <source>retto femorale</source>
        <translation>股直肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="620"/>
        <source>arto superiore plegico</source>
        <translation>上肢偏瘫部位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="935"/>
        <location filename="dialogelencoprotocolli.cpp" line="1272"/>
        <location filename="dialogelencoprotocolli.cpp" line="1308"/>
        <location filename="dialogelencoprotocolli.cpp" line="1322"/>
        <location filename="dialogelencoprotocolli.cpp" line="1434"/>
        <location filename="dialogelencoprotocolli.cpp" line="1448"/>
        <location filename="dialogelencoprotocolli.cpp" line="1522"/>
        <location filename="dialogelencoprotocolli.cpp" line="1536"/>
        <location filename="dialogelencoprotocolli.cpp" line="1560"/>
        <source>seduto</source>
        <translation>坐位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="814"/>
        <location filename="dialogelencoprotocolli.cpp" line="1009"/>
        <source>confortevole</source>
        <translation>舒适体位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="816"/>
        <source>compressione intervertebrale</source>
        <translation>椎体压痛点</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="309"/>
        <location filename="dialogelencoprotocolli.cpp" line="337"/>
        <location filename="dialogelencoprotocolli.cpp" line="359"/>
        <location filename="dialogelencoprotocolli.cpp" line="742"/>
        <location filename="dialogelencoprotocolli.cpp" line="792"/>
        <location filename="dialogelencoprotocolli.cpp" line="987"/>
        <location filename="dialogelencoprotocolli.cpp" line="1104"/>
        <location filename="dialogelencoprotocolli.cpp" line="1154"/>
        <source>zona lombare</source>
        <translation>腰部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="728"/>
        <location filename="dialogelencoprotocolli.cpp" line="1090"/>
        <location filename="dialogelencoprotocolli.cpp" line="1612"/>
        <location filename="dialogelencoprotocolli.cpp" line="1626"/>
        <location filename="dialogelencoprotocolli.cpp" line="1640"/>
        <source>scapola</source>
        <translation>肩胛骨</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="729"/>
        <location filename="dialogelencoprotocolli.cpp" line="1091"/>
        <source>deltoide e bicipiti brachiali</source>
        <translation>三角肌和肱二头肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="743"/>
        <location filename="dialogelencoprotocolli.cpp" line="1105"/>
        <source>pettorale e bicipiti brachiali</source>
        <translation>胸肌和肱二头肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="756"/>
        <location filename="dialogelencoprotocolli.cpp" line="908"/>
        <location filename="dialogelencoprotocolli.cpp" line="1118"/>
        <location filename="dialogelencoprotocolli.cpp" line="1245"/>
        <location filename="dialogelencoprotocolli.cpp" line="1691"/>
        <location filename="dialogelencoprotocolli.cpp" line="1705"/>
        <location filename="dialogelencoprotocolli.cpp" line="1719"/>
        <location filename="dialogelencoprotocolli.cpp" line="1733"/>
        <location filename="dialogelencoprotocolli.cpp" line="1785"/>
        <location filename="dialogelencoprotocolli.cpp" line="1799"/>
        <location filename="dialogelencoprotocolli.cpp" line="1813"/>
        <location filename="dialogelencoprotocolli.cpp" line="1827"/>
        <location filename="dialogelencoprotocolli.cpp" line="1892"/>
        <source>braccio</source>
        <translation>上臂</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="757"/>
        <location filename="dialogelencoprotocolli.cpp" line="1119"/>
        <source>sotto scapolare e scapolare</source>
        <translation>肩胛骨下部和肩胛部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="769"/>
        <location filename="dialogelencoprotocolli.cpp" line="921"/>
        <location filename="dialogelencoprotocolli.cpp" line="949"/>
        <location filename="dialogelencoprotocolli.cpp" line="1131"/>
        <location filename="dialogelencoprotocolli.cpp" line="1258"/>
        <location filename="dialogelencoprotocolli.cpp" line="1286"/>
        <source>su di un lato</source>
        <translation>侧卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="770"/>
        <location filename="dialogelencoprotocolli.cpp" line="922"/>
        <location filename="dialogelencoprotocolli.cpp" line="950"/>
        <location filename="dialogelencoprotocolli.cpp" line="1132"/>
        <location filename="dialogelencoprotocolli.cpp" line="1259"/>
        <location filename="dialogelencoprotocolli.cpp" line="1287"/>
        <source>gomito</source>
        <translation>肘部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="771"/>
        <location filename="dialogelencoprotocolli.cpp" line="923"/>
        <location filename="dialogelencoprotocolli.cpp" line="951"/>
        <location filename="dialogelencoprotocolli.cpp" line="1133"/>
        <location filename="dialogelencoprotocolli.cpp" line="1260"/>
        <location filename="dialogelencoprotocolli.cpp" line="1288"/>
        <source>deltoide e zona scapolare</source>
        <translation>三角肌和肩胛部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="909"/>
        <location filename="dialogelencoprotocolli.cpp" line="1246"/>
        <source>zona scapolare</source>
        <translation>肩胛部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="937"/>
        <location filename="dialogelencoprotocolli.cpp" line="1274"/>
        <source>bicipite brachiale</source>
        <translation>颈-背部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1117"/>
        <location filename="dialogelencoprotocolli.cpp" line="1905"/>
        <source>prona</source>
        <translation>俯卧</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1178"/>
        <source>lombare</source>
        <translation>腰部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1179"/>
        <source>muscoli denervati</source>
        <translation>肌肉损伤</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1010"/>
        <source>plesso brachiale posteriore</source>
        <translation>后臂丛神经</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1011"/>
        <source>plesso brachiale anteriore</source>
        <translation>前臂丛神经</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="936"/>
        <location filename="dialogelencoprotocolli.cpp" line="1273"/>
        <location filename="dialogelencoprotocolli.cpp" line="1641"/>
        <location filename="dialogelencoprotocolli.cpp" line="1706"/>
        <location filename="dialogelencoprotocolli.cpp" line="1734"/>
        <location filename="dialogelencoprotocolli.cpp" line="1800"/>
        <location filename="dialogelencoprotocolli.cpp" line="1828"/>
        <source>avambraccio</source>
        <translation>前臂</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="296"/>
        <source>lombosacrale</source>
        <translation>腰骶部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1893"/>
        <source>coscia</source>
        <translation>大腿</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="360"/>
        <source>cavo popliteo</source>
        <translation>腘窝</translation>
    </message>
</context>
<context>
    <name>dialogMenuGuida</name>
    <message>
        <location filename="dialogmenuguida.cpp" line="37"/>
        <source>PROTOCOLLI</source>
        <translation>处方</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="38"/>
        <source>GENERICI</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="39"/>
        <location filename="dialogmenuguida.cpp" line="41"/>
        <source>PROTOCOLLI PATOLOGIE</source>
        <translation>常见病症处方</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="40"/>
        <source>NEUROLOGICI</source>
        <translation>神經</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="42"/>
        <source>ORTOPEDICI</source>
        <translation>骨科</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="44"/>
        <source>Descrizione protocolli generici</source>
        <translation>结合运动疗法的动/静态治疗处方</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="45"/>
        <source>Descrizione protocolli neurologici</source>
        <translation>适用于神经损伤患者的治疗处方</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="46"/>
        <source>Descrizione protocolli ortopedici</source>
        <translation>适用于骨损伤患者的治疗处方</translation>
    </message>
</context>
<context>
    <name>dialogSelezionaProgramma</name>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="58"/>
        <location filename="dialogselezionaprogramma.cpp" line="59"/>
        <location filename="dialogselezionaprogramma.cpp" line="60"/>
        <location filename="dialogselezionaprogramma.cpp" line="61"/>
        <source>Posizione Neutro :</source>
        <translation>负极位置 :</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="53"/>
        <location filename="dialogselezionaprogramma.cpp" line="54"/>
        <location filename="dialogselezionaprogramma.cpp" line="55"/>
        <location filename="dialogselezionaprogramma.cpp" line="56"/>
        <source>Posizione Paziente :</source>
        <translation>患者体位 :</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="63"/>
        <location filename="dialogselezionaprogramma.cpp" line="64"/>
        <location filename="dialogselezionaprogramma.cpp" line="65"/>
        <location filename="dialogselezionaprogramma.cpp" line="66"/>
        <source>Posizione Elettrodo :</source>
        <translation>工作电极位置 :</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="68"/>
        <location filename="dialogselezionaprogramma.cpp" line="69"/>
        <location filename="dialogselezionaprogramma.cpp" line="70"/>
        <location filename="dialogselezionaprogramma.cpp" line="71"/>
        <source>Elettrodo</source>
        <translation>电极</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="84"/>
        <location filename="dialogselezionaprogramma.cpp" line="164"/>
        <location filename="dialogselezionaprogramma.cpp" line="189"/>
        <location filename="dialogselezionaprogramma.cpp" line="214"/>
        <location filename="dialogselezionaprogramma.cpp" line="239"/>
        <source>POTENZA</source>
        <translation>功率</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="85"/>
        <location filename="dialogselezionaprogramma.cpp" line="165"/>
        <location filename="dialogselezionaprogramma.cpp" line="190"/>
        <location filename="dialogselezionaprogramma.cpp" line="215"/>
        <location filename="dialogselezionaprogramma.cpp" line="240"/>
        <source>MODALITA</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="86"/>
        <location filename="dialogselezionaprogramma.cpp" line="166"/>
        <location filename="dialogselezionaprogramma.cpp" line="167"/>
        <location filename="dialogselezionaprogramma.cpp" line="191"/>
        <location filename="dialogselezionaprogramma.cpp" line="192"/>
        <location filename="dialogselezionaprogramma.cpp" line="216"/>
        <location filename="dialogselezionaprogramma.cpp" line="217"/>
        <location filename="dialogselezionaprogramma.cpp" line="241"/>
        <location filename="dialogselezionaprogramma.cpp" line="242"/>
        <source>FREQUENZA</source>
        <translation>频率</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="87"/>
        <location filename="dialogselezionaprogramma.cpp" line="168"/>
        <location filename="dialogselezionaprogramma.cpp" line="169"/>
        <location filename="dialogselezionaprogramma.cpp" line="170"/>
        <location filename="dialogselezionaprogramma.cpp" line="193"/>
        <location filename="dialogselezionaprogramma.cpp" line="194"/>
        <location filename="dialogselezionaprogramma.cpp" line="195"/>
        <location filename="dialogselezionaprogramma.cpp" line="218"/>
        <location filename="dialogselezionaprogramma.cpp" line="219"/>
        <location filename="dialogselezionaprogramma.cpp" line="220"/>
        <location filename="dialogselezionaprogramma.cpp" line="243"/>
        <location filename="dialogselezionaprogramma.cpp" line="244"/>
        <location filename="dialogselezionaprogramma.cpp" line="245"/>
        <source>SLF</source>
        <translation>调制频率</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="163"/>
        <location filename="dialogselezionaprogramma.cpp" line="188"/>
        <location filename="dialogselezionaprogramma.cpp" line="213"/>
        <location filename="dialogselezionaprogramma.cpp" line="238"/>
        <source>TEMPO</source>
        <translation>时间</translation>
    </message>
</context>
<context>
    <name>dialogSelezionaabcd</name>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="42"/>
        <source>CAPACITIVO DINAMICO</source>
        <translation>动态电容</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="43"/>
        <source>CAPACITIVO STATICO GRANDE</source>
        <translation>大静态电容</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="44"/>
        <source>CAPACITIVO STATICO PICCOLO</source>
        <translation>小静态电容</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="45"/>
        <source>RESISTIVO DINAMICO</source>
        <translation>动态电阻</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="46"/>
        <source>RESISTIVO STATICO GRANDE</source>
        <translation>大静态电阻</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="47"/>
        <source>RESISTIVO STATICO PICCOLO</source>
        <translation>小静态电阻</translation>
    </message>
</context>
<context>
    <name>dialogSetup</name>
    <message>
        <location filename="dialogsetup.ui" line="47"/>
        <source>Setup</source>
        <translation>組態</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="47"/>
        <source>Tipo 1</source>
        <translation>DIA-01</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="48"/>
        <source>Tipo 2</source>
        <translation>DIA-02</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="49"/>
        <source>Tipo 3</source>
        <translation>DIA-03</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="51"/>
        <source>Tipo di macchina</source>
        <translation>機的類型</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="98"/>
        <source>Vecchia password</source>
        <translation>舊密碼</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="110"/>
        <source>Nuova password</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="113"/>
        <source>Riscrivi password</source>
        <translation>重新輸入密碼</translation>
    </message>
</context>
<context>
    <name>dialogStartCalibrazione</name>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="31"/>
        <source>PROCEDURA DI CALIBRAZIONE</source>
        <translation>校準程序</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="32"/>
        <source>Info calibrazione</source>
        <translation>連接機頭電容和電阻未插入電極。放置一方面接地返回電極。開始校準過程</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="79"/>
        <source>Vecchia password</source>
        <translation>舊密碼</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="92"/>
        <source>Nuova password</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="95"/>
        <source>Riscrivi password</source>
        <translation>重新輸入密碼</translation>
    </message>
</context>
<context>
    <name>dielogUser</name>
    <message>
        <location filename="dieloguser.cpp" line="79"/>
        <source>CALIBRA</source>
        <translation>校准</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="80"/>
        <source>OROLOGIO</source>
        <translation>时钟</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="142"/>
        <source>Vecchia password</source>
        <translation>舊密碼</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="154"/>
        <source>Nuova password</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="157"/>
        <source>Riscrivi password</source>
        <translation>重新輸入密碼</translation>
    </message>
</context>
<context>
    <name>ufgOrologio</name>
    <message>
        <location filename="ufgorologio.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
</TS>
