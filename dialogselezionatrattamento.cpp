#include "dialogselezionatrattamento.h"
#include "ui_dialogselezionaabcd.h"

#include "QPainter"

dialogSelezionaabcd::dialogSelezionaabcd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogSelezionaabcd)
{
    ui->setupUi(this);

    ui->button_ok->setImage( ":/img/ico/ok.png",":/img/ico/ok.png",":/img/ico/ok.png", 100 );
    //ui->button_ok->setBordoDimensione(2);

    ui->selezione_1->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->selezione_2->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->selezione_3->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->selezione_4->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->selezione_5->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->selezione_6->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );

    connect( ui->selezione_1,  SIGNAL(buttonClicked()), this, SLOT(selezionatoA()));
    connect( ui->selezione_2,  SIGNAL(buttonClicked()), this, SLOT(selezionatoB()));
    connect( ui->selezione_3,  SIGNAL(buttonClicked()), this, SLOT(selezionatoC()));
    connect( ui->selezione_4,  SIGNAL(buttonClicked()), this, SLOT(selezionatoD()));
    connect( ui->selezione_5,  SIGNAL(buttonClicked()), this, SLOT(selezionatoE()));
    connect( ui->selezione_6,  SIGNAL(buttonClicked()), this, SLOT(selezionatoF()));

    connect( ui->button_ok, SIGNAL(buttonClicked()), this, SLOT(esci()));

}

dialogSelezionaabcd::~dialogSelezionaabcd()
{
    delete ui;
}

void dialogSelezionaabcd::setParametri(parametri *conf)
{
    mConfig = conf;

    ui->selezione_1->setTitle( tr("CAPACITIVO DINAMICO").toUpper());
    ui->selezione_2->setTitle( tr("CAPACITIVO STATICO GRANDE").toUpper());
    ui->selezione_3->setTitle( tr("CAPACITIVO STATICO PICCOLO").toUpper());
    ui->selezione_4->setTitle( tr("RESISTIVO DINAMICO").toUpper());
    ui->selezione_5->setTitle( tr("RESISTIVO STATICO GRANDE").toUpper());
    ui->selezione_6->setTitle( tr("RESISTIVO STATICO PICCOLO").toUpper());

    ui->selezione_1->setFontSize( 11 );
    ui->selezione_2->setFontSize( 11 );
    ui->selezione_3->setFontSize( 11 );
    ui->selezione_4->setFontSize( 11 );
    ui->selezione_5->setFontSize( 11 );
    ui->selezione_6->setFontSize( 11 );

    if(      mConfig->mioProgramma.sonda.tipo.capacitivo_dinamico        ) selezionatoA();
    else if( mConfig->mioProgramma.sonda.tipo.capacitivo_old             ) selezionatoA();
    else if( mConfig->mioProgramma.sonda.tipo.capacitivo_statico_grande  ) selezionatoB();
    else if( mConfig->mioProgramma.sonda.tipo.capacitivo_statico_piccolo ) selezionatoC();
    else if( mConfig->mioProgramma.sonda.tipo.resistivo_dinamico         ) selezionatoD();
    else if( mConfig->mioProgramma.sonda.tipo.resistivo_old              ) selezionatoD();
    else if( mConfig->mioProgramma.sonda.tipo.resistivo_statico_grande   ) selezionatoE();
    else if( mConfig->mioProgramma.sonda.tipo.resistivo_statico_piccolo  ) selezionatoF();

}

void dialogSelezionaabcd::setTitle(QString title)
{
    ui->labelTitolo->setText(title);
}

void dialogSelezionaabcd::selezionatoA()
{
    ui->selezione_1->setChecked(true);
    ui->selezione_2->setChecked(false);
    ui->selezione_3->setChecked(false);
    ui->selezione_4->setChecked(false);
    ui->selezione_5->setChecked(false);
    ui->selezione_6->setChecked(false);
}
void dialogSelezionaabcd::selezionatoB()
{
    ui->selezione_1->setChecked(false);
    ui->selezione_2->setChecked(true);
    ui->selezione_3->setChecked(false);
    ui->selezione_4->setChecked(false);
    ui->selezione_5->setChecked(false);
    ui->selezione_6->setChecked(false);
}
void dialogSelezionaabcd::selezionatoC()
{
    ui->selezione_1->setChecked(false);
    ui->selezione_2->setChecked(false);
    ui->selezione_3->setChecked(true);
    ui->selezione_4->setChecked(false);
    ui->selezione_5->setChecked(false);
    ui->selezione_6->setChecked(false);
}
void dialogSelezionaabcd::selezionatoD()
{
    ui->selezione_1->setChecked(false);
    ui->selezione_2->setChecked(false);
    ui->selezione_3->setChecked(false);
    ui->selezione_4->setChecked(true);
    ui->selezione_5->setChecked(false);
    ui->selezione_6->setChecked(false);
}
void dialogSelezionaabcd::selezionatoE()
{
    ui->selezione_1->setChecked(false);
    ui->selezione_2->setChecked(false);
    ui->selezione_3->setChecked(false);
    ui->selezione_4->setChecked(false);
    ui->selezione_5->setChecked(true);
    ui->selezione_6->setChecked(false);
}
void dialogSelezionaabcd::selezionatoF()
{
    ui->selezione_1->setChecked(false);
    ui->selezione_2->setChecked(false);
    ui->selezione_3->setChecked(false);
    ui->selezione_4->setChecked(false);
    ui->selezione_5->setChecked(false);
    ui->selezione_6->setChecked(true);
 }

void dialogSelezionaabcd::esci()
{    
    mConfig->mioProgramma.sonda.val = 0;

    if(      ui->selezione_1->checked ) mConfig->mioProgramma.sonda.tipo.capacitivo_dinamico        = 1;
    else if( ui->selezione_2->checked ) mConfig->mioProgramma.sonda.tipo.capacitivo_statico_grande  = 1;
    else if( ui->selezione_3->checked ) mConfig->mioProgramma.sonda.tipo.capacitivo_statico_piccolo = 1;
    else if( ui->selezione_4->checked ) mConfig->mioProgramma.sonda.tipo.resistivo_dinamico         = 1;
    else if( ui->selezione_5->checked ) mConfig->mioProgramma.sonda.tipo.resistivo_statico_grande   = 1;
    else if( ui->selezione_6->checked ) mConfig->mioProgramma.sonda.tipo.resistivo_statico_piccolo  = 1;

    this->close();
    emit onClose();
}

void dialogSelezionaabcd::rotellaClick()
{
    esci();
}

void dialogSelezionaabcd::rotellaMove(bool up)
{
    if(      ui->selezione_1->focus ) ui->selezione_2->setFocus();
    else if( ui->selezione_2->focus ) ui->selezione_3->setFocus();
    else if( ui->selezione_3->focus ) ui->selezione_4->setFocus();
    else if( ui->selezione_4->focus ) ui->selezione_5->setFocus();
    else if( ui->selezione_5->focus ) ui->selezione_6->setFocus();
    else                              ui->selezione_1->setFocus();

    if(     this->focusWidget() == ui->selezione_1  ) selezionatoA();
    else if(this->focusWidget() == ui->selezione_2  ) selezionatoB();
    else if(this->focusWidget() == ui->selezione_3  ) selezionatoC();
    else if(this->focusWidget() == ui->selezione_4  ) selezionatoD();
    else if(this->focusWidget() == ui->selezione_5  ) selezionatoE();
    else if(this->focusWidget() == ui->selezione_6  ) selezionatoF();

}

void dialogSelezionaabcd::paintEvent(QPaintEvent *e)
{
    //QPainterPath path;
    QPainter painter(this);
    QRect area = this->rect();

    area.setX(2);
    area.setY(2);
    area.setWidth( area.width()-2 );
    area.setHeight( area.height()-2 );


    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(4);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);


    painter.setPen(pen);

    //path.addRoundedRect( area, 15, 15);
    //painter.drawPath( path );
    //painter.fillPath( path , Qt::blue );
    painter.drawRoundRect( area ,4,4 );
}

void dialogSelezionaabcd::showEvent(QShowEvent *e)
{

}
