#include "QPainter"
#include "ufgbutton2label.h"
#include "ui_ufgbutton2label.h"

ufgButton2Label::ufgButton2Label(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ufgButton2Label)
{
    ui->setupUi(this);

    bdColor  = QColor( 255 ,255 ,255 );
    bgColor2 = QColor( 255,  150,   0 );
    bgColor1 = QColor( 142,180,227);


    focus   = false;
    premuto = false;
}

ufgButton2Label::~ufgButton2Label()
{
    delete ui;
}

void ufgButton2Label::setTitle(QString title)
{
    ui->labelTitolo->setText(title);
}

void ufgButton2Label::setTitleColor(QColor colore)
{
    QVariant variant = colore;
    QString colcode = variant.toString();
    ui->labelTitolo->setStyleSheet("QLabel { color :"+colcode+" }");
}

void ufgButton2Label::setText(QString testo)
{
    ui->labelTesto->setText(testo);
}

void ufgButton2Label::setTextColor(QColor colore)
{
    QVariant variant = colore;
    QString colcode = variant.toString();
    ui->labelTesto->setStyleSheet("QLabel { color :"+colcode+" }");
}

void ufgButton2Label::setBackgroundColor( QColor col1, QColor col2)
{
    bgColor1 = col1;
    bgColor2 = col2;
    this->update();
}

void ufgButton2Label::setBorderColor( QColor col)
{
    bdColor = col;
    this->update();
}

void ufgButton2Label::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    QLinearGradient myGradient;
    QPen pen(     QColor( 255 ,150 ,150 ),   2);
    QPen penBordo( bdColor , 4);
//    QPen penFocus( QColor(255,0,0) , 4);
    QPen penFocus( bgColor2 , 4);

    QRect brect = this->rect();
    brect.setWidth( this->rect().width() - 6 );
    brect.setHeight( this->rect().height() - 6);
    brect.setLeft(3);
    brect.setTop(3);

    myGradient = QLinearGradient( QPointF(0,0),QPointF(1,0) );


    if( premuto == true)
    {
//        p.drawPixmap(this->rect(), QPixmap(":img/bottoneArancioDown.png" ).scaled( this->rect().size()));
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;

        path.addRoundedRect( brect , 15, 15);
//        myGradient.setStart(  brect.size().width() / 2, 0 );
//        myGradient.setFinalStop( brect.size().width() / 2, this->rect().size().height() );
//        myGradient.setColorAt(0,bgColor1);
//        myGradient.setColorAt(1,bgColor2);
        p.setPen(pen);
        p.fillPath(path, bgColor2 );
        p.drawPath(path);
    }
    else if( focus == true )
    {
//        p.drawPixmap(this->rect(), QPixmap(":img/bottoneArancioUP.png" ).scaled( this->rect().size()));
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;
        path.addRoundedRect( brect , 15, 15);
//        myGradient.setStart(  brect.size().width() / 2, 0 );
//        myGradient.setFinalStop( brect.size().width() / 2, this->rect().size().height() );
//        myGradient.setColorAt(0,QColor(255,165,0));
//        myGradient.setColorAt(0.2,QColor(255,50,0));
//        myGradient.setColorAt(0.8,QColor(255,50,0));
//        myGradient.setColorAt(1,QColor(255,165,0));
//        myGradient.setColorAt(1,bgColor1);
//        myGradient.setColorAt(0,bgColor2);

        p.setPen(pen);
        //p.fillPath(path,myGradient);
        p.fillPath(path,bgColor1);
        p.drawPath(path);
        p.setPen(penFocus);
        p.drawRoundedRect( brect , 15, 15);
    }
    else
    {
//        p.drawPixmap(this->rect(), QPixmap(":img/bottoneArancioUP.png" ).scaled( this->rect().size()));
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;
        path.addRoundedRect( brect , 15, 15);
        myGradient.setStart(  brect.size().width() / 2, 0 );
        myGradient.setFinalStop( brect.size().width() / 2, this->rect().size().height() );
//        myGradient.setColorAt(0,QColor(255,165,0));
//        myGradient.setColorAt(0.2,QColor(255,50,0));
//        myGradient.setColorAt(0.8,QColor(255,50,0));
//        myGradient.setColorAt(1,QColor(255,165,0));
//        myGradient.setColorAt(1,bgColor1);
//        myGradient.setColorAt(0,bgColor2);

        p.setPen(pen);
        p.fillPath(path,bgColor1);
        p.drawPath(path);
        p.setPen(penBordo);
        p.drawRoundedRect( brect , 15, 15);
    }
} bool isFocused();

void ufgButton2Label::mousePressEvent(QMouseEvent* )
{
//    QPainter painter(this);
//    painter.drawPixmap(this->rect(), QPixmap(":img/button_rosso.png" ).scaled( this->rect().size()));

    //this->setStyleSheet( "border-image { ");

    //this->set
    premuto = true;
    //qDebug( "premuto");

    this->update();


}
void ufgButton2Label::mouseReleaseEvent(QMouseEvent* )
{
    //QPainter painter(this);
    //painter.drawPixmap(this->rect(), QPixmap(":img/button_verde.png" ).scaled( this->rect().size()));
 bool isFocused();
    premuto = false;
    //qDebug( "rilasciato");
    this->update();

    emit buttonClicked();
}

void ufgButton2Label::focusInEvent(QFocusEvent *)
{
    focus = true;
    this->update();
}

void ufgButton2Label::focusOutEvent(QFocusEvent *)
{
    focus = false;
    this->update();
}

bool ufgButton2Label::isFocused()
{
    return focus;
}
