#ifndef PARAMTERI_H
#define PARAMTERI_H

#include <QSharedDataPointer>

class paramteriData;

class paramteri
{
public:
    paramteri();
    paramteri(const paramteri &);
    paramteri &operator=(const paramteri &);
    ~paramteri();

private:
    QSharedDataPointer<paramteriData> data;
};

#endif // PARAMTERI_H
